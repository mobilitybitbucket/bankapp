//
//  NumericTextField.swift
//  mUPAM
//
//  Created by Target Soft Systems on 19/02/16.
//  Copyright (c) 2016 Maybank2E. All rights reserved.
//

import UIKit
import QuartzCore

class NumericTextField: PaddedTextField, TextFieldValidation {

    let minimumLength: UInt
    let maximumLength: UInt
    
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */
    
    init(minimumLength:UInt, maximumLength:UInt) {
        self.minimumLength = minimumLength
        self.maximumLength = maximumLength
        super.init(frame: CGRectZero)
        self.keyboardType = UIKeyboardType.NumberPad
    }
    
    init(frame: CGRect, minimumLength:UInt, maximumLength:UInt) {
        self.minimumLength = minimumLength
        self.maximumLength = maximumLength
        super.init(frame: frame)
        self.keyboardType = UIKeyboardType.NumberPad
    }

    required override init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func isValid() -> Bool {
        let length = UInt(countElements(self.text))
        return self.text != "" && !(length > maximumLength || length < minimumLength)
    }
    func isPhoneValid() -> Bool {
        
        let phoneRegEx = "[+][6][0][0-9]{6,14}$"
       
        if let PhoneTest = NSPredicate(format:"SELF MATCHES %@", phoneRegEx) {
            return self.text != "" && PhoneTest.evaluateWithObject(self.text!)
        }
        return false
    }
   

    func alert() {
        self.layer.borderColor = UIColor.redColor().CGColor
        self.becomeFirstResponder()
        UIView.animateWithDuration(0.1, animations: { () -> Void in
            self.frame = CGRectMake(self.frame.origin.x - 6, self.frame.origin.y, self.frame.size.width, self.frame.size.height)
            }) { (result: Bool) -> Void in
                UIView.animateWithDuration(0.1, animations: { () -> Void in
                    self.frame = CGRectMake(self.frame.origin.x + 12, self.frame.origin.y, self.frame.size.width, self.frame.size.height)
                    }) { (result: Bool) -> Void in
                        UIView.animateWithDuration(0.1, animations: { () -> Void in
                            self.frame = CGRectMake(self.frame.origin.x - 6, self.frame.origin.y, self.frame.size.width, self.frame.size.height)
                            }) { (result: Bool) -> Void in
                                
                        }
                }
        }
    }
    
    func canAllowReplacementString(replacementString: String, range: NSRange) -> Bool {
        return range.length > 0 || ("1234567890+" as NSString).rangeOfString(replacementString).location != NSNotFound
    }
}
