//
//  ApplicationTableViewCell.swift
//  mUPAM
//
//  Created by Target Soft Systems on 23/02/16.
//  Copyright (c) 2016 Maybank2E. All rights reserved.
//

import UIKit

class ApplicationTableViewCell: UITableViewCell {

    @IBOutlet var title: UILabel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
