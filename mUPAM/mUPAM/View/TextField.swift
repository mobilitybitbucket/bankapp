//
//  TextField.swift
//  mUPAM
//
//  Created by Target Soft Systems on 7/22/16.
//  Copyright (c) 2016 Maybank2E. All rights reserved.
//

import Foundation

import QuartzCore

class TextField : UITextField {
 
    override func didMoveToSuperview() {
        self.layer.borderWidth = 1
        self.layer.borderColor = UIColor.darkGrayColor().CGColor
    }
}
