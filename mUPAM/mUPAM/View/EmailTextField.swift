//
//  EmailTextField.swift
//  mUPAM
//
//  Created by Target Soft Systems on 19/02/16.
//  Copyright (c) 2016 Maybank2E. All rights reserved.
//

import UIKit

class EmailTextField: PaddedTextField, TextFieldValidation {

    let minimumLength: UInt
    let maximumLength: UInt
    
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
    // Drawing code
    }
    */
    
    init(minimumLength:UInt, maximumLength:UInt) {
        self.minimumLength = minimumLength
        self.maximumLength = maximumLength
        super.init(frame: CGRectZero)
        super.keyboardType = UIKeyboardType.EmailAddress
    }
    
    init(frame: CGRect, minimumLength:UInt, maximumLength:UInt) {
        self.minimumLength = minimumLength
        self.maximumLength = maximumLength
        super.init(frame: frame)
        super.keyboardType = UIKeyboardType.EmailAddress        
    }
    
    required override init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func isValid() -> Bool {
        //let emailRegEx = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$"
        let emailRegEx="[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        if let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx) {
            return self.text != "" && emailTest.evaluateWithObject(self.text!)
        }
        return false
    }
    
    func alert() {
        self.layer.borderColor = UIColor.redColor().CGColor
        self.becomeFirstResponder()
        UIView.animateWithDuration(0.1, animations: { () -> Void in
            self.frame = CGRectMake(self.frame.origin.x - 6, self.frame.origin.y, self.frame.size.width, self.frame.size.height)
            }) { (result: Bool) -> Void in
                UIView.animateWithDuration(0.1, animations: { () -> Void in
                    self.frame = CGRectMake(self.frame.origin.x + 12, self.frame.origin.y, self.frame.size.width, self.frame.size.height)
                    }) { (result: Bool) -> Void in
                        UIView.animateWithDuration(0.1, animations: { () -> Void in
                            self.frame = CGRectMake(self.frame.origin.x - 6, self.frame.origin.y, self.frame.size.width, self.frame.size.height)
                            }) { (result: Bool) -> Void in
                                
                        }
                }
        }
    }
    
    func canAllowReplacementString(replacementString: String, range: NSRange) -> Bool {
        return range.length > 0 || ("1234567890abcdefghijklmnopqrstuvwxyz.@_ " as NSString).rangeOfString(replacementString.lowercaseString).location != NSNotFound
    }
}
