//
//  MandatorableTextField.swift
//  mUPAM
//
//  Created by Target Soft Systems on 7/22/16.
//  Copyright (c) 2016 Maybank2E. All rights reserved.
//

import Foundation
import QuartzCore

class MandatorableTextField : TextField {
    var mandatory: Bool = false
    
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        if(mandatory) {
            self.layer.borderColor = UIColor.redColor().CGColor
        }
    }
}
