//
//  CharactersTextField.swift
//  mUPAM
//
//  Created by Target Soft Systems on 19/02/16.
//  Copyright (c) 2016 Maybank2E. All rights reserved.
//

import UIKit

protocol TextFieldValidation : NSObjectProtocol {
    func canAllowReplacementString(replacementString: String, range: NSRange) -> Bool
}

class CharactersTextField: PaddedTextField, TextFieldValidation {

    let minimumLength: UInt
    let maximumLength: UInt
    
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
    // Drawing code
    }
    */
    
    init(minimumLength:UInt, maximumLength:UInt) {
        self.minimumLength = minimumLength
        self.maximumLength = maximumLength
        super.init(frame: CGRectZero)
    }
    
    init(frame: CGRect, minimumLength:UInt, maximumLength:UInt) {
        self.minimumLength = minimumLength
        self.maximumLength = maximumLength
        super.init(frame: frame)
    }
    
    required override init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func isValid() -> Bool {
        let length = UInt(countElements(self.text))
        return self.text != "" && !(length > maximumLength || length < minimumLength)
    }
    
    func websiteVlaid() -> Bool{
        //let webregex="((\\w)*|([0-9]*)|([-|_])*)+([\\.|/]((\\w)*|([0-9]*)|([-|_])*))+"
        let webregex="(www\\.)[\\w\\d\\-_]+\\.\\w{2,3}(\\.\\w{2})?(/(?<=/)(?:[\\w\\d\\-./_]+)?)?"
        if let webTest = NSPredicate(format:"SELF MATCHES %@", webregex) {
            return self.text != "" && webTest.evaluateWithObject(self.text!)
        }
        return false
    }
    
    func alert() {
        self.layer.borderColor = UIColor.redColor().CGColor
        self.becomeFirstResponder()
        UIView.animateWithDuration(0.1, animations: { () -> Void in
            self.frame = CGRectMake(self.frame.origin.x - 6, self.frame.origin.y, self.frame.size.width, self.frame.size.height)
            }) { (result: Bool) -> Void in
                UIView.animateWithDuration(0.1, animations: { () -> Void in
                    self.frame = CGRectMake(self.frame.origin.x + 12, self.frame.origin.y, self.frame.size.width, self.frame.size.height)
                    }) { (result: Bool) -> Void in
                        UIView.animateWithDuration(0.1, animations: { () -> Void in
                            self.frame = CGRectMake(self.frame.origin.x - 6, self.frame.origin.y, self.frame.size.width, self.frame.size.height)
                            }) { (result: Bool) -> Void in
                                
                        }
                }
        }
    }
    
    func canAllowReplacementString(replacementString: String, range: NSRange) -> Bool {
        return range.length > 0 || ("1234567890abcdefghijklmnopqrstuvwxyz ." as NSString).rangeOfString(replacementString.lowercaseString).location != NSNotFound
    }
}
