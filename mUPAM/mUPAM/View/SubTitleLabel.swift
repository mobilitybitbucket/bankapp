//
//  SubTitleLabel.swift
//  mUPAM
//
//  Created by Target Soft Systems on 7/19/16.
//  Copyright (c) 2016 Maybank2E. All rights reserved.
//

import Foundation

class SubTitleLabel : UILabel {
    override init() {
        super.init()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.font = UIFont.boldSystemFontOfSize(17)
        self.textColor = UIColor.darkGrayColor()
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}