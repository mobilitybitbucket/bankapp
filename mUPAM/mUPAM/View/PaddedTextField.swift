//
//  PaddedTextField.swift
//  mUPAM
//
//  Created by Target Soft Systems on 7/18/16.
//  Copyright (c) 2016 Maybank2E. All rights reserved.
//

class PaddedTextField: MandatorableTextField {

    override func textRectForBounds(bounds: CGRect) -> CGRect {
        let width = self.leftView != nil ? self.leftView!.frame.size.width : 8
        return CGRectMake(width, 8, bounds.width - 16, bounds.width - 16)
    }
    
    override func editingRectForBounds(bounds: CGRect) -> CGRect {
        return self.textRectForBounds(bounds)
    }
}
