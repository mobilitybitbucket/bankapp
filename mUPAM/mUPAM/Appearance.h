//
//  Appearance.h
//  mUPAM
//
//  Created by Target Soft Systems on 7/17/16.
//  Copyright (c) 2016 Maybank2E. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Appearance)

// appearanceWhenContainedIn: is not available in Swift. This fixes that.
+ (instancetype)my_appearanceWhenContainedIn:(Class<UIAppearanceContainer>)containerClass;

@end
