//
//  ClientDeclarationEditViewController.swift
//  mUPAM
//
//  Created by Target Soft Systems on 17/11/15.
//  Copyright (c) 2015 Maybank2E. All rights reserved.
//

import UIKit

class ClientDeclarationEditViewController: UIViewController {
    
    var id: String?
    let nameHint = UILabel()
    let name = CharactersTextField(frame: CGRectMake(0.0, 0.0, 534, 30.0), minimumLength: 1, maximumLength: 40)
    let imageHint = UILabel()
    let image = UIButton()
    let signatureHint = UILabel()
    let signature = UIButton()
    //let sigpath=str
    let dateHint = UILabel(), date = TextField(frame: CGRectMake(0, 0, 534, 30))
    let dateFormatter = NSDateFormatter()
    var photoPickerViewController: UIImagePickerController?
    var signaturePickerViewController: UIImagePickerController?
    var imagePickerPopOverController: UIPopoverController?
    var declaration: Declaration? {
        get {
            if let _photo = image.imageForState(UIControlState.Normal) {
                if let _signature = signature.imageForState(UIControlState.Normal) {
                    var declarationId = ""
                    if let _id = id {
                        declarationId = _id
                    }
                    return Declaration(id: declarationId, name: name.text, photo: _photo, signature: _signature, date: selectedDate)
                }
            }
            return nil
        }
    }
    
    var selectedDate = NSDate()
        
    override init() {
        super.init(nibName: "ClientDeclarationEditViewController", bundle: nil)
        nameHint.text = "Name"
        name.keyboardType = UIKeyboardType.NamePhonePad
        name.delegate = self
        nameHint.sizeToFit()
        imageHint.text = "Photo"
        image.backgroundColor = UIColor.lightGrayColor()
        image.addTarget(self, action: "didTapPhoto", forControlEvents: UIControlEvents.TouchUpInside)
        signatureHint.text = "Signature"
        signature.backgroundColor = UIColor.lightGrayColor()
        signature.addTarget(self, action: "didTapSignature", forControlEvents: UIControlEvents.TouchUpInside)
        dateHint.text = "Date"
        dateHint.sizeToFit()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        date.delegate=self
        let datePicker = UIDatePicker()
        datePicker.datePickerMode = .Date
        datePicker.date = NSDate()
        datePicker.addTarget(self, action: "didSelectDate:", forControlEvents: UIControlEvents.ValueChanged)
        selectedDate = datePicker.date
        date.inputView = datePicker
        date.text = dateFormatter.stringFromDate(selectedDate)
    
//        dateHint.backgroundColor = UIColor.yellowColor()
    }
    
    func didSelectDate(sender: UIDatePicker) {
        selectedDate = sender.date
        date.text = dateFormatter.stringFromDate(selectedDate)
    }
    
    convenience init(clientDeclaration: Declaration) {
        self.init()
        id = clientDeclaration.id
        name.text = clientDeclaration.name
        selectedDate = clientDeclaration.date
        date.text = dateFormatter.stringFromDate(selectedDate)
        image.setImage(clientDeclaration.photo, forState: UIControlState.Normal)
        signature.setImage(clientDeclaration.signature, forState: UIControlState.Normal)
       // signature.setBackgroundImage(UIImage(named:Declaration.sigpath), forState:UIControlState.Normal)
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.addSubview(nameHint)
        view.addSubview(name)
        view.addSubview(imageHint)
        view.addSubview(image)
        view.addSubview(signatureHint)
        view.addSubview(signature)
        view.addSubview(dateHint)
        view.addSubview(date)
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.layoutFacade()
    }
    
    func layoutFacade() {
        view.groupVertically([nameHint], inUpperLeftWithLeftPadding: 10, topPadding: 11, spacing: 10, width: 150, height: 30)
        imageHint.alignUnder(nameHint, withLeftPadding: 10, topPadding: 11, width: nameHint.frame.size.width, height: nameHint.frame.size.height)
        signatureHint.alignUnder(image, withLeftPadding: 10, topPadding: 11, width: nameHint.frame.size.width, height: nameHint.frame.size.height)
        dateHint.alignUnder(signature, withLeftPadding: 10, topPadding: 11, width: nameHint.frame.size.width, height: nameHint.frame.size.height)

        //        imageHint.alignUnder(nameHint, withLeftPadding: 0, topPadding: 0, width: nameHint.frame.size.width, height: nameHint.frame.size.height)
//        imageHint.alignUnder(nameHint, withLeftPadding: 0, topPadding: 0, width: nameHint.frame.size.width, height: nameHint.frame.size.height)

//        view.groupVertically([imageHint, signatureHint], inUpperLeftWithLeftPadding: 10, topPadding: 11, spacing: 10, width: 150, height: 30)
//        view.groupVertically([dateHint], inUpperLeftWithLeftPadding: 10, topPadding: 11, spacing: 10, width: 150, height: 30)

        view.groupVertically([name], inUpperLeftWithLeftPadding: nameHint.frame.origin.x + 150, topPadding: 11, spacing: 10, width: name.frame.size.width, height: name.frame.size.height)
        image.alignUnder(name, withLeftPadding: nameHint.frame.origin.x + 150, topPadding: 10, width: 120, height: 160)
        signature.alignUnder(image, withLeftPadding: nameHint.frame.origin.x + 150, topPadding: 10, width: 200, height:40)
        date.alignUnder(signature, withLeftPadding: nameHint.frame.origin.x + 150, topPadding: 10, width: nameHint.frame.size.width, height: nameHint.frame.size.height)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func validateAndThrowResult() -> Bool {
        if(!name.isValid()){
            name.alert()
            return false
        }
//        if(!date.isValid())
//        {
//            date.alert()
//            return false
//        }
        return true
    }
    
    func setPhoto(photo: UIImage) {
        image.setImage(photo, forState: UIControlState.Normal)
    }
    
    
    func setSignature(signature: UIImage) {
        print(signature)
        self.signature.setImage(signature, forState: UIControlState.Normal)
    }
    
    func didTapSignature() {
        signaturePickerViewController = UIImagePickerController()
        signaturePickerViewController?.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
        signaturePickerViewController?.delegate = self
        imagePickerPopOverController = UIPopoverController(contentViewController: signaturePickerViewController!)
        imagePickerPopOverController!.presentPopoverFromRect(signature.frame, inView: view, permittedArrowDirections: UIPopoverArrowDirection.Left, animated: true)
    }
    
    func didTapPhoto() {
        photoPickerViewController = UIImagePickerController()
        photoPickerViewController?.sourceType =  UIImagePickerControllerSourceType.PhotoLibrary
        photoPickerViewController?.delegate = self
        imagePickerPopOverController = UIPopoverController(contentViewController: photoPickerViewController!)
        imagePickerPopOverController!.presentPopoverFromRect(image.frame, inView: view, permittedArrowDirections: UIPopoverArrowDirection.Left, animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension ClientDeclarationEditViewController : UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [NSObject : AnyObject]) {
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            switch picker {
            case let photoPicker where photoPicker == photoPickerViewController :
                setPhoto(image)
                photoPickerViewController = nil
            case let signaturePicker where signaturePicker == signaturePickerViewController :
                setSignature(image)
                signaturePickerViewController = nil
            default :
                break;
            }
        }
        if let _imagePickerPopOverController = imagePickerPopOverController {
            _imagePickerPopOverController.dismissPopoverAnimated(true)
        }
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        if let _imagePickerPopOverController = imagePickerPopOverController {
            _imagePickerPopOverController.dismissPopoverAnimated(true)
        }
    }
}

extension ClientDeclarationEditViewController: UITextFieldDelegate {
   
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        switch textField {
        case let numericTextField where textField is CharactersTextField:
            return (numericTextField as CharactersTextField).canAllowReplacementString(string, range: range)
        case let numericTextField as NumericTextField where textField is NumericTextField:
            return (numericTextField as NumericTextField).canAllowReplacementString(string, range: range)
        
        default:
            return true
        }
    }
}

