//
//  AccountsEditViewController.swift
//  mUPAM
//
//  Created by Target Soft Systems on 17/11/15.
//  Copyright (c) 2015 Maybank2E. All rights reserved.
//

import UIKit

class AccountsEditViewController: ApplicationFormSectionEditViewController, Editable, Reloadable {
    
    weak var delegate: AccountsEditViewControllerDelegate?
    weak var datasource: AccountsEditViewControllerDatasource?
    
    var users: [AccountUser] {
        get {
            if let _datasource = self.datasource {
                return _datasource.accountUsersForAccountsEditViewController(self)
            }
            else {
                return []
            }
        }
    }
    
    let accountsTableView = UITableView()
    
    var accounts: [Account] = [
        /*
        Account(id: "201", modules: [AccountModule(id: "101", title: "Account Services"), AccountModule(id: "101", title: "Payment")], users: [AccountUser(id: "102", name: "Dass", designation: "Accountant", email: EmailContact(id: "101", value: "darputharaj@yahoo.com"), telephone: TelephoneContact(id: "101", value: "04132254715"), fax: FaxContact(id: "101", value: "04132254715"), mobile: MobileContact(id: "101", value: "9003531715"), verificationReference: VerificationReference(type: VerificationReferenceType(id: "101", name: "Passport"), verificationReferenceId: "2154564"), right: AccountUserRight(role: AccountUserRole(id: "101", title: "Admin"), permissions: [AccountUserPermission(id: "101", title: "Can edit")], canAuthorizerViewDetails: true, approvalGroup: AccountUserApprovalGroup(id: "101", name: "A")))])
        */
        ] {  
        didSet {
            accountsTableView.reloadData()
        }
    }
    
    override init() {
        super.init(nibName: "AccountsEditViewController", bundle: nil)
        accountsTableView.delegate = self
        accountsTableView.dataSource = self
    }
    
    convenience init(title: String) {
        self.init()
        self.title = title
    }
    
    convenience init(title: String, accounts: [Account]) {
        self.init(title: title)
        self.accounts = accounts
        if accounts.count > 0 {
            mode = .Edit
        }
    }
    
//    required init(coder aDecoder: NSCoder) {
//        fatalError("init(coder:) has not been implemented")
//    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(accountsTableView)
        // Do any additional setup after loading the view.
    }
    
    override func viewDidBecomeInEditMode() {
        super.viewDidBecomeInEditMode()
        var rightBarButtonItems = navigationItem.rightBarButtonItems
        if rightBarButtonItems?.count == 1 {
            rightBarButtonItems?.append(UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Add, target: self, action: "addAccount"))
            navigationItem.rightBarButtonItems = rightBarButtonItems
        }
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.layoutFacade()
    }
    
    func layoutFacade() {
        accountsTableView.fillSuperviewWithLeftPadding(0, rightPadding: 0, topPadding: 0, bottomPadding: 0)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func addAccount() {
        let accountEditViewController = AccountEditViewController(users: users)
        let accountEditViewContainerViewController = UINavigationController(rootViewController: accountEditViewController)
        accountEditViewController.delegate = self
        accountEditViewContainerViewController.modalPresentationStyle = UIModalPresentationStyle.FormSheet
        presentViewController(accountEditViewContainerViewController, animated: true) { () -> Void in
            
        }
    }
    
    func edit() {
        mode = .Edit
    }
    
    func save() {
        self.delegate?.accountsEditViewController(self, didRequestToCreateAccounts: accounts)
    }
    
    func reload() {
        
    }
}

extension AccountsEditViewController : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return accounts.count
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell: UITableViewCell!
        if let userCell = tableView .dequeueReusableCellWithIdentifier("user") as? UITableViewCell {
            cell = userCell
        }
        else {
            cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "user")
        }
        
        let account = accounts[indexPath.row]
        
        var attributedTitle = NSMutableAttributedString()
        
        let accountHintDetail = NSMutableAttributedString(string: "Account ID\t:\t")
        attributedTitle.appendAttributedString(accountHintDetail)
        
        let accountDetail = NSMutableAttributedString(string: "\(account.id)\n")
        attributedTitle.appendAttributedString(accountDetail)
        

        let usersHintDetail = NSMutableAttributedString(string: "Users\t\t\t:\t")
        attributedTitle.appendAttributedString(usersHintDetail)
        
        let users = account.users
        var userNames = [String]()
        for user in users {
            userNames.append(user.name)
        }
        let usersDetail = NSMutableAttributedString(string: (userNames as NSArray).componentsJoinedByString(", "))
        attributedTitle.appendAttributedString(usersDetail)
        
        cell.textLabel?.attributedText = attributedTitle
        cell.textLabel?.numberOfLines = 0
        return cell
    }
    
    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        switch editingStyle {
        case .Delete:
            let account = accounts[indexPath.row]
            var foundIndex: Int?
            for i in 0..<accounts.count {
                if (accounts[i] == account) {
                    foundIndex = i
                    break
                }
            }
            if let index = foundIndex {
                accounts.removeAtIndex(index)
            }
            return
        default:
            return
        }
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let account = accounts[indexPath.row]
        let accountEditViewController = AccountEditViewController(account: account, users: users)
        let accountEditViewContainerViewController = UINavigationController(rootViewController: accountEditViewController)
        accountEditViewController.delegate = self
        accountEditViewContainerViewController.modalPresentationStyle = UIModalPresentationStyle.FormSheet
        presentViewController(accountEditViewContainerViewController, animated: true) { () -> Void in
            
        }
    }
}

extension AccountsEditViewController : AccountEditViewControllerDelegate {
    
    func accountEditViewController(controller: AccountEditViewController, didRequestToCreateAccount account: Account) {
        var foundIndex: Int?
        for i in 0..<accounts.count {
            if (accounts[i] == account) {
                foundIndex = i
                break
            }
        }
        if let index = foundIndex {
            accounts.removeAtIndex(index)
            accounts.insert(account, atIndex: index)
        }
        else {
            accounts.append(account)
        }
    }
}

protocol AccountsEditViewControllerDelegate : NSObjectProtocol {
    
    func accountsEditViewController(controller: AccountsEditViewController, didRequestToCreateAccounts accounts: [Account])
}

protocol AccountsEditViewControllerDatasource : NSObjectProtocol {
    
    func accountUsersForAccountsEditViewController(controller: AccountsEditViewController) -> [AccountUser]
}