//
//  ReportViewController.swift
//  mUPAM
//
//  Created by Target Soft Systems on 23/02/16.
//  Copyright (c) 2016 Maybank2E. All rights reserved.
//

import UIKit
import MessageUI

class ReportViewController: UIViewController {
    
    var webView: UIWebView!
    private var application: Application
    
    let appDelegate = UIApplication.sharedApplication().delegate as AppDelegate
    init(application: Application) {
        self.application = application
        super.init(nibName:nil, bundle: nil)
    }
   
    override func viewDidLoad() {
        super.viewDidLoad()
        webView = UIWebView(frame: CGRectMake(0, 0, view.frame.size.width, view.frame.size.height))
        webView.autoresizingMask = UIViewAutoresizing.FlexibleBottomMargin |
            UIViewAutoresizing.FlexibleHeight |
            UIViewAutoresizing.FlexibleLeftMargin |
            UIViewAutoresizing.FlexibleRightMargin |
            UIViewAutoresizing.FlexibleTopMargin |
            UIViewAutoresizing.FlexibleWidth
        webView.dataDetectorTypes = UIDataDetectorTypes.None
         webView.scalesPageToFit = true
        view.addSubview(webView)
       
        if let documentDirectoryPath: String = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.UserDomainMask, true).first as? String {
            webView.loadRequest(NSURLRequest(URL: NSURL(fileURLWithPath:documentDirectoryPath.stringByAppendingPathComponent("report.html"))!))
        }
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.Plain, target: self, action: "cancel")
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Submit", style: UIBarButtonItemStyle.Plain, target: self, action: "submit")
        // Do any additional setup after loading the view.
    }

    func cancel() {
        dismissViewControllerAnimated(true, completion: { () -> Void in
            
        })
    }

    func submit() {
        if (true) {
            print("submit")
            let documentsDirectory: String = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.UserDomainMask, true).first as String
            
            let render = UIPrintPageRenderer()
            render.addPrintFormatter(webView.viewPrintFormatter(), startingAtPageAtIndex: 0)
            let pageSize = CGSizeMake(595.2,841.8)
            let pageMargins = UIEdgeInsetsZero
            let printableRect = CGRectMake(pageMargins.left, pageMargins.top,
                pageSize.width - pageMargins.left - pageMargins.right,
                pageSize.height - pageMargins.top - pageMargins.bottom);
            
            let paperRect = CGRectMake(0, 0, pageSize.width, pageSize.height);
            
            render.setValue(NSValue(CGRect: paperRect), forKey: "paperRect")
            render.setValue(NSValue(CGRect: printableRect), forKey: "printableRect")
            
            let todaydate = NSDate()
            let formatter = NSDateFormatter()
            formatter.dateFormat = "ddMMYYYY"
            let dateStr=formatter.stringFromDate(todaydate)
            print(dateStr)
            
            let businessreg = application.company?.registrationNumber            
            let pdfFileName = String(format: "MY-%@-%@",businessreg!,dateStr) + ".pdf"
            let jsonFileName = String(format: "MY-%@-%@",businessreg!,dateStr) + ".json"
            
            let PDFdata = render.printToPDF();
            PDFdata.writeToFile(documentsDirectory.stringByAppendingPathComponent(pdfFileName), atomically: true)
            
            let jsonData = NSData(contentsOfURL: NSURL.fileURLWithPath(documentsDirectory.stringByAppendingPathComponent("application.json"))!)
         
            print(documentsDirectory)

            var smtpSession = MCOSMTPSession()
            //GMAIL
            smtpSession.hostname = "smtp.gmail.com";
            smtpSession.port = 465;
            smtpSession.username = "aims.ascertain@gmail.com";
            smtpSession.password = "mUPAM639$";
            smtpSession.authType = MCOAuthType.SASLPlain | MCOAuthType.SASLLogin;
            smtpSession.connectionType = MCOConnectionType.TLS;
            let senderMail = "aims.ascertain@gmail.com"
            let bankMail = "aims.ascertain@gmail.com"
            
            //ASCERTAIN
//            smtpSession.hostname = "mail.ascertain.com.my";
//            smtpSession.port = 587;
//            smtpSession.username = "aims@ascertain.com.my";
//            smtpSession.password = "p0thys21!";
//            smtpSession.authType = MCOAuthType.SASLPlain | MCOAuthType.SASLLogin;
//            smtpSession.connectionType = MCOConnectionType.TLS;
//            let senderMail = "aims@ascertain.com.my"
//            let bankMail = "aims@ascertain.com.my"

            let senderName = "Ascertain"
            
            var bankMessageBuilder = MCOMessageBuilder()
            
            bankMessageBuilder.header.from = MCOAddress(displayName: senderName, mailbox: senderMail)
            bankMessageBuilder.header.to = [
                MCOAddress(displayName: "Bank", mailbox: bankMail)
            ]
            bankMessageBuilder.header.bcc = [
                MCOAddress(displayName: "Bank", mailbox: "ascertain.andev@gmail.com"),
                MCOAddress(displayName: "Bank", mailbox: "josephprasanth@ascertain.com.my"),
                MCOAddress(displayName: "Bank", mailbox: "raj@targetsoft.in"),
                MCOAddress(displayName: "Bank", mailbox: "devi@targetsoft.in"),
                MCOAddress(displayName: "Bank", mailbox: "peter@targetsoft.in")
            ]
            bankMessageBuilder.header.subject = "New application received"
            bankMessageBuilder.htmlBody = "New application received"
            bankMessageBuilder.addAttachment(MCOAttachment(data: PDFdata, filename: pdfFileName))
            bankMessageBuilder.addAttachment(MCOAttachment(data: jsonData, filename: jsonFileName))
            let bankMessageSendOperation = smtpSession.sendOperationWithData(bankMessageBuilder.data())
            self.appDelegate.progressindicator("Sending Mail")
            bankMessageSendOperation.start({ (var error: NSError!) -> Void in
                if(error != nil){
                    self.appDelegate.progressindicator("Sorry, some problem in sending mail")
                    println(error)
                }
                NSOperationQueue.mainQueue().addOperationWithBlock({ () -> Void in
                    self.dismiss()
                })
            })
            
            if let customerMail = application.companyContactPeople?.primaryContactPerson.email.value {
                if let customerName = application.companyContactPeople?.primaryContactPerson.name {
                    var customerMessageBuilder = MCOMessageBuilder()
                    customerMessageBuilder.header.from = MCOAddress(displayName: senderName, mailbox: senderMail)
                    customerMessageBuilder.header.to = [
                        MCOAddress(displayName: customerName, mailbox: customerMail),
                    ]
                    customerMessageBuilder.header.bcc = [
                        MCOAddress(displayName: "Bank", mailbox: "ascertain.andev@gmail.com"),
                        MCOAddress(displayName: "Bank", mailbox: "josephprasanth@ascertain.com.my"),
                        MCOAddress(displayName: "Bank", mailbox: "raj@targetsoft.in"),
                        MCOAddress(displayName: "Bank", mailbox: "devi@targetsoft.in"),
                        MCOAddress(displayName: "Bank", mailbox: "peter@targetsoft.in")
                    ]
                    customerMessageBuilder.header.subject = "Your application has been successfully received"
                    customerMessageBuilder.htmlBody = "Hello \(customerName),<br><br>Your application has been successfully received.<br><br>Regards,<br>Maybank."
                    customerMessageBuilder.addAttachment(MCOAttachment(data: PDFdata, filename: pdfFileName))
                    let customerMessageSendOperation = smtpSession.sendOperationWithData(customerMessageBuilder.data())
                    self.appDelegate.progressindicator("Sending Mail")
                    customerMessageSendOperation.start({ (var error: NSError!) -> Void in
                        if(error != nil){
                            self.appDelegate.progressindicator("Sorry, some problem in sending mail")
                            println(error)
                        }
                        NSOperationQueue.mainQueue().addOperationWithBlock({ () -> Void in
                            self.dismiss()
                        })
                    })
                }
            }
        }
    }
    
    func dismiss() {
       dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}


extension ReportViewController: MFMailComposeViewControllerDelegate, UINavigationControllerDelegate {
    func mailComposeController(controller: MFMailComposeViewController!, didFinishWithResult result: MFMailComposeResult, error: NSError!) {
        controller.dismissViewControllerAnimated(true, completion: { () -> Void in
            
        })
    }
}
