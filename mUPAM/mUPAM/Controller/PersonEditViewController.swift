//
//  PersonEditViewController.swift
//  mUPAM
//
//  Created by Target Soft Systems on 19/11/15.
//  Copyright (c) 2015 Maybank2E. All rights reserved.
//

import UIKit

enum PersonEditViewControllerAdditionalField {
    case Fax, VerificationReference
}

class PersonEditViewController: UIViewController {
    
    let overlay = UIView()
    let viewControllerHeader = UILabel()
    let nameHint = UILabel()
    let name = CharactersTextField(frame: CGRectMake(0.0, 0.0, 534, 30.0), minimumLength: 4, maximumLength: 40)
    let designationHint = UILabel(), designation = CharactersTextField(frame: CGRectMake(0.0, 0.0, 534, 30.0), minimumLength: 4, maximumLength: 40)
    let emailHint = UILabel(), email = EmailTextField(frame: CGRectMake(0.0, 0.0, 534, 30.0), minimumLength: 4, maximumLength: 100)
    let telephoneNumberHint = UILabel(), telephoneNumber = NumericTextField (frame: CGRectMake(0, 0, 534, 30),minimumLength: 9, maximumLength:10)
    let mobileNumberHint = UILabel(), mobileNumber = NumericTextField (frame: CGRectMake(0, 0, 534, 30),minimumLength: 9, maximumLength:10)
    let  areano=UILabel(frame: CGRectMake(0, 0, 50, 50))
    let areano1=UILabel(frame: CGRectMake(0, 0, 50, 50))
    
    let faxNumberHint = UILabel(), faxNumber = NumericTextField (frame: CGRectMake(0, 0, 534, 30),minimumLength: 8, maximumLength:18)
    let verificationReferenceIdHint = UILabel(), verificationReferenceId = CharactersTextField (frame: CGRectMake(0, 0, 534, 30),minimumLength: 6, maximumLength:14)

    var person: Person {
        get {
            var personId = ""
            if let _personId = id {
                personId = _personId
            }
            return Person(id: personId, name: name.text, designation: designation.text, email: EmailContact(id: "", value: email.text), telephone: TelephoneContact(id: "", value:String(format: "%@%@",areano.text!,telephoneNumber.text)), fax: FaxContact(id: "", value: faxNumber.text), mobile: MobileContact(id: "", value:String(format: "%@%@",areano.text!,mobileNumber.text)), verificationReference: VerificationReference(type: VerificationReferenceType(id: "101", name: "ID"), verificationReferenceId: verificationReferenceId.text))
        }
    }
    var id: String?
    
    var additionalFields = [PersonEditViewControllerAdditionalField]()
    
    override init() {
        super.init(nibName: "PersonEditViewController", bundle: nil)
        overlay.backgroundColor = UIColor.grayColor()
        overlay.alpha = 0.2
        nameHint.text = "Name"
        nameHint.sizeToFit()
        name.keyboardType=UIKeyboardType.NamePhonePad
        name.mandatory = true
        name.delegate=self
        
        designationHint.text = "Designation"
        designationHint.sizeToFit()
        
        emailHint.text = "Email"
        emailHint.sizeToFit()
        email.autocapitalizationType = UITextAutocapitalizationType.None
        email.mandatory = true
        email.delegate = self
        
        areano.text = "+60"
        areano1.text="+60"
        telephoneNumberHint.text = "Tel No."
        telephoneNumberHint.sizeToFit()
        telephoneNumber.leftView = areano1
        telephoneNumber.leftViewMode = UITextFieldViewMode.Always
        telephoneNumber.mandatory = true
        telephoneNumber.delegate = self
       
        mobileNumberHint.text = "Mobile No."
        mobileNumberHint.sizeToFit()
        mobileNumber.leftView = areano
        mobileNumber.leftViewMode = UITextFieldViewMode.Always
        mobileNumber.mandatory = true
        mobileNumber.delegate = self
        
        faxNumberHint.text = "Fax No"
        faxNumberHint.sizeToFit()
        faxNumber.delegate = self
        
        verificationReferenceIdHint.text = "ID / Passport No"
        verificationReferenceIdHint.sizeToFit()
        verificationReferenceId.delegate = self
        
//        faxNumberHint.backgroundColor = UIColor.yellowColor()
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    convenience init(title: String, additionalFields: [PersonEditViewControllerAdditionalField]) {
        self.init()
        self.title = title
        self.additionalFields = additionalFields
        
        view.addSubview(nameHint)
        view.addSubview(name)
        view.addSubview(designationHint)
        view.addSubview(designation)
        view.addSubview(emailHint)
        view.addSubview(email)
        view.addSubview(telephoneNumberHint)
        view.addSubview(telephoneNumber)
        view.addSubview(mobileNumberHint)
        view.addSubview(mobileNumber)
        
        for additionalField in additionalFields {
            switch additionalField {
            case .Fax:
                view.addSubview(faxNumberHint)
                view.addSubview(faxNumber)
            case .VerificationReference:
                view.addSubview(verificationReferenceIdHint)
                view.addSubview(verificationReferenceId)
            }
        }
    }

    convenience init(title: String, person: Person, additionalFields: [PersonEditViewControllerAdditionalField]) {
        self.init(title: title, additionalFields: additionalFields)
        updateWithPerson(person)
    }
    
    func clear() {
        id = nil
        name.text = ""
        designation.text = ""
        email.text = ""
        telephoneNumber.text = ""
        mobileNumber.text = ""
        faxNumber.text = ""
    }
    
    func updateWithPerson(person: Person) {
        id = person.id
        name.text = person.name
        designation.text = person.designation
        email.text = person.email.value
        
        let phoneNumber = person.telephone.value
        let newphoneNumber = phoneNumber.stringByReplacingOccurrencesOfString("+60", withString:"", options: NSStringCompareOptions.LiteralSearch, range: nil)
        telephoneNumber.text = newphoneNumber
        let mobilePhoneNumber = person.mobile.value
        let phoneNo=mobilePhoneNumber.stringByReplacingOccurrencesOfString("+60", withString:"", options: NSStringCompareOptions.LiteralSearch, range: nil)
        mobileNumber.text = phoneNo
        if let _fax = person.fax {
            faxNumber.text = _fax.value
        }
        if let _verificationReference = person.verificationReference {
            verificationReferenceId.text = _verificationReference.verificationReferenceId
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.layoutFacade()
    }
    
    func layoutFacade() {
        overlay.fillSuperview()
//        view.groupGrid([nameHint, name, businessRegistrationNumberHint, businessRegistrationNumber, businessAddressHint, businessAddress, businessNatureHint, businessNature, websiteHint, website, telephoneNumberHint, telephoneNumberHint, telephoneNumber, faxNumberHint, faxNumber], fillingWidthWithColumnCount: 2, spacing: 20)
        var hints = [nameHint, designationHint, emailHint, telephoneNumberHint, mobileNumberHint]
        var textFields = [name, designation, email, telephoneNumber, mobileNumber]
        
        for additionalField in additionalFields {
            switch additionalField {
            case .Fax:
                hints.append(faxNumberHint)
                textFields.append(faxNumber)
            case .VerificationReference:
                hints.append(verificationReferenceIdHint)
                textFields.append(verificationReferenceId)
            }
        }
        
        view.groupVertically(hints, inUpperLeftWithLeftPadding: 10, topPadding: 11, spacing: 10, width: 150, height: 30)
        view.groupVertically(textFields, inUpperLeftWithLeftPadding: designationHint.frame.origin.x + 150, topPadding: 11, spacing: 10, width: view.frame.size.width - (150 + 10 + 20), height: 30)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func disableEditing() {
        view.endEditing(true)
        view.addSubview(overlay)
    }

    func enableEditing() {
        overlay.removeFromSuperview()
    }
    
    func validateAndThrowResult() -> Bool {
    
    if(!name.isValid()) {
        name.alert()
        return false
    }
    if(!email.isValid()) {
        email.alert()
        return false
    }
    if(!telephoneNumber.isValid()) {
        telephoneNumber.alert()
        return false
    }
    if(!mobileNumber.isValid()) {
        mobileNumber.alert()
        return false
    }
    if(!(faxNumber.text == "")) {
        if(!faxNumber.isValid()) {
            faxNumber.alert()
            return false
        }
    }
    if(!(verificationReferenceId.text == "")) {
        if(!verificationReferenceId.isValid()) {
            verificationReferenceId.alert()
            return false
        }
    }
      return true
}
        
    
    
    func secondaryValidate()->Bool
    {
        if(!(email.text == "")){
            if(!email.isValid()){
            email.alert()
                return false
            }
            
        }
        if(!(telephoneNumber.text == "")){
           if(!telephoneNumber.isValid()){
            telephoneNumber.alert()
            return false
        }
            
        }
        
        if(!(mobileNumber.text == "")){
          if(!mobileNumber.isValid()){
            mobileNumber.alert()
            return false
        }
            
        }
        if(!(faxNumber.text == "")){
            if(!faxNumber.isValid()){
                faxNumber.alert()
                return false
            }
            
        }
        
        return true
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension PersonEditViewController : UITextFieldDelegate {
   
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        switch textField {
        case let numericTextField as NumericTextField where textField is NumericTextField:
            return (numericTextField as NumericTextField).canAllowReplacementString(string, range: range)
        case let numericTextField where textField is CharactersTextField:
            return (numericTextField as CharactersTextField).canAllowReplacementString(string, range: range)
        case let numericTextField where textField is EmailTextField:
            return (numericTextField as EmailTextField).canAllowReplacementString(string, range: range)
        default:
            return true
        }
    }
}

