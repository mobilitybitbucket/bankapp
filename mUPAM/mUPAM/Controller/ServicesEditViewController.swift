//
//  ServicesEditViewController.swift
//  mUPAM
//
//  Created by Target Soft Systems on 19/11/15.
//  Copyright (c) 2015 Maybank2E. All rights reserved.
//

import UIKit

class ServicesEditViewController: ApplicationFormSectionEditViewController, Editable, Reloadable {
    
    weak var delegate: ServicesEditViewControllerDelegate?
    
    let portfolioSummaryServices = [AccountPortfolioSummaryService(id: "101", title: "Deposit"),
        AccountPortfolioSummaryService(id: "102", title: "Loan"),
        AccountPortfolioSummaryService(id: "103", title: "Investment"),
        AccountPortfolioSummaryService(id: "104", title: "Trade")]
    let paymentManagementServices = [AccountPaymentManagementService(id: "101", title: "Local Payment"),
        AccountPaymentManagementService(id: "102", title: "Outward Telegraphic Transfer. AGREE TO FOREIGN CURRENCY PAYMENT DECLARATION")]
    let transferPurpose = TransferPurpose(id: "Foreign currency payment declaration", title: "We hereby declare that the information given to you herein is accurate and complete and in compliance with the Financial Services Act 2013 and Central Bank of Malaysia Act 2009.  We shall be fully responsible for any inaccurate and incomplete information provided herein.  We also authorise you to make the information available to Bank Negara Malaysia in compliance with the Financial Services Act 2013 and Central Bank of Malaysia Act 2009.")
    
    var selectedPortfolioSummaryServices: [AccountPortfolioSummaryService] = [] {
        didSet {
            services.reloadData()
         }
    }
    var selectedPaymentManagementServices: [AccountPaymentManagementService] = [] {
        didSet {
            services.reloadData()
        }
    }
    let foreignCurrencyPaymentDeclarationDisclosure = UIButton.buttonWithType(UIButtonType.DetailDisclosure) as UIButton
    
    var selectedAuthorizerAccessToViewDetails = false
    
    let services = UITableView(frame: CGRectMake(0, 0, 10, 10), style: .Grouped)
    
    override init() {
        super.init(nibName: "ServicesEditViewController", bundle: nil)
        services.allowsMultipleSelection = true
        services.registerClass(UITableViewCell.self, forCellReuseIdentifier: "portfolioSummaryService")
        services.registerClass(UITableViewCell.self, forCellReuseIdentifier: "paymentManagementService")
        foreignCurrencyPaymentDeclarationDisclosure.addTarget(self, action: "didTapforeignCurrencyPaymentDeclarationDisclosure", forControlEvents: UIControlEvents.TouchUpInside)
    }
    
    convenience init(title:String) {
        self.init()
        self.title = title
        services.delegate = self
        services.dataSource = self
    }
    
    convenience init(title:String, portfolioSummaryServices: [AccountPortfolioSummaryService], paymentManagementServices: [AccountPaymentManagementService]) {        
        self.init()
        if portfolioSummaryServices.count > 0 && paymentManagementServices.count > 0 {
            mode = .Edit
        }
        self.title = title
        selectedPortfolioSummaryServices = portfolioSummaryServices
        selectedPaymentManagementServices = paymentManagementServices
        services.delegate = self
        services.dataSource = self
    }
    
//    required init(coder aDecoder: NSCoder) {
//        fatalError("init(coder:) has not been implemented")
//    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if selectedPortfolioSummaryServices.count == 0 {
            selectedPortfolioSummaryServices.append(portfolioSummaryServices.first!)            
        }
//        navigationController?.navigationBar.translucent = false
//        navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Cancel, target: self, action: "cancel")
//        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Save, target: self, action: "done")
        
        view.addSubview(services)
        services.scrollEnabled = false
//        services.alpha = 0.5
        // Do any additional setup after loading the view.
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.layoutFacade()
    }
    
    func layoutFacade() {
        services.fillSuperviewWithLeftPadding(0, rightPadding: 0, topPadding: 0, bottomPadding: 0)
    }
    
    func cancel() {
        dismissViewControllerAnimated(true, completion: { () -> Void in
            
        })
    }
    
    func done() {
        dismissViewControllerAnimated(true, completion: { () -> Void in
            
        })
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func edit() {
        mode = .Edit
    }
    
    func save() {
        self.delegate?.servicesEditViewController(self, didSelectAccountPortfolioSummaryServices: selectedPortfolioSummaryServices, accountPaymentManagementServices: selectedPaymentManagementServices, accountTransferPurpose: transferPurpose)
    }
    
    func reload() {
        
    }
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    
}

extension ServicesEditViewController : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        switch indexPath.section {
        case 0:
            let portfolioSummaryService = portfolioSummaryServices[indexPath.row]
            var foundIndex: Int? = indexOfPortfolioSummaryService(portfolioSummaryService)
            if let index = foundIndex {
                selectedPortfolioSummaryServices.removeAtIndex(index)
            }
            else {
                selectedPortfolioSummaryServices.append(portfolioSummaryService)
            }
        case 1:
            let paymentManagementService = paymentManagementServices[indexPath.row]
            var foundIndex: Int? = indexOfPaymentManagementService(paymentManagementService)
            if let index = foundIndex {
                selectedPaymentManagementServices.removeAtIndex(index)
            }
            else {
                selectedPaymentManagementServices.append(paymentManagementService)
            }
        default:
            return
        }
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 2
    }
    
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        switch section {
        case 0:
            return "Portfolio Summary View"
        case 1:
            return "Payment Management"
        default:
            return ""
        }
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return portfolioSummaryServices.count
        case 1:
            return paymentManagementServices.count
        default:
            return 0
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let portfolioSummaryService = portfolioSummaryServices[indexPath.row]
            let cell = tableView.dequeueReusableCellWithIdentifier("portfolioSummaryService", forIndexPath: indexPath) as UITableViewCell
            cell.textLabel?.text = portfolioSummaryService.title
            
            if let _ = indexOfPortfolioSummaryService(portfolioSummaryService) {
                cell.accessoryType = UITableViewCellAccessoryType.Checkmark
            }
            else {
                cell.accessoryType = .None
            }
            return cell
        case 1:
            let paymentManagementService = paymentManagementServices[indexPath.row]
            let cell = tableView.dequeueReusableCellWithIdentifier("paymentManagementService", forIndexPath: indexPath) as UITableViewCell
            cell.textLabel?.text = paymentManagementService.title
            
            if let _ = indexOfPaymentManagementService(paymentManagementService) {
                cell.accessoryType = UITableViewCellAccessoryType.Checkmark
                foreignCurrencyPaymentDeclarationDisclosure.frame = CGRectMake(cell.frame.size.width - foreignCurrencyPaymentDeclarationDisclosure.frame.size.width - 60, 12, foreignCurrencyPaymentDeclarationDisclosure.frame.size.width, foreignCurrencyPaymentDeclarationDisclosure.frame.size.height)
                
            }
            else {
                cell.accessoryType = .None
                foreignCurrencyPaymentDeclarationDisclosure.frame = CGRectMake(cell.frame.size.width - foreignCurrencyPaymentDeclarationDisclosure.frame.size.width - 10, 12, foreignCurrencyPaymentDeclarationDisclosure.frame.size.width, foreignCurrencyPaymentDeclarationDisclosure.frame.size.height)
            }
            foreignCurrencyPaymentDeclarationDisclosure.removeFromSuperview()
            cell.addSubview(foreignCurrencyPaymentDeclarationDisclosure)
//            cell.textLabel!.backgroundColor = UIColor.redColor()
            return cell
        default:
            return UITableViewCell()
        }
    }
    
    func indexOfPortfolioSummaryService(portfolioSummaryService: AccountPortfolioSummaryService) -> Int? {
        var foundIndex: Int?
        for i in 0..<selectedPortfolioSummaryServices.count {
            if (selectedPortfolioSummaryServices[i] == portfolioSummaryService) {
                foundIndex = i
                break
            }
        }
        return foundIndex
    }
    
    func indexOfPaymentManagementService(paymentManagementService: AccountPaymentManagementService) -> Int? {
        var foundIndex: Int?
        for i in 0..<selectedPaymentManagementServices.count {
            if (selectedPaymentManagementServices[i] == paymentManagementService) {
                foundIndex = i
                break
            }
        }
        return foundIndex
    }
    
    func didTapforeignCurrencyPaymentDeclarationDisclosure() {
        UIAlertView(title: transferPurpose.id, message: transferPurpose.title, delegate: nil, cancelButtonTitle: "Ok").show()
    }
}

protocol ServicesEditViewControllerDelegate : NSObjectProtocol {
    
    func servicesEditViewController(controller: ServicesEditViewController, didSelectAccountPortfolioSummaryServices portfolioSummaryServices:[AccountPortfolioSummaryService], accountPaymentManagementServices paymentManagementServices:[AccountPaymentManagementService], accountTransferPurpose transferPurpose:TransferPurpose)
}
