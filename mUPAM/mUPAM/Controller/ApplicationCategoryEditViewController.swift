//
//  ApplicationCategoryEditViewController.swift
//  mUPAM
//
//  Created by Target Soft Systems on 23/11/15.
//  Copyright (c) 2015 Maybank2E. All rights reserved.
//

import UIKit

class ApplicationCategoryEditViewController: UIViewController {

    weak var delegate: ApplicationCategoryEditViewControllerDelegate?
    
    let applicationCategoryTableView = UITableView()
    let cifNo = NumericTextField(minimumLength: 7, maximumLength: 7)
    
    let regions = [Region(id: "101", name: "Peninsular Malaysia", packages: [Package(id: "MYR 38", name: "Basic Enquiry"),
        Package(id: "MYR 78", name: "Domestic & International Payment"),
        Package(id: "MYR 128", name: "Domestic & International Payment")]),
        Region(id: "102", name: "East Malaysia", packages: [Package(id: "MYR 40", name: "Basic Enquiry"),
        Package(id: "MYR 60", name: "Domestic & International Payment"),
        Package(id: "MYR 80", name: "Domestic & International Payment")])]
    let segments = [Segment(id: "101", name: "SME"),
        Segment(id: "102", name: "BB"),
        Segment(id: "103", name: "Society"),
        Segment(id: "104", name: "Embassy"),
        Segment(id: "105", name: "PLT")]
    
    var selectedRegion: Region? {
        didSet {
            applicationCategoryTableView.reloadData()
            selectedPackage = nil
        }
    }
    
    var selectedPackage: Package? {
        didSet {
            applicationCategoryTableView.reloadData()
            
        }
    }
    var selectedSegment: Segment? {
        didSet {
            applicationCategoryTableView.reloadData()
        }
    }
    var givenCIFNo: String?
    
    override init() {
        super.init(nibName: "ApplicationCategoryEditViewController", bundle: nil)
        cifNo.mandatory = true
        applicationCategoryTableView.registerClass(UITableViewCell.self, forCellReuseIdentifier: "region")
        applicationCategoryTableView.registerClass(UITableViewCell.self, forCellReuseIdentifier: "package")
        applicationCategoryTableView.registerClass(UITableViewCell.self, forCellReuseIdentifier: "segment")
        applicationCategoryTableView.registerClass(UITableViewCell.self, forCellReuseIdentifier: "cifno")
        applicationCategoryTableView.delegate = self
        applicationCategoryTableView.dataSource = self
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Cancel, target: self, action: "cancel")
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Save, target: self, action: "done")
        
        view.addSubview(applicationCategoryTableView)
        selectedRegion = regions[0]

        // Do any additional setup after loading the view.
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.layoutFacade()
    }
    
    func layoutFacade() {
        applicationCategoryTableView.fillSuperview()
    }
    
    func cancel() {
        self.dismissViewControllerAnimated(true, completion: { () -> Void in
            
        })
    }

    func done() {
        if cifNo.text != "" {
            givenCIFNo = cifNo.text
            if !cifNo.isValid() {
                cifNo.alert()
                return
            }
        }
        if let region = selectedRegion {
            if let package = selectedPackage {
                if let segment = selectedSegment {
                    delegate?.applicationCategoryEditViewController(self, didRequestToCreateApplicationOfCateogry: ApplicationCategory(region: region, package: package, segment: segment, cifNo: givenCIFNo))
                    dismissViewControllerAnimated(true, completion: { () -> Void in
                        
                    })
                }
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

extension ApplicationCategoryEditViewController : UITextFieldDelegate {
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        return true
    }
}

extension ApplicationCategoryEditViewController : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 42
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 4
    }
    
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        switch section {
        case 0:
             return "REGION"
            
        case 1:
            return "PACKAGE"
        case 2:
            return "SEGMENT"
        case 3:
            return "CIF No"
        default:
            return ""
        }
    }

    
   
        

    
        

        
        
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return regions.count
        case 1:
            var packagesCount = 0
            if let selectedRegionPackages = selectedRegion?.packages {
                packagesCount = selectedRegionPackages.count
            }
            return packagesCount
        case 2:
            return segments.count
        case 3:
            return 1
        default:
            return 0
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCellWithIdentifier("region", forIndexPath: indexPath) as UITableViewCell
            cell.textLabel?.text = regions[indexPath.row].name
            if let region = selectedRegion {
                cell.accessoryType = region == regions[indexPath.row] ? UITableViewCellAccessoryType.Checkmark : UITableViewCellAccessoryType.None
            }
            else {
                cell.accessoryType = .None
            }
            return cell
        case 1:
            let cell = tableView.dequeueReusableCellWithIdentifier("package", forIndexPath: indexPath) as UITableViewCell
            if let selectedRegionPackages = selectedRegion?.packages {
                cell.textLabel?.text = "\(selectedRegionPackages[indexPath.row].id) - \(selectedRegionPackages[indexPath.row].name)"
            }
            if let packages = selectedRegion?.packages {
                if let package = selectedPackage {
                    cell.accessoryType = package == packages[indexPath.row] ? UITableViewCellAccessoryType.Checkmark : UITableViewCellAccessoryType.None
                }
            }
            else {
                cell.accessoryType = .None
            }
            return cell
        case 2:
            let cell = tableView.dequeueReusableCellWithIdentifier("segment", forIndexPath: indexPath) as UITableViewCell
            cell.textLabel?.text = segments[indexPath.row].name
            if let segment = selectedSegment {
                cell.accessoryType = segment == segments[indexPath.row] ? UITableViewCellAccessoryType.Checkmark : UITableViewCellAccessoryType.None
            }
            else {
                cell.accessoryType = .None
            }
            return cell            
        case 3:
            let cell = tableView.dequeueReusableCellWithIdentifier("cifno", forIndexPath: indexPath) as UITableViewCell
            cell.contentView.addSubview(cifNo)
            cifNo.fillSuperviewWithLeftPadding(16, rightPadding: 5, topPadding: 5, bottomPadding: 5)
            if let providedCIFNo = givenCIFNo {
                cifNo.text = providedCIFNo
            }
            return cell
        default:
            return UITableViewCell()
        }
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        switch indexPath.section {
        case 0:
            selectedRegion = regions[indexPath.row]
        case 1:
            if let packages = selectedRegion?.packages {
                selectedPackage = packages[indexPath.row]
            }
        case 2:
            selectedSegment = segments[indexPath.row]
        default:
            return
        }
    }
    
}

protocol ApplicationCategoryEditViewControllerDelegate : NSObjectProtocol {
    func applicationCategoryEditViewController(controller: ApplicationCategoryEditViewController, didRequestToCreateApplicationOfCateogry category: ApplicationCategory)
}

