//
//  AuthorizationMatrixEditViewController.swift
//  mUPAM
//
//  Created by Target Soft Systems on 19/11/15.
//  Copyright (c) 2015 Maybank2E. All rights reserved.
//

import UIKit

class AuthorizationMatrixEditViewController: ApplicationFormSectionEditViewController, Editable, Reloadable {
    
    weak var delegate: AuthorizationMatrixEditViewControllerDelegate?

    let otherAuthorizationType = PaddedTextField()
    
    var authorizationTypes: [AuthorizationType] = [
        AuthorizationType(id: "101", name: "Single"),
        AuthorizationType(id: "102", name: "Both to Authorise")
    ]
    
    var selectedAuthorizationType: AuthorizationType {
        didSet {
            if let _customAuthorizationType = customAuthorizationType {
                if !(selectedAuthorizationType == _customAuthorizationType) {
                    otherAuthorizationType.text = ""
                }
            }
            authorizationTypesTableView.reloadSections(NSIndexSet(index: 0), withRowAnimation: UITableViewRowAnimation.None)
        }
    }
    
    var customAuthorizationType: AuthorizationType? {
        didSet {
            if customAuthorizationType == nil {
                authorizationTypesTableView.reloadSections(NSIndexSet(index: 1), withRowAnimation: UITableViewRowAnimation.None)
            }
        }
    }

    let authorizationTypesTableView = UITableView(frame: CGRectMake(0, 0, 10, 10), style: UITableViewStyle.Plain)
    
    override init() {
        otherAuthorizationType.placeholder = "Others"
        authorizationTypesTableView.allowsMultipleSelection = true
        authorizationTypesTableView.registerClass(UITableViewCell.self, forCellReuseIdentifier: "normalCell")
        authorizationTypesTableView.registerClass(UITableViewCell.self, forCellReuseIdentifier: "textfieldCell")
        selectedAuthorizationType = self.authorizationTypes.first!
        super.init(nibName: "AuthorizationMatrixEditViewController", bundle: nil)
        otherAuthorizationType.delegate = self
        authorizationTypesTableView.delegate = self
        authorizationTypesTableView.dataSource = self
    }
    
    convenience init(title:String) {
        self.init()
        self.title = title
    }
    
    convenience init(title:String, authorizationType: AuthorizationType) {
        self.init()
        mode = .Edit        
        self.title = title
        
        var indices = indicesOfAuthorizationTypes(authorizationTypes)
        if (indices.filter { (index) -> Bool in
            return index == authorizationType.id
        }.count == 0) {
            customAuthorizationType = authorizationType
        }
        self.selectedAuthorizationType = authorizationType
    }
    
    func indicesOfAuthorizationTypes(authorizationTypes: [AuthorizationType]) -> [String] {
        var indices = [String]()
        for authorizationType in authorizationTypes {
            indices.append(authorizationType.id)
        }
        return indices
    }
    
//    required init(coder aDecoder: NSCoder) {
//        fatalError("init(coder:) has not been implemented")
//    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.translucent = false
        
        view.addSubview(authorizationTypesTableView)
        authorizationTypesTableView.scrollEnabled = false
        //        services.alpha = 0.5
        // Do any additional setup after loading the view.
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.layoutFacade()
    }
    
    func layoutFacade() {
        authorizationTypesTableView.fillSuperviewWithLeftPadding(0, rightPadding: 0, topPadding: 0, bottomPadding: 0)
    }
    
    func edit() {
        mode = .Edit
    }
    
    func save() {
        self.delegate?.authorizationMatrixEditViewController(self, didRequestToSetAuthorizationType: selectedAuthorizationType)
    }
    
    func reload() {
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    
}

extension AuthorizationMatrixEditViewController : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if(indexPath.row != authorizationTypes.count) {
            self.selectedAuthorizationType = authorizationTypes[indexPath.row]
        }
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 2;
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return authorizationTypes.count
        case 1:
            return 1
        default:
            return 0
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 1:
            let cell = tableView.dequeueReusableCellWithIdentifier("textfieldCell", forIndexPath: indexPath) as UITableViewCell
            if let authorizationType = customAuthorizationType {
                otherAuthorizationType.text = authorizationType.name
            }
            else {
                otherAuthorizationType.text = ""
            }
            cell.contentView.addSubview(otherAuthorizationType)
            otherAuthorizationType.fillSuperviewWithLeftPadding(16, rightPadding: 5, topPadding: 5, bottomPadding: 5)
            return cell
        default:
            let authorizationType = authorizationTypes[indexPath.row]
            let cell = tableView.dequeueReusableCellWithIdentifier("normalCell", forIndexPath: indexPath) as UITableViewCell
            cell.textLabel?.text = authorizationType.name
            if authorizationType == selectedAuthorizationType {
                cell.accessoryType = UITableViewCellAccessoryType.Checkmark
            }
            else {
                cell.accessoryType = .None
            }
            return cell
        }
    }
}

extension AuthorizationMatrixEditViewController : UITextFieldDelegate {
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        var text = textField.text + string;
        if((text != "") && (string == "")) {
            text = (text as NSString).stringByReplacingCharactersInRange(range, withString: "") as String
        }
        if text != "" {
            let _customAuthorizationType = AuthorizationType(id: (text.lowercaseString as NSString).lowercaseString , name: text)
            customAuthorizationType = _customAuthorizationType
            selectedAuthorizationType = _customAuthorizationType
        }
        else {
            customAuthorizationType = nil
            selectedAuthorizationType = authorizationTypes.first!
        }
        return true
    }
    
    func textFieldDidEndEditing(textField: UITextField) {

    }
}

protocol AuthorizationMatrixEditViewControllerDelegate : NSObjectProtocol {
    func authorizationMatrixEditViewController(controller: AuthorizationMatrixEditViewController, didRequestToSetAuthorizationType authorizationType: AuthorizationType)
}