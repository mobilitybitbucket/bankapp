//
//  BankAdministrationDetailEditViewController.swift
//  mUPAM
//
//  Created by Target Soft Systems on 17/11/15.
//  Copyright (c) 2015 Maybank2E. All rights 

import UIKit

class BankAdministrationDetailEditViewController: ApplicationFormSectionEditViewController, Editable, Reloadable {

    weak var delegate: BankAdministrationDetailEditViewControllerDelegate?
    
    let systems = [
        System(id: "101", name: "RCMS"),
        System(id: "102", name: "CMS2")
    ]
    
    let branches = [
        Branch(id: "101", name: "Branch1"),
        Branch(id: "102", name: "Branch2"),
        Branch(id: "101", name: "Branch3"),
        Branch(id: "102", name: "Branch4"),
        Branch(id: "101", name: "Branch5")
    ]
    
    let officerNameHint = UILabel(), officerName = CharactersTextField(frame: CGRectMake(1, 0, 534, 30), minimumLength: 3, maximumLength: 40)
    let providentFundHint = UILabel(), providentFund = CharactersTextField(frame: CGRectMake(1, 0, 534, 30), minimumLength: 3, maximumLength: 20)
    let initiatingBranchHint = UILabel(), initiatingBranch = CharactersTextField(frame: CGRectMake(1, 0, 534, 30), minimumLength: 3, maximumLength: 50)
    let telephoneNumberHint = UILabel(), telephoneNumber = NumericTextField(frame: CGRectMake(1, 0, 534, 30), minimumLength: 9, maximumLength: 10)
    let dateHint = UILabel(), date = NumericTextField(frame: CGRectMake(1, 0, 534, 30), minimumLength: 8, maximumLength: 8)
    let systemHint = UILabel(), system = CharactersTextField(frame: CGRectMake(1, 0, 534, 30), minimumLength: 3, maximumLength: 10)
    let dateFormatter = NSDateFormatter()
    let  areaCode=UILabel(frame: CGRectMake(0, 0, 50, 20))
    let branchPicker = UIPickerView()
    let systemPicker = UIPickerView()
    var selectedDate = NSDate()
    var selectedSystem: System?
    var selectedBranch: Branch?
    
    override init() {
        super.init(nibName: "BankAdministrationDetailEditViewController", bundle: nil)
        officerNameHint.text = "Officer Name"
        officerName.sizeToFit()
        officerName.mandatory = true
        
        providentFundHint.text = "PF No"
        providentFundHint.sizeToFit()
        
        initiatingBranchHint.text = "Initiating Branch"
        initiatingBranchHint.sizeToFit()
        
        telephoneNumberHint.text = "Tel No"
        telephoneNumberHint.sizeToFit()
        
        dateHint.text = "Date"
        dateHint.sizeToFit()
        
        systemHint.text = "System"
        systemHint.sizeToFit()
        
        officerName.keyboardType=UIKeyboardType.NamePhonePad
        officerName.mandatory = true
        officerName.delegate = self
        
        providentFund.keyboardType=UIKeyboardType.NumberPad
        providentFund.mandatory = true
        providentFund.delegate = self
        
        systemPicker.delegate = self
        systemPicker.dataSource = self
        branchPicker.delegate = self
        branchPicker.dataSource = self
        
        initiatingBranch.keyboardType = UIKeyboardType.NamePhonePad
        initiatingBranch.inputView = branchPicker
        initiatingBranch.mandatory = true
        initiatingBranch.delegate = self
        
        areaCode.text = "+60"
        telephoneNumber.leftView = areaCode
        telephoneNumber.leftViewMode = UITextFieldViewMode.Always
        telephoneNumber.mandatory = true
        telephoneNumber.delegate = self
        
        date.mandatory = true
        date.delegate = self
        
        system.keyboardType = UIKeyboardType.NamePhonePad
        system.inputView = systemPicker
        system.mandatory = true
        system.delegate = self
        
        dateFormatter.dateFormat = "dd-MM-yy"
        let datePicker = UIDatePicker()
        datePicker.datePickerMode = .Date
        datePicker.date = NSDate()
        datePicker.addTarget(self, action: "didSelectDate:", forControlEvents: UIControlEvents.ValueChanged)
        selectedDate = datePicker.date
        date.inputView = datePicker
        date.text = dateFormatter.stringFromDate(selectedDate)
        date.mandatory = true
    }

    convenience init(title: String) {
        self.init()
        
        self.title = title
    }
    
    convenience init(title: String, bankAdminstrationDetail: BankAdminstrationDetail) {
        self.init()
        mode = .Edit
        
        self.title = title
        officerName.text = bankAdminstrationDetail.officerName
        providentFund.text = bankAdminstrationDetail.providentFundNumber
        initiatingBranch.text = bankAdminstrationDetail.initiationBranch.name
        let phoneNumber = bankAdminstrationDetail.telephoneNumber
        let  newphoneNumber=phoneNumber.stringByReplacingOccurrencesOfString("+60", withString:"", options: NSStringCompareOptions.LiteralSearch, range: nil)
      

        telephoneNumber.text = newphoneNumber
        selectedDate = bankAdminstrationDetail.date
        date.text = dateFormatter.stringFromDate(selectedDate)
        system.text = bankAdminstrationDetail.system.name
    }
    
    func didSelectDate(sender: UIDatePicker) {
        selectedDate = sender.date
        date.text = dateFormatter.stringFromDate(selectedDate)
    }
    
//    required init(coder aDecoder: NSCoder) {
//        fatalError("init(coder:) has not been implemented")
//    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(officerNameHint)
        view.addSubview(officerName)
        view.addSubview(providentFundHint)
        view.addSubview(providentFund)
        view.addSubview(initiatingBranchHint)
        view.addSubview(initiatingBranch)
        view.addSubview(telephoneNumberHint)
        view.addSubview(telephoneNumber)
        view.addSubview(dateHint)
        view.addSubview(date)
        view.addSubview(systemHint)
        view.addSubview(system)
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.layoutFacade()
    }
    
    func layoutFacade() {
        view.groupVertically([officerNameHint, providentFundHint, initiatingBranchHint, telephoneNumberHint, dateHint, systemHint], inUpperLeftWithLeftPadding: 10, topPadding: 11, spacing: 10, width: 150, height: 30)
        view.groupVertically([officerName, providentFund, initiatingBranch, telephoneNumber, date, system], inUpperLeftWithLeftPadding: initiatingBranchHint.frame.origin.x + 150, topPadding: 11, spacing: 10, width: initiatingBranch.frame.size.width, height: 30)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func edit() {
        mode = .Edit
    }
    
    func save() {
        
        if(!officerName.isValid()) {
            officerName.alert()
            return
        }
        if(!providentFund.isValid()) {
            providentFund.alert()
            return
        }
        if(!initiatingBranch.isValid()) {
            initiatingBranch.alert()
            return
        }
        if(!telephoneNumber.isValid()) {
            telephoneNumber.alert()
            return
        }
        if(!date.isValid()) {
            date.alert()
            return
        }
        if(!system.isValid()) {
            system.alert()
            return
        }
    
        self.delegate?.bankAdministrationDetailEditViewController(self, didRequestToSetBankAdministrationDetail: BankAdminstrationDetail(id:"", officerName:officerName.text, telephoneNumber:String(format: "%@%@",areaCode.text!,telephoneNumber.text), providentFundNumber: providentFund.text, initiationBranch: Branch(id: "", name: initiatingBranch.text), system: System(id: "", name: system.text), date: selectedDate))

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    }
    
    func reload() {
        
    }

}

extension BankAdministrationDetailEditViewController : UITextFieldDelegate {
   
        
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        switch textField {
        case let numericTextField as NumericTextField where textField is NumericTextField:
            return (numericTextField as NumericTextField).canAllowReplacementString(string, range: range)
        case let numericTextField where textField is CharactersTextField:
            return (numericTextField as CharactersTextField).canAllowReplacementString(string, range: range)
        default:
            return true
        }
    }
}

extension BankAdministrationDetailEditViewController : UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch pickerView {
        case let _systemPickerView where _systemPickerView == systemPicker :
            return systems.count
        case let _branchPickerView where _branchPickerView == branchPicker :
            return branches.count
        default :
            return 0
        }
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String! {
        switch pickerView {
        case let _systemPickerView where pickerView == systemPicker :
            return systems[row].name
        case let _branchPickerView where pickerView == branchPicker :
            return branches[row].name
        default :
            return ""
        }
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        switch pickerView {
        case let _systemPickerView where pickerView == systemPicker :
            selectedSystem = systems[row]
            system.text = selectedSystem?.name
        case let _branchPickerView where pickerView == branchPicker :
            selectedBranch = branches[row]
            initiatingBranch.text = selectedBranch?.name
        default :
            break
        }
    }
}


protocol BankAdministrationDetailEditViewControllerDelegate : NSObjectProtocol {
    
    func bankAdministrationDetailEditViewController(controller: BankAdministrationDetailEditViewController, didRequestToSetBankAdministrationDetail bankAdministrationDetail:BankAdminstrationDetail)
}