//
//  AccountEditViewController.swift
//  mUPAM
//
//  Created by Target Soft Systems on 19/11/15.
//  Copyright (c) 2015 Maybank2E. All rights reserved.
//

import UIKit

class AccountEditViewController: UIViewController {
    
    weak var delegate: AccountEditViewControllerDelegate?
    
    let accountId = NumericTextField(minimumLength: 12, maximumLength: 12)
    let saturatoryBodyAccountModuleEmployerNames = [TextField(), TextField(), TextField(), TextField()]
    let saturatoryBodyAccountModuleEmployerCodes = [TextField(), TextField(), TextField(), TextField()]

    
    let modules = [
        AccountModule(id: "101", title: "Account Services"),
        AccountModule(id: "102", title: "Payment"),
        AccountModule(id: "103", title: "Payroll / Saturatory Body Payment (EPF, SOSCO, IRB and Zakat)")
    ]
    
    let saturatoryBodies = [
        SaturatoryBody(id: "101", name: "Social Security Organisation"),
        SaturatoryBody(id: "102", name: "Employees Provident Fund"),
        SaturatoryBody(id: "103", name: "Inland Revenue Board"),
        SaturatoryBody(id: "104", name: "ZAKAT")
    ]
    
    var saturatoryBodyAccountModule: AccountModule {
        return modules.last!
    }
    
    private var users : [AccountUser] = []
    
    var givenAccountId: String?
    var givenSaturatoryBodyAccountModuleEmployerNames: [String] = ["", "", "", ""]
    var givenSaturatoryBodyAccountModuleEmployerCodes: [String] = ["", "", "", ""]
    
    var selectedModules: [AccountModule] = [] {
        didSet {
            accountDetails.reloadData()
        }
    }
    var selectedUsers: [AccountUser] = [] {
        didSet {
            accountDetails.reloadData()
        }
    }
    
    let personEditViewController = PersonEditViewController(title: "dfs", additionalFields: [PersonEditViewControllerAdditionalField.Fax])
    
    let accountDetails = UITableView(frame: CGRectMake(0, 0, 10, 10), style: .Grouped)
    
    override init() {
        super.init(nibName: "AccountEditViewController", bundle: nil)
        accountDetails.registerClass(UITableViewCell.self, forCellReuseIdentifier: "accountid")
        accountDetails.registerClass(UITableViewCell.self, forCellReuseIdentifier: "modules")
        accountDetails.registerClass(UITableViewCell.self, forCellReuseIdentifier: "saturatoryBodies")
        accountDetails.registerClass(UITableViewCell.self, forCellReuseIdentifier: "users")

        accountDetails.allowsMultipleSelection = true
        accountDetails.delegate = self
        accountDetails.dataSource = self
        
        accountId.mandatory = true
        accountId.delegate = self
    }
    
    convenience init(users: [AccountUser]) {
        self.init()
        self.users = users
    }
    
    convenience init(account: Account, users: [AccountUser]) {
        self.init(users: users)
        selectedModules = account.modules
        selectedUsers = account.users
        givenAccountId = account.id
        if let saturatoryBodyPaymentInfos = account.saturatoryBodyPaymentInfos {
            for i in 0..<saturatoryBodyPaymentInfos.count {
                givenSaturatoryBodyAccountModuleEmployerNames[i] = saturatoryBodyPaymentInfos[i].employer.name
                givenSaturatoryBodyAccountModuleEmployerCodes[i] = saturatoryBodyPaymentInfos[i].employer.id
            }
        }
        title = account.id
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        for i in 0..<saturatoryBodyAccountModuleEmployerNames.count {
            saturatoryBodyAccountModuleEmployerNames[i].text = givenSaturatoryBodyAccountModuleEmployerNames[i]
        }
        for i in 0..<saturatoryBodyAccountModuleEmployerCodes.count {
            saturatoryBodyAccountModuleEmployerCodes[i].text = givenSaturatoryBodyAccountModuleEmployerCodes[i]
        }
        navigationController?.navigationBar.translucent = false
        navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Cancel, target: self, action: "cancel")
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Save, target: self, action: "done")
        
        view.addSubview(personEditViewController.view)
        view.addSubview(accountDetails)
        // Do any additional setup after loading the view.
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.layoutFacade()
    }
    
    func layoutFacade() {
        accountDetails.fillSuperviewWithLeftPadding(0, rightPadding: 0, topPadding: 0, bottomPadding: 0)
    }
    
    func cancel() {
        dismissViewControllerAnimated(true, completion: { () -> Void in
            
        })
    }
    
    func done() {
        
        if(!accountId.isValid()) {
            accountId.alert()
        }
        else{
            dismissViewControllerAnimated(true, completion: { () -> Void in
                let person = self.personEditViewController.person
                var saturatoryBodyPaymentInfos: [SaturatoryBodyPaymentInfo] = []
                var index = -1
                if (self.hasSelectedModulesContainsSaturatoryBodyAccountModule()) {
                    for saturatoryBody in self.saturatoryBodies {
                        index += 1                        
                        if(self.saturatoryBodyAccountModuleEmployerNames[index].text != "" && self.saturatoryBodyAccountModuleEmployerCodes[index].text != "") {
                            saturatoryBodyPaymentInfos.append(SaturatoryBodyPaymentInfo(id: "\(index)", saturatoryBody: saturatoryBody, employer: Employer(id: self.saturatoryBodyAccountModuleEmployerCodes[index].text, title: self.saturatoryBodyAccountModuleEmployerNames[index].text)))
                        }
                    }
                }
                if saturatoryBodyPaymentInfos.count > 0 {
                    self.delegate?.accountEditViewController(self, didRequestToCreateAccount: Account(id: self.accountId.text, modules: self.selectedModules, users: self.selectedUsers, saturatoryBodyPaymentInfos: saturatoryBodyPaymentInfos))
                }
                else {
                    self.delegate?.accountEditViewController(self, didRequestToCreateAccount: Account(id: self.accountId.text, modules: self.selectedModules, users: self.selectedUsers))
                }
            })
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func hasSelectedModulesContainsSaturatoryBodyAccountModule() -> Bool {
        for i in 0..<selectedModules.count {
            if (selectedModules[i] == saturatoryBodyAccountModule) {
                return true
            }
        }
        return false
    }
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    
}

extension AccountEditViewController : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        switch indexPath.section {
        case 1:
            if indexPath.row < modules.count {
                let module = modules[indexPath.row]
                var foundIndex: Int?
                for i in 0..<selectedModules.count {
                    if (selectedModules[i] == module) {
                        foundIndex = i
                        break
                    }
                }
                if let index = foundIndex {
                    selectedModules.removeAtIndex(index)
                }
                else {
                    selectedModules.append(module)
                }
                tableView.reloadData()
            }
        case 2:
            let user = users[indexPath.row]
            var foundIndex: Int?
            for i in 0..<selectedUsers.count {
                if (selectedUsers[i] == user) {
                    foundIndex = i
                    break
                }
            }
            if let index = foundIndex {
                selectedUsers.removeAtIndex(index)
            }
            else {
                selectedUsers.append(user)
            }
        default:
            return
        }
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        switch section {
        case 0:
            return "Account Number"
        case 1:
            return "Modules"
        case 2:
            return "Users"
        default:
            return ""
        }
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 1
        case 1:
            return hasSelectedModulesContainsSaturatoryBodyAccountModule() ? modules.count + saturatoryBodies.count : modules.count
        case 2:
            return users.count
        default:
            return 0
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCellWithIdentifier("accountid", forIndexPath: indexPath) as UITableViewCell
            cell.contentView.addSubview(accountId)
            accountId.fillSuperviewWithLeftPadding(16, rightPadding: 5, topPadding: 5, bottomPadding: 5)
            if let providedAccountId = givenAccountId {
                accountId.text = providedAccountId
            }
            return cell
        case 1:
            if indexPath.row < modules.count {
                let module = modules[indexPath.row]
                let cell = tableView.dequeueReusableCellWithIdentifier("modules", forIndexPath: indexPath) as UITableViewCell
                cell.textLabel?.text = module.title
                cell.accessoryType = selectedModules.filter({ (selectedModule) -> Bool in
                    return module == selectedModule
                }).count > 0 ? UITableViewCellAccessoryType.Checkmark : UITableViewCellAccessoryType.None
                return cell
            }
            else {
                let cell = tableView.dequeueReusableCellWithIdentifier("saturatoryBodies", forIndexPath: indexPath) as UITableViewCell
                let row = indexPath.row - modules.count
                cell.textLabel?.text = "  " + saturatoryBodies[row].name + " : "
                let saturatoryBodyAccountModuleEmployerName = saturatoryBodyAccountModuleEmployerNames[row]
                saturatoryBodyAccountModuleEmployerName.frame = CGRectMake(244, 5, 210, 30)
                cell.contentView.addSubview(saturatoryBodyAccountModuleEmployerName)
                saturatoryBodyAccountModuleEmployerName.placeholder = "Employer Name"
//                saturatoryBodyAccountModuleEmployerName.text = givenSaturatoryBodyAccountModuleEmployerNames[row]
                
                let saturatoryBodyAccountModuleEmployerCode = saturatoryBodyAccountModuleEmployerCodes[row]
                saturatoryBodyAccountModuleEmployerCode.frame = CGRectMake(456, 5, 80, 30)
                cell.contentView.addSubview(saturatoryBodyAccountModuleEmployerCode)
                saturatoryBodyAccountModuleEmployerCode.placeholder = "Code"
//                saturatoryBodyAccountModuleEmployerCode.text = givenSaturatoryBodyAccountModuleEmployerCodes[row]
                return cell
            }
        case 2:
            let user = users[indexPath.row]
            let cell = tableView.dequeueReusableCellWithIdentifier("users", forIndexPath: indexPath) as UITableViewCell
            cell.textLabel?.text = user.name
            cell.accessoryType = selectedUsers.filter({ (selectedUser) -> Bool in
                return user == selectedUser
            }).count > 0 ? UITableViewCellAccessoryType.Checkmark : UITableViewCellAccessoryType.None
            return cell
        default:
            return UITableViewCell()
        }
    }
}

extension AccountEditViewController : UITextFieldDelegate {
    
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        switch textField {
        case let numericTextField as NumericTextField where textField is NumericTextField:
            return (numericTextField as NumericTextField).canAllowReplacementString(string, range: range)
            
        default:
            return true
        }
    }
}
protocol AccountEditViewControllerDelegate : NSObjectProtocol {
    func accountEditViewController(controller: AccountEditViewController, didRequestToCreateAccount account: Account)
}

protocol AccountEditViewControllerDatasource : NSObjectProtocol {
    
    func accountUsersForAccountEditViewController(controller: AccountEditViewController) -> [AccountUser]
}