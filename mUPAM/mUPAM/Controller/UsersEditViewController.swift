//
//  UsersEditViewController.swift
//  mUPAM
//
//  Created by Target Soft Systems on 17/11/15.
//  Copyright (c) 2015 Maybank2E. All rights reserved.
//

import UIKit

class UsersEditViewController : ApplicationFormSectionEditViewController, Editable, Reloadable {
    
    weak var delegate: UsersEditViewControllerDelegate?
    
    let usersTableView = UITableView()
    
    var users: [AccountUser] = [] {
        didSet {
            usersTableView.reloadData()
        }
    }
    
    override init() {
        super.init(nibName: "UsersEditViewController", bundle: nil)
        usersTableView.delegate = self
        usersTableView.dataSource = self
    }
    
    convenience init(title: String) {
        self.init()
        self.title = title
    }
    
    convenience init(title: String, users: [AccountUser]) {
        self.init(title: title)
        if users.count > 0{
            mode = .Edit
        }
        self.users = users
    }
    
//    required init(coder aDecoder: NSCoder) {
//        fatalError("init(coder:) has not been implemented")
//    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(usersTableView)
        // Do any additional setup after loading the view.
    }
    
    override func viewDidBecomeInEditMode() {
        super.viewDidBecomeInEditMode()
        var rightBarButtonItems = navigationItem.rightBarButtonItems as [UIBarButtonItem]?
        if rightBarButtonItems?.count == 1 {
            rightBarButtonItems?.append(UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Add, target: self, action: "addUser"))
            navigationItem.rightBarButtonItems = rightBarButtonItems
        }
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.layoutFacade()
    }
    
    func layoutFacade() {
        usersTableView.fillSuperviewWithLeftPadding(0, rightPadding: 0, topPadding: 0, bottomPadding: 0)
    }
    
    func addUser() {
        let userEditViewController = UserEditViewController()
        userEditViewController.delegate = self
        let userEditViewContainerViewController = UINavigationController(rootViewController: userEditViewController)
        userEditViewContainerViewController.modalPresentationStyle = UIModalPresentationStyle.FormSheet
        presentViewController(userEditViewContainerViewController, animated: true) { () -> Void in
            
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func edit() {
        mode = .Edit
    }
    
    func save() {
        self.delegate?.usersEditViewController(self, didRequestToCreateUsers: users)
    }
    
    func reload() {
        
    }
}

extension UsersEditViewController : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return users.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell: UITableViewCell!
        if let userCell = tableView .dequeueReusableCellWithIdentifier("user") as? UITableViewCell {
            cell = userCell
        }
        else {
            cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "user")
        }
        cell.textLabel?.text = users[indexPath.row].name
        return cell
    }
    
    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        switch editingStyle {
        case .Delete:
            let user = users[indexPath.row]
            var foundIndex: Int?
            for i in 0..<users.count {
                if (users[i] == user) {
                    foundIndex = i
                    break
                }
            }
            if let index = foundIndex {
                users.removeAtIndex(index)
            }
            return
        default:
            return
        }
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let userEditViewController = UserEditViewController(user: users[indexPath.row])
        userEditViewController.delegate = self
        let userEditViewContainerViewController = UINavigationController(rootViewController: userEditViewController)
        userEditViewContainerViewController.modalPresentationStyle = UIModalPresentationStyle.FormSheet
        presentViewController(userEditViewContainerViewController, animated: true) { () -> Void in
            
        }
    }
}

extension UsersEditViewController : UserEditViewControllerDelegate {
    
    func userEditViewController(controller: UserEditViewController, didRequestToCreateUser user: AccountUser) {
        var accountUserId: Int = 0
        for index in 0..<users.count {
            var id = (users[index].id as NSString).integerValue
            if (id > accountUserId) {
                accountUserId = id
            }
        }
        var editedUser: AccountUser
        if user.id == "" {
            editedUser = AccountUser(id: "\(accountUserId + 1)", name: user.name, designation: user.designation, email: user.email, telephone: user.telephone, fax: user.fax, mobile: user.mobile, verificationReference: user.verificationReference, right: user.right)
        }
        else {
            editedUser = user
        }
        var foundIndex: Int?
        for i in 0..<users.count {
            if (users[i] == editedUser) {
                foundIndex = i
                break
            }
        }
        if let index = foundIndex {
            users.removeAtIndex(index)
            users.insert(editedUser, atIndex:index)
        }
        else {
            users.append(editedUser)
        }
    }
}

protocol UsersEditViewControllerDelegate : NSObjectProtocol {
    
    func usersEditViewController(controller: UsersEditViewController, didRequestToCreateUsers users: [AccountUser])
}
