//
//  CorporateAdministratorEditViewController.swift
//  mUPAM
//
//  Created by Target Soft Systems on 17/11/15.
//  Copyright (c) 2015 Maybank2E. All rights reserved.
//

import UIKit


class HintSwitch : UIView {
    
    let label: UILabel
    
    var hint: String
    var switcher: UISwitch
    
    init(hint: String, switcher: UISwitch) {
        self.hint = hint
        self.switcher = switcher
        self.label = UILabel()
        self.label.text = self.hint + " : " ;
        self.label.sizeToFit()
        super.init(frame: CGRectMake(0, 0, label.frame.size.width + switcher.frame.size.width, switcher.frame.size.height))
        addSubview(label)
        addSubview(switcher)
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.layoutFacade()
    }
    
    func layoutFacade() {
        label.frame = CGRectMake(0, 0, label.frame.size.width, switcher.frame.size.height)
        switcher.alignToTheRightOf(label, fillingWidthWithLeftAndRightPadding: 0, topPadding: 0, height: switcher.frame.size.height)
    }
    
    override func sizeToFit() {
        super.sizeToFit()
        self.frame = CGRectMake(0, 0, label.frame.size.width + 5 + switcher.frame.size.width, switcher.frame.size.height)
    }
    
}

class CorporateAdministratorEditViewController : ApplicationFormSectionEditViewController, Editable, Reloadable {

    var primaryContactPerson: Person?
    var secondaryContactPerson: Person?
    
    weak var delegate: CorporateAdministratorEditViewControllerDelegate?
    weak var datasource: CorporateAdministratorEditViewControllerDatasource?

    
    var adminMakerPersonEditViewController: PersonEditViewController!
    var adminCheckerPersonEditViewController: PersonEditViewController!
    
    let adminMakerPersonTitle = SubTitleLabel(frame: CGRectMake(10, 0, 150, 30))
    let adminCheckerPersonTitle = SubTitleLabel(frame: CGRectMake(10, 0, 150, 30))

    let adminMakerAndPrimaryContactPersonAreSameConfirmation = UISwitch()
    
    let adminCheckerAndSecondaryContactPersonAreSameConfirmation = UISwitch()
    let adminCheckerAndAdminMakerPersonAreSameConfirmation = UISwitch()

    let adminMakerSameAsPrimaryContactHintSwitch: HintSwitch
    
    let adminCheckerSameAsSecondaryContactHintSwitch: HintSwitch
    let adminCheckerSameAsAdminMakerHintSwitch: HintSwitch
    
    override init() {
        adminCheckerSameAsSecondaryContactHintSwitch = HintSwitch(hint: "Same as secondary contact", switcher: adminCheckerAndSecondaryContactPersonAreSameConfirmation)
        adminCheckerSameAsAdminMakerHintSwitch = HintSwitch(hint: "Same as admin maker", switcher: adminCheckerAndAdminMakerPersonAreSameConfirmation)
        
        adminMakerSameAsPrimaryContactHintSwitch = HintSwitch(hint: "Same as primary contact", switcher: adminMakerAndPrimaryContactPersonAreSameConfirmation)
        
        super.init(nibName:"CorporateAdministratorEditViewController", bundle: nil)

        
        adminMakerAndPrimaryContactPersonAreSameConfirmation.addTarget(self, action: "sameAsPrimaryContactPersonOptionChanged:", forControlEvents: UIControlEvents.ValueChanged)
        
        adminCheckerAndSecondaryContactPersonAreSameConfirmation.addTarget(self, action: "sameAsSecondaryContactPersonOptionChanged:", forControlEvents: UIControlEvents.ValueChanged)
        adminCheckerAndAdminMakerPersonAreSameConfirmation.addTarget(self, action: "sameAsAdminMakerOptionChanged:", forControlEvents: UIControlEvents.ValueChanged)
        
        adminCheckerSameAsAdminMakerHintSwitch.sizeToFit()
        adminCheckerSameAsSecondaryContactHintSwitch.sizeToFit()
    }
    
    convenience init(title: String) {
        self.init()
        self.title = title
        adminMakerPersonEditViewController = PersonEditViewController(title: "ADMIN MAKER", additionalFields: [PersonEditViewControllerAdditionalField.Fax])
        adminCheckerPersonEditViewController = PersonEditViewController(title: "ADMIN CHECKER", additionalFields: [PersonEditViewControllerAdditionalField.Fax])
    }
    
    convenience init(title: String, adminMaker: AdminMaker, adminChecker: AdminChecker) {
        self.init()
        mode = .Edit        
        self.title = title
        adminMakerPersonEditViewController = PersonEditViewController(title: "ADMIN MAKER", person: adminMaker, additionalFields: [PersonEditViewControllerAdditionalField.Fax])
        adminCheckerPersonEditViewController = PersonEditViewController(title: "ADMIN CHECKER", person: adminChecker, additionalFields: [PersonEditViewControllerAdditionalField.Fax])
    }
    
//    required init(coder aDecoder: NSCoder) {
//        fatalError("init(coder:) has not been implemented")
//    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        adminMakerPersonTitle.textAlignment = NSTextAlignment.Left
        adminMakerPersonTitle.text = self.adminMakerPersonEditViewController.title
        view.addSubview(adminMakerPersonTitle)

        addChildViewController(adminMakerPersonEditViewController)
        view.addSubview(adminMakerPersonEditViewController.view)
        adminMakerPersonEditViewController.didMoveToParentViewController(self)
        
        adminCheckerPersonTitle.textAlignment = NSTextAlignment.Left
        adminCheckerPersonTitle.text = self.adminCheckerPersonEditViewController.title
        view.addSubview(adminCheckerPersonTitle)

        view.addSubview(adminMakerSameAsPrimaryContactHintSwitch)
        
        view.addSubview(adminCheckerSameAsAdminMakerHintSwitch)
        view.addSubview(adminCheckerSameAsSecondaryContactHintSwitch)
        
        addChildViewController(adminCheckerPersonEditViewController)
        view.addSubview(adminCheckerPersonEditViewController.view)
        adminCheckerPersonEditViewController.didMoveToParentViewController(self)
        
        // Do any additional setup after loading the view.
    }

    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.layoutFacade()
        
        reload()
    }
    
    func layoutFacade() {
        adminMakerPersonTitle.frame = CGRectMake(10, 0, adminMakerPersonTitle.frame.size.width, 30)
        
        adminMakerPersonEditViewController.view.alignUnder(adminMakerPersonTitle, withLeftPadding: 0, topPadding: 0, width: adminMakerPersonEditViewController.view.frame.size.width, height: adminMakerPersonEditViewController.view.frame.size.height)
        adminCheckerPersonTitle.alignUnder(adminMakerPersonEditViewController.view, withLeftPadding: 10, topPadding: 10, width: adminCheckerPersonTitle.frame.size.width, height: adminCheckerPersonTitle.frame.size.height)

        adminMakerSameAsPrimaryContactHintSwitch.alignToTheRightOf(adminMakerPersonTitle, withLeftPadding: 10, topPadding: 0, width: adminMakerSameAsPrimaryContactHintSwitch.frame.size.width, height: adminMakerSameAsPrimaryContactHintSwitch.frame.size.height)
        
        adminCheckerSameAsAdminMakerHintSwitch.alignToTheRightOf(adminCheckerPersonTitle, withLeftPadding: 10, topPadding: adminCheckerPersonTitle.frame.origin.y, width: adminCheckerSameAsAdminMakerHintSwitch.frame.size.width, height: adminCheckerSameAsAdminMakerHintSwitch.frame.size.height)
        adminCheckerSameAsSecondaryContactHintSwitch.alignToTheRightOf(adminCheckerSameAsAdminMakerHintSwitch, withLeftPadding: 10, topPadding: adminCheckerPersonTitle.frame.origin.y, width: adminCheckerSameAsSecondaryContactHintSwitch.frame.size.width, height: adminCheckerSameAsSecondaryContactHintSwitch.frame.size.height)
        
//        adminCheckerAndSecondaryContactPersonAreSameConfirmation.alignToTheRightOf(adminCheckerPersonTitle, withLeftPadding: 246, topPadding: adminCheckerPersonTitle.frame.origin.y, width: adminCheckerAndSecondaryContactPersonAreSameConfirmation.frame.size.width, height: adminCheckerAndSecondaryContactPersonAreSameConfirmation.frame.size.height)
//        adminCheckerAndSecondaryContactPersonAreSameConfirmationHint.alignToTheRightOf(adminCheckerPersonTitle, withLeftPadding: 20, topPadding: adminCheckerPersonTitle.frame.origin.y, width: adminCheckerAndSecondaryContactPersonAreSameConfirmationHint.frame.size.width, height: adminCheckerAndSecondaryContactPersonAreSameConfirmationHint.frame.size.height)
        
        adminCheckerPersonEditViewController.view.alignUnder(adminCheckerPersonTitle, withLeftPadding: 0, topPadding: 3, width: adminCheckerPersonEditViewController.view.frame.size.width, height: adminCheckerPersonEditViewController.view.frame.size.height)
            
//        view.groupVertically([adminMakerPersonEditViewController.view, adminCheckerPersonEditViewController.view], inUpperLeftWithLeftPadding: 0, topPadding: 0, spacing: 20, width: view.frame.size.width, height: adminMakerPersonEditViewController.view.frame.size.height)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func edit() {
        mode = .Edit
    }
    
    func save() {
        
        if(!adminMakerPersonEditViewController.validateAndThrowResult())
        {
            return
        }
        
        if(!adminCheckerPersonEditViewController.validateAndThrowResult())
        {
            return
        }
        let adminMakerPerson = adminMakerPersonEditViewController.person
        let adminCheckerPerson = adminCheckerPersonEditViewController.person
        self.delegate?.corporateAdministratorEditViewController(self, didRequestToAddCorporateAdminMaker: AdminMaker(id: adminMakerPerson.id, name: adminMakerPerson.name, designation: adminMakerPerson.designation, email: adminMakerPerson.email, telephone: adminMakerPerson.telephone, fax: adminMakerPerson.fax, mobile: adminMakerPerson.mobile, verificationReference: adminMakerPerson.verificationReference), adminChecker: AdminChecker(id: adminCheckerPerson.id, name: adminCheckerPerson.name, designation: adminCheckerPerson.designation, email: adminCheckerPerson.email, telephone: adminCheckerPerson.telephone, fax: adminCheckerPerson.fax, mobile: adminCheckerPerson.mobile, verificationReference: adminCheckerPerson.verificationReference))
    }
    
    func sameAsPrimaryContactPersonOptionChanged(optionSwitch: UISwitch) {
        if(optionSwitch.on) {
            if let _primaryContactPerson = primaryContactPerson {
//                adminMakerPersonEditViewController.disableEditing()
                adminMakerPersonEditViewController.updateWithPerson(_primaryContactPerson)
            }
        }
        else {
//            adminMakerPersonEditViewController.enableEditing()
            adminMakerPersonEditViewController.clear()
        }
    }
    
    func sameAsSecondaryContactPersonOptionChanged(optionSwitch: UISwitch) {
        if(optionSwitch.on) {
            adminCheckerAndAdminMakerPersonAreSameConfirmation.on = false
            if let _secondaryContactPerson = secondaryContactPerson {
//                adminCheckerPersonEditViewController.disableEditing()
                adminCheckerPersonEditViewController.updateWithPerson(_secondaryContactPerson)
            }
        }
        else {
//            adminCheckerPersonEditViewController.enableEditing()
            adminCheckerPersonEditViewController.clear()
        }
    }
    
    func sameAsAdminMakerOptionChanged(optionSwitch: UISwitch) {
        if(optionSwitch.on) {
            adminCheckerAndSecondaryContactPersonAreSameConfirmation.on = false
            let _adminMaker = adminMakerPersonEditViewController.person
//            adminCheckerPersonEditViewController.disableEditing()
            adminCheckerPersonEditViewController.updateWithPerson(_adminMaker)
        }
        else {
//            adminCheckerPersonEditViewController.enableEditing()
            adminCheckerPersonEditViewController.clear()
        }
    }
    
    func reload() {
        primaryContactPerson = self.datasource?.primaryContactPersonForCorporateAdministratorEditViewController(self)
        secondaryContactPerson = self.datasource?.secondaryContactPersonForCorporateAdministratorEditViewController(self)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

protocol CorporateAdministratorEditViewControllerDatasource : NSObjectProtocol {
    
    func primaryContactPersonForCorporateAdministratorEditViewController(controller: CorporateAdministratorEditViewController) -> Person?
    func secondaryContactPersonForCorporateAdministratorEditViewController(controller: CorporateAdministratorEditViewController) -> Person?

}

protocol CorporateAdministratorEditViewControllerDelegate : NSObjectProtocol {
    
    func corporateAdministratorEditViewController(controller: CorporateAdministratorEditViewController, didRequestToAddCorporateAdminMaker adminMaker: AdminMaker, #adminChecker: AdminChecker)
}