//
//  DashboardViewController.swift
//  mUPAM
//
//  Created by Target Soft Systems on 23/10/15.
//  Copyright (c) 2015 Maybank2E. All rights reserved.
//

import UIKit

class DashboardViewController: UIViewController {
    
    weak var delegate: DashboardViewControllerDelegate?
  
    override func viewDidLoad() {
        super.viewDidLoad()

        
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func navigateToManageApplicationForms() {
        self.delegate?.dashboardViewControllerDidRequestToNavigateToManageApplicationForms(self)
    }


}

protocol DashboardViewControllerDelegate : NSObjectProtocol {
    func dashboardViewControllerDidRequestToNavigateToManageApplicationForms(controller: DashboardViewController)
}

