//
//  CompanyEditViewController.swift
//  mUPAM
//
//  Created by Target Soft Systems on 17/11/15.
//  Copyright (c) 2015 Maybank2E. All rights reserved.
//

import UIKit

class CompanyEditViewController: ApplicationFormSectionEditViewController, Editable, Reloadable {

    weak var delegate: CompanyEditViewControllerDelegate?
    
    let nameHint = UILabel()
    let name = CharactersTextField(frame: CGRectMake(0.0, 0.0, 534, 30.0), minimumLength: 1, maximumLength: 68)
    let businessRegistrationNumberHint = UILabel(), businessRegistrationNumber = CharactersTextField(frame: CGRectMake(1, 0, 534, 30), minimumLength: 3, maximumLength: 14)
    let businessAddressHint = UILabel(), businessAddress = CharactersTextField(frame: CGRectMake(4, 0, 534, 30), minimumLength: 8, maximumLength: 180)
    let businessNatureHint = UILabel(), businessNature = CharactersTextField(frame: CGRectMake(1, 0, 534, 30), minimumLength: 2, maximumLength: 100)
    let websiteHint = UILabel(), website = CharactersTextField(frame: CGRectMake(4, 0, 534, 30), minimumLength: 3, maximumLength: 45)
    let telephoneNumberHint = UILabel(), officeTelephoneNumber = NumericTextField(frame: CGRectMake(0, 0, 534, 30), minimumLength: 9, maximumLength: 10)
    let faxNumberHint = UILabel(), faxNumber = NumericTextField(frame: CGRectMake(0, 0, 534, 30), minimumLength: 8, maximumLength: 18)
    //let areaCode=UILabel(0,0,30,30)
    var address: Address?
    let areaCode = UILabel(frame: CGRectMake(0, 0, 50, 20))

    override init() {
        super.init(nibName: "CompanyEditViewController", bundle: nil)
        
        nameHint.text = "Company Name"
        nameHint.sizeToFit()
        name.keyboardType = UIKeyboardType.NamePhonePad
        name.mandatory = true
        name.delegate = self
        
        businessRegistrationNumberHint.text = "Business Reg No."
        businessRegistrationNumberHint.sizeToFit()
        businessRegistrationNumber.keyboardType = UIKeyboardType.NamePhonePad
        businessRegistrationNumber.mandatory = true
        businessRegistrationNumber.delegate = self
        
        businessAddressHint.text = "Business Address"
        businessAddressHint.sizeToFit()
        businessAddress.keyboardType = UIKeyboardType.NamePhonePad
        businessAddress.mandatory = true
        businessAddress.delegate = self
        
        businessNatureHint.text = "Nature of Business"
        businessNatureHint.sizeToFit()
        businessNature.keyboardType = UIKeyboardType.NamePhonePad
        businessNature.mandatory = true
        businessNature.delegate = self
        
        websiteHint.text = "Company Website"
        websiteHint.sizeToFit()
        website.autocapitalizationType=UITextAutocapitalizationType.None
        website.keyboardType = UIKeyboardType.NamePhonePad
        website.delegate = self
        
        telephoneNumberHint.text = "Office Tel No."
        telephoneNumberHint.sizeToFit()
        officeTelephoneNumber.keyboardType = UIKeyboardType.PhonePad
        officeTelephoneNumber.mandatory = true
        officeTelephoneNumber.delegate = self
        areaCode.text = "+60"
        officeTelephoneNumber.leftView = areaCode
        officeTelephoneNumber.leftViewMode=UITextFieldViewMode.Always
      
        
        //telephoneNumber.text="+60"
        
        faxNumberHint.text = "Fax"
        faxNumberHint.sizeToFit()
        faxNumber.keyboardType = UIKeyboardType.PhonePad
        faxNumber.delegate = self
        
//        faxNumberHint.backgroundColor = UIColor.yellowColor()
    }

    convenience init(title: String) {
        self.init()        
        self.title = title
    }
    
    convenience init(title: String, company: Company) {
        self.init(title: title)
        mode = .Edit
        name.text = company.name
        businessRegistrationNumber.text = company.registrationNumber
        businessNature.text = company.businessNature
        website.text = company.website
        let phoneNumber=company.telephone.value
        let  newphoneNumber=phoneNumber.stringByReplacingOccurrencesOfString("+60", withString:"", options: NSStringCompareOptions.LiteralSearch, range: nil)
        officeTelephoneNumber.text = newphoneNumber
        faxNumber.text = company.fax.value
        address = company.businessAddress
        updateAddress(company.businessAddress)
    }
    
//    required init(coder aDecoder: NSCoder) {
//        fatalError("init(coder:) has not been implemented")
//    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.addSubview(nameHint)
        view.addSubview(name)
        view.addSubview(businessRegistrationNumberHint)
        view.addSubview(businessRegistrationNumber)
        view.addSubview(businessAddressHint)
        view.addSubview(businessAddress)
        view.addSubview(businessNatureHint)
        view.addSubview(businessNature)
        view.addSubview(websiteHint)
        view.addSubview(website)
        view.addSubview(telephoneNumberHint)
        view.addSubview(officeTelephoneNumber)
        view.addSubview(faxNumberHint)
        view.addSubview(faxNumber)
        
        // Do any additional setup after loading the view.
    }

    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.layoutFacade()
    }
    
    func layoutFacade() {
        view.groupVertically([nameHint, businessRegistrationNumberHint, businessAddressHint, businessNatureHint, websiteHint, telephoneNumberHint, faxNumberHint], inUpperLeftWithLeftPadding: 10, topPadding: 11, spacing: 10, width: 150, height: 30)
        view.groupVertically([name, businessRegistrationNumber, businessAddress, businessNature, website, officeTelephoneNumber, faxNumber], inUpperLeftWithLeftPadding: businessNatureHint.frame.origin.x + 150, topPadding: 11, spacing: 10, width: businessNature.frame.size.width, height: 30)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func edit() {
        mode = .Edit        
    }
    
    func save() {
        if (!name.isValid()) {
            name.alert()
            return
        }
        if (!businessRegistrationNumber.isValid()) {
            businessRegistrationNumber.alert()
            return
        }
        if address == nil {
            businessAddress.alert()
            return
        }
        if (!businessNature.isValid()) {
            businessNature.alert()
            return
        }

        if (!officeTelephoneNumber.isValid()) {
            officeTelephoneNumber.alert()
            return
        }
        if(!(faxNumber.text == "")) {
            if(!faxNumber.isValid()) {
                faxNumber.alert()
                return 
            }
            
        }
        self.delegate?.companyEditViewController(self, didRequestToSaveCompany: Company(name: name.text, registrationNumber: businessRegistrationNumber.text, businessAddress: address!, businessNature: businessNature.text, email: nil, telephone: TelephoneContact(id: "", value:String(format: "%@%@",areaCode.text!,officeTelephoneNumber.text)), fax: FaxContact(id: "", value: faxNumber.text), website: website.text))
    }
    
    func updateAddress(address: Address) {
        businessAddress.text = "\(address.lowLevelSpatialReference), \(address.highLevelSpatialReference), \(address.state.name), \(address.country.name)"
    }

    func reload() {
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension CompanyEditViewController : UITextFieldDelegate {
    func textFieldDidBeginEditing(textField: UITextField) {
        if textField == businessAddress {
            textField.resignFirstResponder()
            var addressEditViewController: AddressEditViewController!
            if let _address = self.address {
                addressEditViewController = AddressEditViewController(address:_address)
            }
            else {
                addressEditViewController = AddressEditViewController()
            }
            addressEditViewController.delegate = self
            let addressEditViewNavigationController = UINavigationController(rootViewController: addressEditViewController)
            addressEditViewNavigationController.modalPresentationStyle = UIModalPresentationStyle.FormSheet
            presentViewController(addressEditViewNavigationController, animated: true) { () -> Void in
                
            }
        }
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        switch textField {
        case let numericTextField as NumericTextField where textField is NumericTextField:
            return (numericTextField as NumericTextField).canAllowReplacementString(string, range: range)
        case let numericTextField where textField is CharactersTextField:
            return (numericTextField as CharactersTextField).canAllowReplacementString(string, range: range)
        default:
            return true
        }
    }
}

extension CompanyEditViewController : AddressEditViewControllerDelegate {
    func addressEditViewController(controller: AddressEditViewController, didEditAddress address: Address) {
        self.address = address
        updateAddress(address)
    }
}
    
protocol CompanyEditViewControllerDelegate : NSObjectProtocol {
    func companyEditViewController(controller: CompanyEditViewController, didRequestToSaveCompany company:Company)
}

