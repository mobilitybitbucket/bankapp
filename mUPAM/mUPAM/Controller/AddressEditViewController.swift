//
//  AddressEditViewController.swift
//  mUPAM
//
//  Created by Target Soft Systems on 19/11/15.
//  Copyright (c) 2015 Maybank2E. All rights reserved.
//

import UIKit

class AddressEditViewController: UIViewController {
    
    let lowLevelSpatialReference = CharactersTextField(minimumLength: 1, maximumLength: 68)
    let highLevelSpatialReference = CharactersTextField(minimumLength: 1, maximumLength: 68)
    let state = CharactersTextField(minimumLength: 1, maximumLength: 68)
    let country = CharactersTextField(minimumLength: 1, maximumLength: 68)
    
    let givenHighLevelSpatialReference: String?
    let givenLowLevelSpatialReference: String?
    let givenState: String?
    let givenCountry: String?
    
    var givenAccountId : String?
    let addressDetails = UITableView(frame: CGRectMake(0, 0, 10, 10), style: .Grouped)
    
    var address: Address? {
        didSet {
            addressDetails.reloadData()
        }
    }
    
    weak var delegate: AddressEditViewControllerDelegate?
    
    override init() {
        super.init(nibName: "AddressEditViewController", bundle: nil)
        addressDetails.registerClass(UITableViewCell.self, forCellReuseIdentifier: "lowLevelSpatialReference")
        addressDetails.registerClass(UITableViewCell.self, forCellReuseIdentifier: "highLevelSpatialReference")
        addressDetails.registerClass(UITableViewCell.self, forCellReuseIdentifier: "state")
        addressDetails.registerClass(UITableViewCell.self, forCellReuseIdentifier: "country")
        
        addressDetails.delegate = self
        addressDetails.dataSource = self
        addressDetails.allowsSelection = false
        
        lowLevelSpatialReference.placeholder = "Address Line 1"
        lowLevelSpatialReference.mandatory = true
        
        highLevelSpatialReference.placeholder = "Address Line 2"
        highLevelSpatialReference.mandatory = true
        
        state.mandatory = true
        
        country.mandatory = true
    }

    convenience init(address: Address) {
        self.init()
        self.address = address
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.translucent = false
        navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Cancel, target: self, action: "cancel")
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Save, target: self, action: "done")
        
        view.addSubview(addressDetails)
    
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardDidShow:", name: UIKeyboardDidShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardWillHide:", name: UIKeyboardWillHideNotification, object: nil)
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.layoutFacade()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        highLevelSpatialReference.becomeFirstResponder()        
    }
    
    func layoutFacade() {
        addressDetails.fillSuperviewWithLeftPadding(0, rightPadding: 0, topPadding: 0, bottomPadding: 0)
    }
    
    func cancel() {
        dismissViewControllerAnimated(true, completion: { () -> Void in
            
        })
    }
    
    func done() {
        if (!lowLevelSpatialReference.isValid()) {
            lowLevelSpatialReference.alert()
            return
        }
        if (!highLevelSpatialReference.isValid()) {
            highLevelSpatialReference.alert()
            return
        }
        if (!state.isValid()) {
            state.alert()
            return
        }
        if (!country.isValid()) {
            country.alert()
            return
        }
        
        self.delegate?.addressEditViewController(self, didEditAddress: Address(lowLevelSpatialReference: lowLevelSpatialReference.text, highLevelSpatialReference: highLevelSpatialReference.text, state: State(id: "", name: state.text), country: Country(id: "", name: country.text)))
        
        dismissViewControllerAnimated(true, completion: { () -> Void in
            
        })
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func keyboardDidShow(notification: NSNotification) {
        if let userInfo = notification.userInfo {
            if let keyboardFrameValue = userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue {
                let keyboardFrame = keyboardFrameValue.CGRectValue()
                addressDetails.contentSize = CGSizeMake(addressDetails.contentSize.width, view.frame.size.height + 100)
            }
        }
    }
    
    func keyboardWillHide(notification: NSNotification) {
        if let userInfo = notification.userInfo {
            if let keyboardFrameValue = userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue {
                let keyboardFrame = keyboardFrameValue.CGRectValue()
                addressDetails.contentSize = view.frame.size
                addressDetails.setContentOffset(CGPointMake(addressDetails.contentOffset.x, addressDetails.contentOffset.y - keyboardFrame.height < 0 ? 0 : addressDetails.contentOffset.y - keyboardFrame.height), animated: true)
            }
        }
    }
    
    deinit {
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardDidShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillHideNotification, object: nil)
    }
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    
}

extension AddressEditViewController : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        switch section {
        case 0:
            return "Address Lines"
        case 1:
            return "State"
        case 2:
            return "Country"
        default:
            return ""
        }
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 2
        case 1:
            return 1
        case 2:
            return 1
        default:
            return 0
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            switch indexPath.row {
            case 0:
                let cell = tableView.dequeueReusableCellWithIdentifier("lowLevelSpatialReference", forIndexPath: indexPath) as UITableViewCell
                cell.contentView.addSubview(lowLevelSpatialReference)
                lowLevelSpatialReference.fillSuperviewWithLeftPadding(16, rightPadding: 5, topPadding: 5, bottomPadding: 5)
                if let providedLowLevelSpatialReference = givenLowLevelSpatialReference {
                    lowLevelSpatialReference.text = providedLowLevelSpatialReference
                }
                else {
                    lowLevelSpatialReference.text=address?.lowLevelSpatialReference
                }
                return cell
            case 1:
                let cell = tableView.dequeueReusableCellWithIdentifier("highLevelSpatialReference", forIndexPath: indexPath) as UITableViewCell
                cell.contentView.addSubview(highLevelSpatialReference)
                highLevelSpatialReference.fillSuperviewWithLeftPadding(16, rightPadding: 5, topPadding: 5, bottomPadding: 5)
                if let providedHighLevelSpatialReference = givenHighLevelSpatialReference {
                    highLevelSpatialReference.text = providedHighLevelSpatialReference
                }
                else {
                    highLevelSpatialReference.text=address?.highLevelSpatialReference
                }
                
                return cell
            default:
                return UITableViewCell()
            }
        case 1:
            let cell = tableView.dequeueReusableCellWithIdentifier("state", forIndexPath: indexPath) as UITableViewCell
            cell.contentView.addSubview(state)
            state.fillSuperviewWithLeftPadding(16, rightPadding: 5, topPadding: 5, bottomPadding: 5)
            if let providedState = givenState {
                state.text = providedState
            }
            else
            {
               state.text=address?.state.name
            }
            return cell
        case 2:
            let cell = tableView.dequeueReusableCellWithIdentifier("country", forIndexPath: indexPath) as UITableViewCell
            cell.contentView.addSubview(country)
            country.fillSuperviewWithLeftPadding(16, rightPadding: 5, topPadding: 5, bottomPadding: 5)
            if let providedCountry = givenCountry {
                country.text = providedCountry
            }
            else
            {
                country.text=address?.country.name
            }
            return cell
        default:
            return UITableViewCell()
        }
    }
}

protocol AddressEditViewControllerDelegate : NSObjectProtocol {
    func addressEditViewController(controller: AddressEditViewController, didEditAddress address: Address)
}
