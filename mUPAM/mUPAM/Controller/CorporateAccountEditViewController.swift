//
//  CorporateAccountEditViewController.swift
//  mUPAM
//
//  Created by Target Soft Systems on 17/11/15.
//  Copyright (c) 2015 Maybank2E. All rights reserved.
//

import UIKit

class CorporateAccountEditViewController : ApplicationFormSectionEditViewController, Editable, Reloadable {

    weak var delegate: CorporateAccountEditViewControllerDelegate?
    
    let accountNumberHint = UILabel()
    let accountNumber = NumericTextField(frame: CGRectMake(0, 0, 534, 30), minimumLength: 12, maximumLength: 16)
    let residencyHint = UILabel()
    let residency = UISwitch()
    
    override init() {
        super.init(nibName: "CorporateAccountEditViewController", bundle: nil)
        accountNumberHint.text = "Primary Billing Account"
        accountNumberHint.sizeToFit()
        accountNumber.mandatory = true
        accountNumber.delegate = self
        
        residencyHint.text = "Non-Resident ?"
        residencyHint.sizeToFit()
//        residencyHint.backgroundColor = UIColor.yellowColor()
    }
    
    convenience init(title: String) {
        self.init()
        self.title = title
    }
    
    convenience init(title: String, billingAccount: BillingAccount) {
        self.init()
        mode = .Edit
        self.title = title
        accountNumber.text = billingAccount.id
        residency.on = billingAccount.isNonResident
    }
    
    
//    required init(coder aDecoder: NSCoder) {
//        fatalError("init(coder:) has not been implemented")
//    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(accountNumberHint)
        view.addSubview(accountNumber)
        view.addSubview(residencyHint)
        view.addSubview(residency)
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.layoutFacade()
    }
    
    func layoutFacade() {
        view.groupVertically([accountNumberHint, residencyHint], inUpperLeftWithLeftPadding: 10, topPadding: 11, spacing: 10, width: 200, height: 30)
        view.groupVertically([accountNumber, residency], inUpperLeftWithLeftPadding: accountNumberHint.frame.origin.x + 180, topPadding: 11, spacing: 10, width: 200, height: 30)
      
    }
    
    func edit() {
        mode = .Edit
    }
    
    func save() {

        if (!accountNumber.isValid()) {
            accountNumber.alert()
            return
        }
        self.delegate?.corporateAccountEditViewController(self, didRequestToSetCorporateBillingAccount: BillingAccount(id: accountNumber.text, isNonResident: residency.on))
    }
    
    
    func reload() {
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}


extension CorporateAccountEditViewController : UITextFieldDelegate {
    
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        switch textField {
        case let numericTextField as NumericTextField where textField is NumericTextField:
            return (numericTextField as NumericTextField).canAllowReplacementString(string, range: range)
       
        default:
            return true
        }
    }
}

protocol CorporateAccountEditViewControllerDelegate : NSObjectProtocol {
    
    func corporateAccountEditViewController(controller: CorporateAccountEditViewController, didRequestToSetCorporateBillingAccount corporateBillingAccount: BillingAccount)
}