//
//  ApplicationsViewController.swift
//  mUPAM
//
//  Created by Target Soft Systems on 16/11/15.
//  Copyright (c) 2015 Maybank2E. All rights reserved.
//

import UIKit

class ApplicationsViewController: UIViewController {

    weak var delegate: ApplicationsViewControllerDelegate?
    
    let applicationsTableView = UITableView()
    
    var applications: [Application] = [] {
        didSet {
            applicationsTableView.reloadData()
        }
    }

    override init() {
        super.init(nibName: "ApplicationsViewController", bundle: nil)
    }
    
    convenience init(title: String) {
        self.init()
        self.title = title
    }
    
    convenience init(title: String, applications: [Application]) {
        self.init(title: title)
        self.applications = applications
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        applicationsTableView.registerNib(UINib(nibName: "ApplicationTableViewCell", bundle: nil), forCellReuseIdentifier: "ApplicationTableViewCell")
        applicationsTableView.delegate = self
        applicationsTableView.dataSource = self
        view.addSubview(applicationsTableView)
        self.navigationController?.navigationBar.translucent=false;
       // title = "Applications"
        let image : UIImage = UIImage(named: "maybank-logo-transparent.png")!
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        imageView.contentMode = .ScaleAspectFit
        imageView.image = image
        self.navigationItem.titleView = imageView
     
       
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Add, target: self, action: "add")
        // Do any additional setup after loading the view.
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.layoutFacade()
    }
    
    func layoutFacade() {
        applicationsTableView.fillSuperview()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func add() {
        let applicationCategoryEditViewController = ApplicationCategoryEditViewController()
        applicationCategoryEditViewController.delegate = self
        let navigationViewController = UINavigationController(rootViewController: applicationCategoryEditViewController)
        navigationViewController.navigationBar.translucent = false
        navigationViewController.modalPresentationStyle = UIModalPresentationStyle.FormSheet
        presentViewController(navigationViewController, animated: true) { () -> Void in
            
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

protocol ApplicationsViewControllerDatasouce : NSObjectProtocol {
    func applicationsForApplicationsViewController(controller: ApplicationsViewController)
}
protocol ApplicationsViewControllerDelegate : NSObjectProtocol {
    func applicationsViewControllerDidUpdateApplications(controller: ApplicationsViewController)
}

extension ApplicationsViewController : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return applications.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("ApplicationTableViewCell", forIndexPath: indexPath) as ApplicationTableViewCell
        let application = applications[indexPath.row]
        var attributedTitle = NSMutableAttributedString()
        
        if let companyName = application.company?.name {
            var companyNameDetail = NSMutableAttributedString(string: "Company : \(companyName)\n")
            companyNameDetail.addAttribute(NSFontAttributeName, value: UIFont(name: "SFUIDisplay-Medium", size: 18)!, range: NSMakeRange(0, countElements(companyNameDetail.string)))
            attributedTitle.appendAttributedString(companyNameDetail)
        }
        
        if let cifNo = application.category.cifNo {
            var cifNoDetail = NSMutableAttributedString(string: "CIF No : \(cifNo)\n")
            cifNoDetail.addAttribute(NSFontAttributeName, value: UIFont(name: "SFUIDisplay-Medium", size: 18)!, range: NSMakeRange(0, countElements(cifNoDetail.string)))
            attributedTitle.appendAttributedString(cifNoDetail)
        }
        
        var applicationCatgoryDetail = (NSMutableAttributedString(string: "\(application.category.region.name) - " + "\n"
            + "\(application.category.package.name) - " + "\n"
            + "\(application.category.segment.name)"))
        applicationCatgoryDetail.addAttribute(NSForegroundColorAttributeName, value: UIColor.darkGrayColor(), range: NSMakeRange(0, countElements(applicationCatgoryDetail.string)))
        attributedTitle.appendAttributedString(applicationCatgoryDetail)
        

        var paragraphStyle = NSMutableParagraphStyle()
//        paragraphStyle.minimumLineHeight = 30.0
//        paragraphStyle.maximumLineHeight = 30.0
        paragraphStyle.lineSpacing = 4.0
//        paragraphStyle.lineHeightMultiple = 2.0
        attributedTitle.addAttribute(NSParagraphStyleAttributeName, value: paragraphStyle, range: NSMakeRange(0, countElements(attributedTitle.string)))
//        attributedTitle.addAttribute(NSForegroundColorAttributeName, value: UIColor.redColor(), range: NSMakeRange(0, 50))

        cell.title?.attributedText = attributedTitle
        return cell
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 120
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let application = applications[indexPath.row]
        if let viewControllers = splitViewController?.viewControllers {
            var contentViewControllers = viewControllers
            let applicationFormEditViewController = ApplicationFormEditViewController(application: application)
            applicationFormEditViewController.delegate = self
            let navigationController = UINavigationController(rootViewController: applicationFormEditViewController)
            navigationController.navigationBar.translucent = false
            contentViewControllers[1] = navigationController
            splitViewController?.viewControllers = contentViewControllers
        }
    }
}

extension ApplicationsViewController : ApplicationCategoryEditViewControllerDelegate {
    
    func applicationCategoryEditViewController(controller: ApplicationCategoryEditViewController, didRequestToCreateApplicationOfCateogry category: ApplicationCategory) {
        let application = Application(applicationId: "\(applications.count + 1)", category: category)
        
        EntitySQLiteCoreDataStore(id: "101").manageApplication(application)
        applications.append(application)
    }
}

extension ApplicationsViewController : ApplicationFormEditViewControllerDelegate {
    func applicationFormEditViewControllerDidUpdateApplications(controller: ApplicationFormEditViewController) {
        self.delegate?.applicationsViewControllerDidUpdateApplications(self)
    }
}