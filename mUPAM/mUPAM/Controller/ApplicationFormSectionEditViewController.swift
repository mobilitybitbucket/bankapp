//
//  ApplicationFormSectionEditViewController.swift
//  mUPAM
//
//  Created by Target Soft Systems on 11/12/15.
//  Copyright (c) 2015 Maybank2E. All rights reserved.
//

import UIKit

enum ApplicationFormSectionViewMode {
    case New, Edit, View
}

class ApplicationFormSectionEditViewController: UIViewController {
    
    var initialHeight: CGFloat = 0
        
    var mode: ApplicationFormSectionViewMode = .New {
        didSet {
            switch mode {
            case .New:
                navigationItem.rightBarButtonItem = nil
            case .Edit:
                navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Save, target: self, action: "save")
                viewDidBecomeInEditMode()
            case .View:
                navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Edit, target: self, action: "edit")
            }
        }
    }
    
    func viewDidBecomeInEditMode() {
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

@objc protocol Reloadable : NSObjectProtocol {
    
    func reload()
}


protocol Editable : NSObjectProtocol {

    func save()
    func edit()
}
