//
//  UserEditViewController.swift
//  mUPAM
//
//  Created by Target Soft Systems on 19/11/15.
//  Copyright (c) 2015 Maybank2E. All rights reserved.
//

import UIKit

class UserEditViewController: UIViewController {

    weak var delegate: UserEditViewControllerDelegate?
    
    let permissions = [AccountUserPermission(id: "101", title: "Viewer"),
        AccountUserPermission(id: "102", title: "Data Entry"),
        AccountUserPermission(id: "103", title: "Authorizer")]
    
    let dataEntryPermission: AccountUserPermission
    let authorizerPermission: AccountUserPermission
    
    let roles = [AccountUserRole(id: "101", title: "Super User"),
        AccountUserRole(id: "102", title: "Role Restricted"),
        AccountUserRole(id: "103", title: "Restricted")]
    
    let approvalGroups = [AccountUserApprovalGroup(id: "101", name: "A"),
        AccountUserApprovalGroup(id: "102", name: "B"),
        AccountUserApprovalGroup(id: "103", name: "C"),
        AccountUserApprovalGroup(id: "104", name: "D"),
        AccountUserApprovalGroup(id: "105", name: "E")]
    
    var selectedRole: AccountUserRole? {
        didSet {
            nominationDetail.reloadData()
        }
    }
    
    var canAuthorizerViewDetails: Bool = false {
        didSet {
            nominationDetail.reloadData()
        }
    }
    
    var selectedApprovalGroup: AccountUserApprovalGroup? {
        didSet {
            nominationDetail.reloadData()
        }
    }
    var selectedPermissions: [AccountUserPermission] = [] {
        didSet {
            if find(selectedPermissions, authorizerPermission) == nil {
                canAuthorizerViewDetails = false
            }
            nominationDetail.reloadData()
        }
    }
    var selectedAuthorizerAccessToViewDetails = false
    
    var personEditViewController: PersonEditViewController!
    
    let nominationDetail = UITableView(frame: CGRectMake(0, 0, 10, 10), style: .Grouped)

    override init() {
        dataEntryPermission = permissions[1];
        authorizerPermission = permissions[2];
        super.init(nibName: "UserEditViewController", bundle: nil)
        personEditViewController = PersonEditViewController(title: "dfs", additionalFields: [PersonEditViewControllerAdditionalField.VerificationReference])
        nominationDetail.allowsMultipleSelection = true
        nominationDetail.registerClass(UITableViewCell.self, forCellReuseIdentifier: "role")
        nominationDetail.registerClass(UITableViewCell.self, forCellReuseIdentifier: "approvalGroup")
        nominationDetail.registerClass(UITableViewCell.self, forCellReuseIdentifier: "permission")
        nominationDetail.registerClass(UITableViewCell.self, forCellReuseIdentifier: "authorizerAccess")
        nominationDetail.delegate = self
        nominationDetail.dataSource = self
    }
    
    convenience init(user: AccountUser) {
        self.init()
        personEditViewController = PersonEditViewController(title: "dfs", person: user, additionalFields: [PersonEditViewControllerAdditionalField.VerificationReference])
        if let _role = user.right.role {
            selectedRole = _role
        }
        if let _approvalGroup = user.right.approvalGroup {
            selectedApprovalGroup = _approvalGroup
        }
        selectedPermissions = user.right.permissions
        canAuthorizerViewDetails = user.right.canAuthorizerViewDetails
        title = user.name
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.translucent = false
        navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Cancel, target: self, action: "cancel")
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Save, target: self, action: "done")
        
        view.addSubview(personEditViewController.view)
        view.addSubview(nominationDetail)
        // Do any additional setup after loading the view.
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.layoutFacade()
    }
    
    func layoutFacade() {
        personEditViewController.view.anchorTopLeftWithLeftPadding(0, topPadding: 0, width: view.frame.size.width, height: personEditViewController.view.frame.size.height)
        nominationDetail.alignUnder(personEditViewController.view, centeredFillingWidthAndHeightWithLeftAndRightPadding: 0, topAndBottomPadding: 0)
//        view.groupVertically([personEditViewController.view, nominationDetail], inUpperLeftWithLeftPadding: 0, topPadding: 0, spacing: 10, width: view.frame.size.width, height: view.frame.size.height)
    }

    func cancel() {
        dismissViewControllerAnimated(true, completion: { () -> Void in
            
        })
    }
    
    func done() {
        
        let personview = personEditViewController
        
        if(!personview.validateAndThrowResult()) {
            return
        }
        
        if let _ = find(selectedPermissions, dataEntryPermission) {
            if let _ = find(selectedPermissions, authorizerPermission) {
                if selectedRole == nil {
                    nominationDetail.scrollToRowAtIndexPath(NSIndexPath(forRow: 0, inSection: 2), atScrollPosition: UITableViewScrollPosition.Top, animated: false)
                    UIAlertView(title: nil, message: "Role is required for user with permission 'Data Entry' & 'Authorizer'", delegate: nil, cancelButtonTitle: "Ok").show()
                    return
                }
                else if (selectedApprovalGroup == nil) {
                    nominationDetail.scrollToRowAtIndexPath(NSIndexPath(forRow: 0, inSection: 3), atScrollPosition: UITableViewScrollPosition.Top, animated: false)
                    UIAlertView(title: nil, message: "Approval group is required for user with permission 'Data Entry' & 'Authorizer'", delegate: nil, cancelButtonTitle: "Ok").show()
                    return
                }
            }
        }
        dismissViewControllerAnimated(true, completion: { () -> Void in
            let person = self.personEditViewController.person
            var verificationRefereceId = person.id
            self.delegate?.userEditViewController(self, didRequestToCreateUser: AccountUser(id: person.id, name: person.name, designation: person.designation, email: person.email, telephone: person.telephone, fax: person.fax, mobile: person.mobile, verificationReference: person.verificationReference, right: AccountUserRight(role: self.selectedRole, permissions: self.selectedPermissions, canAuthorizerViewDetails: self.canAuthorizerViewDetails, approvalGroup: self.selectedApprovalGroup)))
        })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension UserEditViewController : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        switch indexPath.section {
        case 0:
            let permission = permissions[indexPath.row]
            var foundIndex: Int?
            for i in 0..<selectedPermissions.count {
                if (selectedPermissions[i] == permission) {
                    foundIndex = i
                    break
                }
            }
            if let index = foundIndex {
                selectedPermissions.removeAtIndex(index)
            }
            else {
                selectedPermissions.append(permission)
            }
        case 1:
            canAuthorizerViewDetails = !canAuthorizerViewDetails
        case 2:
            let rowRole = roles[indexPath.row]
            selectedRole = selectedRole == rowRole ? nil : rowRole
        case 3:
            let rowApprovalGroup = approvalGroups[indexPath.row]
            selectedApprovalGroup = selectedApprovalGroup == rowApprovalGroup ? nil : rowApprovalGroup
        default:
            return
        }
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 4;
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        let height: CGFloat = 30
        switch section {
        case 1:
            if let _ = find(selectedPermissions, authorizerPermission) {
                return height
            }
            return 0.001;
        default:
            return height
        }
    }
    
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        let height: CGFloat = 20
        switch section {
        case 1:
            if let _ = find(selectedPermissions, authorizerPermission) {
                return height
            }
            return 0.001;
        default:
            return height
        }
    }
    

    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {

        switch section {
        case 0:
            return "User Function"
        case 1:
            if let _ = find(selectedPermissions, authorizerPermission) {
                return "Authorizer Confidentiality"
            }
            return ""
        case 2:
            return "Role"
        case 3:
            return "Approval Group"
        default:
            return ""
        }
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return permissions.count
        case 1:
            if let _ = find(selectedPermissions, authorizerPermission) {
                return 1
            }
            return 0;
        case 2:
            return roles.count
        case 3:
            return approvalGroups.count
        default:
            return 0
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let permission = permissions[indexPath.row]
            let cell = tableView.dequeueReusableCellWithIdentifier("permission", forIndexPath: indexPath) as UITableViewCell
            cell.textLabel?.text = permission.title
            cell.accessoryType = selectedPermissions.filter({ (selectedPermission) -> Bool in
                return permission == selectedPermission
            }).count > 0 ? UITableViewCellAccessoryType.Checkmark : UITableViewCellAccessoryType.None
            return cell
        case 1:
            let cell = tableView.dequeueReusableCellWithIdentifier("authorizerAccess", forIndexPath: indexPath) as UITableViewCell
            cell.textLabel?.text = "Allow authorizer to view details"
            cell.accessoryType = canAuthorizerViewDetails ? UITableViewCellAccessoryType.Checkmark : UITableViewCellAccessoryType.None
            return cell
        case 2:
            let cell = tableView.dequeueReusableCellWithIdentifier("role", forIndexPath: indexPath) as UITableViewCell
            cell.textLabel?.text = roles[indexPath.row].title
            
            if let role = selectedRole {
                cell.accessoryType = role == roles[indexPath.row] ? UITableViewCellAccessoryType.Checkmark : UITableViewCellAccessoryType.None
            }
            else {
                cell.accessoryType = .None
            }
            return cell
        case 3:
            let cell = tableView.dequeueReusableCellWithIdentifier("approvalGroup", forIndexPath: indexPath) as UITableViewCell
            cell.textLabel?.text = approvalGroups[indexPath.row].name
            
            if let approvalGroup = selectedApprovalGroup {
                cell.accessoryType = approvalGroup == approvalGroups[indexPath.row] ? UITableViewCellAccessoryType.Checkmark : UITableViewCellAccessoryType.None
            }
            else {
                cell.accessoryType = .None
            }
            return cell
        default:
            return UITableViewCell()
        }
    }
}

protocol UserEditViewControllerDelegate : NSObjectProtocol {
    func userEditViewController(controller: UserEditViewController, didRequestToCreateUser user: AccountUser)
}
