//
//  ApplicationFormEditViewController.swift
//  mUPAM
//
//  Created by Target Soft Systems on 16/11/15.
//  Copyright (c) 2015 Maybank2E. All rights reserved.
//

import UIKit

class ApplicationFormEditViewController: UIViewController, Reloadable {

    weak var delegate: ApplicationFormEditViewControllerDelegate?
    
    @IBOutlet var scrollView: UIScrollView!
    
    private var application: Application {
        didSet {
            
        }
    }
    
    var applicationAccountUsers: [AccountUser] {
        get {
            if let accountUsers = application.accountUsers {
                return accountUsers
            }
            else {
                return []
            }
        }
    }
    
    var formSectionEditViewControllersHeights = [CGFloat]()
    var formSectionEditViewControllers: [ApplicationFormSectionEditViewController] = []
    
    let companyEditViewContainerViewController: UINavigationController!
    let contactEditViewContainerViewController: UINavigationController!
    let corporateAccountEditViewContainerViewController: UINavigationController!
    let servicesEditViewContainerViewController: UINavigationController!
    let corporateAdministratorEditViewContainerViewController: UINavigationController!
    let usersEditViewContainerViewController: UINavigationController!
    let accountsEditViewContainerViewController: UINavigationController!
    let authorizationMatrixEditViewContainerViewController: UINavigationController!
    let clientDeclarationSectionViewContainerViewController: UINavigationController!
    let bankAdministrationDetailEditViewContainerViewController: UINavigationController!
    
    private var applicationCategory: ApplicationCategory?, company: Company?, companyContactPeople: CompanyContactPeople?, billingAccount: BillingAccount?, portfolioSummaryServices: [AccountPortfolioSummaryService]?, paymentManagementServices:[AccountPaymentManagementService]?, transferPurpose: TransferPurpose?, corporateAdminMaker: AdminMaker?, corporateAdminChecker: AdminChecker?, accountUsers: [AccountUser]?, accounts: [Account]?, accountModules: [AccountModule]?, authorizationType: AuthorizationType?, customAuthorizationType: String?, clientDeclarations: [Declaration]?, bankAdministrationDetail: BankAdminstrationDetail?
    
    init(application: Application) {
        self.application = application
        super.init(nibName: "ApplicationFormEditViewController", bundle: nil)
        
        var companyEditViewController: CompanyEditViewController!
        if let company = self.application.company {
            companyEditViewController = CompanyEditViewController(title: "A. Company Details", company: company)
        }
        else {
            companyEditViewController = CompanyEditViewController(title: "A. Company Details")
        }
        companyEditViewContainerViewController = UINavigationController(rootViewController: companyEditViewController)
        companyEditViewContainerViewController.navigationBar.translucent = false
        companyEditViewContainerViewController.view.frame = CGRectMake(companyEditViewContainerViewController.view.frame.origin.x, companyEditViewContainerViewController.view.frame.origin.y, companyEditViewContainerViewController.view.frame.size.width, companyEditViewController.view.frame.size.height + 45)
        formSectionEditViewControllers.append(companyEditViewController)
        
        var contactEditViewController: ContactEditViewController!
        if let companyContactPeople = self.application.companyContactPeople {
            contactEditViewController = ContactEditViewController(title: "B. Contact Information", companyContactPeople: companyContactPeople)
        }
        else {
            contactEditViewController = ContactEditViewController(title: "B. Contact Information")
        }
        contactEditViewContainerViewController = UINavigationController(rootViewController: contactEditViewController)
        contactEditViewContainerViewController.navigationBar.translucent = false
        contactEditViewContainerViewController.view.frame = CGRectMake(contactEditViewContainerViewController.view.frame.origin.x, contactEditViewContainerViewController.view.frame.origin.y, contactEditViewContainerViewController.view.frame.size.width, contactEditViewController.view.frame.size.height + 45)
        formSectionEditViewControllers.append(contactEditViewController)
        
        var corporateAccountEditViewController: CorporateAccountEditViewController!
        if let billingAccount = self.application.billingAccount {
            corporateAccountEditViewController = CorporateAccountEditViewController(title: "C. Corporate Account", billingAccount: billingAccount)
        }
        else {
            corporateAccountEditViewController = CorporateAccountEditViewController(title: "C. Corporate Account")
        }
        corporateAccountEditViewContainerViewController = UINavigationController(rootViewController: corporateAccountEditViewController)
        corporateAccountEditViewContainerViewController.navigationBar.translucent = false
        corporateAccountEditViewContainerViewController.view.frame = CGRectMake(corporateAccountEditViewContainerViewController.view.frame.origin.x, corporateAccountEditViewContainerViewController.view.frame.origin.y, corporateAccountEditViewContainerViewController.view.frame.size.width, corporateAccountEditViewController.view.frame.size.height + 45)
        formSectionEditViewControllers.append(corporateAccountEditViewController)

        var servicesEditViewController: ServicesEditViewController!
        if let portfolioSummaryServices = self.application.portfolioSummaryServices {
            if let paymentManagementServices = self.application.paymentManagementServices {
                servicesEditViewController = ServicesEditViewController(title: "D. Service Required", portfolioSummaryServices: portfolioSummaryServices, paymentManagementServices: paymentManagementServices)
            }
            else {
                servicesEditViewController = ServicesEditViewController(title: "D. Service Required")
            }
        }
        else {
            servicesEditViewController = ServicesEditViewController(title: "D. Service Required")
        }
        servicesEditViewContainerViewController = UINavigationController(rootViewController: servicesEditViewController)
        servicesEditViewContainerViewController.navigationBar.translucent = false
        servicesEditViewContainerViewController.view.frame = CGRectMake(servicesEditViewContainerViewController.view.frame.origin.x, servicesEditViewContainerViewController.view.frame.origin.y, servicesEditViewContainerViewController.view.frame.size.width, servicesEditViewController.view.frame.size.height + 45)
        formSectionEditViewControllers.append(servicesEditViewController)

        var corporateAdministratorEditViewController: CorporateAdministratorEditViewController!
//        var primaryContactPerson =
        if let adminMaker = self.application.adminMaker {
            if let adminChecker = self.application.adminChecker {
            corporateAdministratorEditViewController = CorporateAdministratorEditViewController(title: "E. Corporate Administrator Details", adminMaker: adminMaker, adminChecker: adminChecker)
            }
            else {
            corporateAdministratorEditViewController = CorporateAdministratorEditViewController(title: "E. Corporate Administrator Details")
            }
        }
        else {
            corporateAdministratorEditViewController = CorporateAdministratorEditViewController(title: "E. Corporate Administrator Details")
        }
        corporateAdministratorEditViewContainerViewController = UINavigationController(rootViewController: corporateAdministratorEditViewController)
        corporateAdministratorEditViewContainerViewController.navigationBar.translucent = false
        corporateAdministratorEditViewContainerViewController.view.frame = CGRectMake(corporateAdministratorEditViewContainerViewController.view.frame.origin.x, corporateAdministratorEditViewContainerViewController.view.frame.origin.y, corporateAdministratorEditViewContainerViewController.view.frame.size.width, corporateAdministratorEditViewController.view.frame.size.height + 45)
        formSectionEditViewControllers.append(corporateAdministratorEditViewController)

        var usersEditViewController: UsersEditViewController!
        usersEditViewController = UsersEditViewController(title: "F. User Nomination Details", users: applicationAccountUsers)
        usersEditViewContainerViewController = UINavigationController(rootViewController: usersEditViewController)
        usersEditViewContainerViewController.navigationBar.translucent = false
        usersEditViewContainerViewController.view.frame = CGRectMake(usersEditViewContainerViewController.view.frame.origin.x, usersEditViewContainerViewController.view.frame.origin.y, usersEditViewContainerViewController.view.frame.size.width, usersEditViewController.view.frame.size.height + 45)
        formSectionEditViewControllers.append(usersEditViewController)
        
        var accountsEditViewController: AccountsEditViewController!
        if let accounts = self.application.accounts {
            accountsEditViewController = AccountsEditViewController(title: "G. Authorised account / Modules / User", accounts: accounts)
        }
        else {
            accountsEditViewController = AccountsEditViewController(title: "G. Authorised account / Modules / User")
        }
        accountsEditViewContainerViewController = UINavigationController(rootViewController: accountsEditViewController)
        accountsEditViewContainerViewController.navigationBar.translucent = false
        accountsEditViewContainerViewController.view.frame = CGRectMake(accountsEditViewContainerViewController.view.frame.origin.x, accountsEditViewContainerViewController.view.frame.origin.y, accountsEditViewContainerViewController.view.frame.size.width, accountsEditViewController.view.frame.size.height + 45)
        formSectionEditViewControllers.append(accountsEditViewController)
        
        
        var authorizationMatrixEditViewController: AuthorizationMatrixEditViewController!
        if let authorizationType = self.application.authorizationType {
            authorizationMatrixEditViewController = AuthorizationMatrixEditViewController(title: "H. Authorisation Matrix", authorizationType: authorizationType)
        }
        else {
            authorizationMatrixEditViewController = AuthorizationMatrixEditViewController(title: "H. Authorisation Matrix")
        }
        authorizationMatrixEditViewContainerViewController = UINavigationController(rootViewController: authorizationMatrixEditViewController)
        authorizationMatrixEditViewContainerViewController.navigationBar.translucent = false
        authorizationMatrixEditViewContainerViewController.view.frame = CGRectMake(authorizationMatrixEditViewContainerViewController.view.frame.origin.x, authorizationMatrixEditViewContainerViewController.view.frame.origin.y, authorizationMatrixEditViewContainerViewController.view.frame.size.width, authorizationMatrixEditViewController.view.frame.size.height + 45)
        formSectionEditViewControllers.append(authorizationMatrixEditViewController)

        var clientDeclarationSectionViewController: ClientDeclarationSectionViewController!
        if let declarations = self.application.declarations {
            clientDeclarationSectionViewController = ClientDeclarationSectionViewController(title: "I. Client Declaration", clientDeclarations: declarations)
        }
        else {
            clientDeclarationSectionViewController = ClientDeclarationSectionViewController(title: "I. Client Declaration")
        }
        clientDeclarationSectionViewContainerViewController = UINavigationController(rootViewController: clientDeclarationSectionViewController)
        clientDeclarationSectionViewContainerViewController.navigationBar.translucent = false
        clientDeclarationSectionViewContainerViewController.view.frame = CGRectMake(clientDeclarationSectionViewContainerViewController.view.frame.origin.x, clientDeclarationSectionViewContainerViewController.view.frame.origin.y, clientDeclarationSectionViewContainerViewController.view.frame.size.width, clientDeclarationSectionViewController.view.frame.size.height + 45)
        formSectionEditViewControllers.append(clientDeclarationSectionViewController)
        
        var bankAdministrationDetailEditViewController: BankAdministrationDetailEditViewController!
        if let bankAdminstrationDetail = self.application.bankAdministrationDetail {
            bankAdministrationDetailEditViewController = BankAdministrationDetailEditViewController(title: "J. For Bank Use Only", bankAdminstrationDetail: bankAdminstrationDetail)
        }
        else {
            bankAdministrationDetailEditViewController = BankAdministrationDetailEditViewController(title: "J. For Bank Use Only")
        }
        bankAdministrationDetailEditViewContainerViewController = UINavigationController(rootViewController: bankAdministrationDetailEditViewController)
        bankAdministrationDetailEditViewContainerViewController.navigationBar.translucent = false
        bankAdministrationDetailEditViewContainerViewController.view.frame = CGRectMake(bankAdministrationDetailEditViewContainerViewController.view.frame.origin.x, bankAdministrationDetailEditViewContainerViewController.view.frame.origin.y, bankAdministrationDetailEditViewContainerViewController.view.frame.size.width, bankAdministrationDetailEditViewController.view.frame.size.height + 45)
        formSectionEditViewControllers.append(bankAdministrationDetailEditViewController)
        
        companyEditViewController.delegate = self
        contactEditViewController.delegate = self
        corporateAccountEditViewController.delegate = self
        servicesEditViewController.delegate = self
        corporateAdministratorEditViewController.delegate = self
        corporateAdministratorEditViewController.datasource = self
        usersEditViewController.delegate = self
        accountsEditViewController.delegate = self
        accountsEditViewController.datasource = self
        authorizationMatrixEditViewController.delegate = self
        clientDeclarationSectionViewController.delegate = self
        bankAdministrationDetailEditViewController.delegate = self
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.layoutFacade()
    }
    
    func layoutFacade() {
        companyEditViewContainerViewController.view.anchorTopCenterFillingWidthWithLeftAndRightPadding(0, topPadding: 0, height: companyEditViewContainerViewController.view.frame.size.height)
        contactEditViewContainerViewController.view.alignUnder(companyEditViewContainerViewController.view, centeredFillingWidthWithLeftAndRightPadding: 0, topPadding: 0, height: contactEditViewContainerViewController.view.frame.size.height)
        corporateAccountEditViewContainerViewController.view.alignUnder(contactEditViewContainerViewController.view, centeredFillingWidthWithLeftAndRightPadding: 0, topPadding: 0, height: corporateAccountEditViewContainerViewController.view.frame.size.height)
        servicesEditViewContainerViewController.view.alignUnder(corporateAccountEditViewContainerViewController.view, centeredFillingWidthWithLeftAndRightPadding: 0, topPadding: 0, height: servicesEditViewContainerViewController.view.frame.size.height)
        corporateAdministratorEditViewContainerViewController.view.alignUnder(servicesEditViewContainerViewController.view, centeredFillingWidthWithLeftAndRightPadding: 0, topPadding: 0, height: corporateAdministratorEditViewContainerViewController.view.frame.size.height)
        usersEditViewContainerViewController.view.alignUnder(corporateAdministratorEditViewContainerViewController.view, centeredFillingWidthWithLeftAndRightPadding: 0, topPadding: 0, height: usersEditViewContainerViewController.view.frame.size.height)
        accountsEditViewContainerViewController.view.alignUnder(usersEditViewContainerViewController.view, centeredFillingWidthWithLeftAndRightPadding: 0, topPadding: 0, height: accountsEditViewContainerViewController.view.frame.size.height)
        authorizationMatrixEditViewContainerViewController.view.alignUnder(accountsEditViewContainerViewController.view, centeredFillingWidthWithLeftAndRightPadding: 0, topPadding: 0, height: authorizationMatrixEditViewContainerViewController.view.frame.size.height)
        clientDeclarationSectionViewContainerViewController.view.alignUnder(authorizationMatrixEditViewContainerViewController.view, centeredFillingWidthWithLeftAndRightPadding: 0, topPadding: 0, height: clientDeclarationSectionViewContainerViewController.view.frame.size.height)
        bankAdministrationDetailEditViewContainerViewController.view.alignUnder(clientDeclarationSectionViewContainerViewController.view, centeredFillingWidthWithLeftAndRightPadding: 0, topPadding: 0, height: bankAdministrationDetailEditViewContainerViewController.view.frame.size.height)
        
        updateScrollViewContentSize()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Generate Report", style: UIBarButtonItemStyle.Plain, target: self, action: "generateReport")
        self.navigationController?.navigationBar.translucent=false;
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardDidShow:", name: UIKeyboardDidShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardWillHide:", name: UIKeyboardWillHideNotification, object: nil)
        for formSectionEditViewController in formSectionEditViewControllers {
            formSectionEditViewControllersHeights.append(formSectionEditViewController.view.frame.size.height)
        }
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationItem.title = "\(application.category.package.id) - \(application.category.package.name)"
        
        addChildViewController(companyEditViewContainerViewController)
        scrollView.addSubview(companyEditViewContainerViewController.view)
        companyEditViewContainerViewController.didMoveToParentViewController(self)
 
        addChildViewController(contactEditViewContainerViewController)
        scrollView.addSubview(contactEditViewContainerViewController.view)
        contactEditViewContainerViewController.didMoveToParentViewController(self)

        addChildViewController(corporateAccountEditViewContainerViewController)
        scrollView.addSubview(corporateAccountEditViewContainerViewController.view)
        corporateAccountEditViewContainerViewController.didMoveToParentViewController(self)

        addChildViewController(servicesEditViewContainerViewController)
        scrollView.addSubview(servicesEditViewContainerViewController.view)
        servicesEditViewContainerViewController.didMoveToParentViewController(self)
        
        addChildViewController(corporateAdministratorEditViewContainerViewController)
        scrollView.addSubview(corporateAdministratorEditViewContainerViewController.view)
        corporateAdministratorEditViewContainerViewController.didMoveToParentViewController(self)
        
        addChildViewController(usersEditViewContainerViewController)
        scrollView.addSubview(usersEditViewContainerViewController.view)
        usersEditViewContainerViewController.didMoveToParentViewController(self)

        addChildViewController(accountsEditViewContainerViewController)
        scrollView.addSubview(accountsEditViewContainerViewController.view)
        accountsEditViewContainerViewController.didMoveToParentViewController(self)
        
        addChildViewController(authorizationMatrixEditViewContainerViewController)
        scrollView.addSubview(authorizationMatrixEditViewContainerViewController.view)
        authorizationMatrixEditViewContainerViewController.didMoveToParentViewController(self)

        addChildViewController(clientDeclarationSectionViewContainerViewController)
        scrollView.addSubview(clientDeclarationSectionViewContainerViewController.view)
        clientDeclarationSectionViewContainerViewController.didMoveToParentViewController(self)
        
        addChildViewController(bankAdministrationDetailEditViewContainerViewController)
        scrollView.addSubview(bankAdministrationDetailEditViewContainerViewController.view)
        bankAdministrationDetailEditViewContainerViewController.didMoveToParentViewController(self)
        
        collapseOrExpandFormSectionEditViewController()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
//        collapseOrExpandFormSectionEditViewController()
    }

    func reload() {
        for formSectionEditViewController in formSectionEditViewControllers {
            if let reloadableFormSectionEditViewController = formSectionEditViewController as? Reloadable {
                reloadableFormSectionEditViewController.reload()
            }
        }
    }
    
    func generateReport() {
        NSURLCache.sharedURLCache().removeAllCachedResponses();
        if let documentDirectoryPath: String = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.UserDomainMask, true).first as? String {
            println(documentDirectoryPath)
            
            var applicationDict = application.dictionaryEncoded()
            if let data = NSJSONSerialization.dataWithJSONObject(applicationDict, options: NSJSONWritingOptions.PrettyPrinted, error: nil) {
                NSString(data: data, encoding: NSUTF8StringEncoding)?.writeToURL(NSURL(fileURLWithPath: documentDirectoryPath.stringByAppendingPathComponent("application.json"))!, atomically: true, encoding: NSUTF8StringEncoding, error: nil)
                NSString(data: data, encoding: NSUTF8StringEncoding)?.writeToURL(NSURL(fileURLWithPath: "/Users/TSS/Documents/Projects/mobility.ios.target.mupam/mUPAM/mUPAM/Assets/report/application.json")!, atomically: true, encoding: NSUTF8StringEncoding, error: nil)
                
            }
            
            if let sourceURL = NSURL(fileURLWithPath: NSBundle.mainBundle().pathForResource("report", ofType: "html")!) {
                NSFileManager.defaultManager().removeItemAtPath(documentDirectoryPath.stringByAppendingPathComponent("report.html"), error: nil)
                NSFileManager.defaultManager().copyItemAtURL(sourceURL, toURL: NSURL(fileURLWithPath: documentDirectoryPath.stringByAppendingPathComponent("report.html"))!, error: nil)
            }
            if let sourceURL = NSURL(fileURLWithPath: NSBundle.mainBundle().pathForResource("angular", ofType: "js")!) {
                NSFileManager.defaultManager().removeItemAtPath(documentDirectoryPath.stringByAppendingPathComponent("angular.js"), error: nil)
                NSFileManager.defaultManager().copyItemAtURL(sourceURL, toURL: NSURL(fileURLWithPath: documentDirectoryPath.stringByAppendingPathComponent("angular.js"))!, error: nil)
            }
//            if let sourceURL = NSURL(fileURLWithPath: NSBundle.mainBundle().pathForResource("application", ofType: "json")!) {
//                NSFileManager.defaultManager().removeItemAtPath(documentDirectoryPath.stringByAppendingPathComponent("application.json"), error: nil)
//                NSFileManager.defaultManager().copyItemAtURL(sourceURL, toURL: NSURL(fileURLWithPath: documentDirectoryPath.stringByAppendingPathComponent("application.json"))!, error: nil)
//            }
            if let sourceURL = NSURL(fileURLWithPath: NSBundle.mainBundle().pathForResource("kievit-regular", ofType: "ttf")!) {
                NSFileManager.defaultManager().removeItemAtPath(documentDirectoryPath.stringByAppendingPathComponent("kievit-regular.ttf"), error: nil)
                NSFileManager.defaultManager().copyItemAtURL(sourceURL, toURL: NSURL(fileURLWithPath: documentDirectoryPath.stringByAppendingPathComponent("kievit-regular.ttf"))!, error: nil)
            }
            if let sourceURL = NSURL(fileURLWithPath: NSBundle.mainBundle().pathForResource("Calibri", ofType: "ttf")!) {
                NSFileManager.defaultManager().removeItemAtPath(documentDirectoryPath.stringByAppendingPathComponent("Calibri.ttf"), error: nil)
                NSFileManager.defaultManager().copyItemAtURL(sourceURL, toURL: NSURL(fileURLWithPath: documentDirectoryPath.stringByAppendingPathComponent("Calibri.ttf"))!, error: nil)
            }
            if let sourceURL = NSURL(fileURLWithPath: NSBundle.mainBundle().pathForResource("logo_maybank", ofType: "jpg")!) {
                NSFileManager.defaultManager().removeItemAtPath(documentDirectoryPath.stringByAppendingPathComponent("logo_maybank.jpg"), error: nil)
                NSFileManager.defaultManager().copyItemAtURL(sourceURL, toURL: NSURL(fileURLWithPath: documentDirectoryPath.stringByAppendingPathComponent("logo_maybank.jpg"))!, error: nil)
            }
            if let sourceURL = NSURL(fileURLWithPath: NSBundle.mainBundle().pathForResource("logo_maybank2e", ofType: "jpg")!) {
                NSFileManager.defaultManager().removeItemAtPath(documentDirectoryPath.stringByAppendingPathComponent("logo_maybank2e.jpg"), error: nil)
                NSFileManager.defaultManager().copyItemAtURL(sourceURL, toURL: NSURL(fileURLWithPath: documentDirectoryPath.stringByAppendingPathComponent("logo_maybank2e.jpg"))!, error: nil)
            }
            
        }
        
        let reportViewController = ReportViewController(application: application)
        
        let reportNavigationViewController = UINavigationController(rootViewController: reportViewController)
        reportNavigationViewController.modalPresentationStyle = UIModalPresentationStyle.PageSheet
        presentViewController(reportNavigationViewController, animated: true) { () -> Void in
            
        }
    }
    
    func collapseOrExpandFormSectionEditViewController() {
        var applicationFormSectionEditViewControllers = childApplicationFormSectionEditViewControllers()
        var colapsableApplicationFormSectionEditViewControllers = [ApplicationFormSectionEditViewController]()
        
        for applicationFormSectionEditViewController in applicationFormSectionEditViewControllers {
            if applicationFormSectionEditViewController.mode == .New {
                println(applicationFormSectionEditViewController.view)
                colapsableApplicationFormSectionEditViewControllers.append(applicationFormSectionEditViewController)
            }
        }
        for colapsableApplicationFormSectionEditViewController in colapsableApplicationFormSectionEditViewControllers {
            collapseApplicationFormSectionEditViewController(colapsableApplicationFormSectionEditViewController, animated: false)
        }
        if let firstColapsableApplicationFormSectionEditViewController = colapsableApplicationFormSectionEditViewControllers.first {
            expandApplicationFormSectionEditViewController(firstColapsableApplicationFormSectionEditViewController, forMode: .Edit, animated: true)
            if let reloadable = firstColapsableApplicationFormSectionEditViewController as? Reloadable {
                reloadable.reload()
            }
        }
    }
    
    func indexOfFormSectionEditViewController(controller: ApplicationFormSectionEditViewController) -> Int? {
        for i in 0..<formSectionEditViewControllers.count {
            if (formSectionEditViewControllers[i] == controller) {
                return i
            }
        }
        return nil
    }
    
    func childApplicationFormSectionEditViewControllers() -> [ApplicationFormSectionEditViewController] {
        var applicationFormSectionEditViewControllers = [ApplicationFormSectionEditViewController]()
        for childViewController in childViewControllers {
            if let childViewControllerNavigationViewController = childViewController as? UINavigationController {
                if let applicationFormSectionEditViewController = childViewControllerNavigationViewController.topViewController as? ApplicationFormSectionEditViewController {
                    applicationFormSectionEditViewControllers.append(applicationFormSectionEditViewController)
                }
            }
        }
        return applicationFormSectionEditViewControllers
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    func collapseApplicationFormSectionEditViewController(controller: ApplicationFormSectionEditViewController, animated: Bool) {
        var collapse = {() -> (Void) in
            controller.mode = .New
            if let navigationViewController = controller.navigationController {
                navigationViewController.view.frame = CGRectMake(navigationViewController.view.frame.origin.x, navigationViewController.view.frame.origin.y, navigationViewController.view.frame.size.height, 45)
                println(controller.view)
                var applicationFormSectionEditViewControllers = self.childApplicationFormSectionEditViewControllers()
                for applicationFormSectionEditViewController in applicationFormSectionEditViewControllers {
                    println(applicationFormSectionEditViewController.view)
                }
            }
            self.layoutFacade()
        }
        if animated {
            UIView.animateWithDuration(1/3, animations: { () -> Void in
                collapse()
            })
        }
        else {
            collapse()
        }
    }
    
    func expandApplicationFormSectionEditViewController(controller: ApplicationFormSectionEditViewController, forMode mode:ApplicationFormSectionViewMode, animated: Bool) {
        println(controller.view)
        var expand = {() -> (Void) in
            if let navigationViewController = controller.navigationController {
                if let formSectionEditViewControllerIndex = self.indexOfFormSectionEditViewController(controller) {
                    navigationViewController.view.frame = CGRectMake(navigationViewController.view.frame.origin.x, navigationViewController.view.frame.origin.y, navigationViewController.view.frame.size.height, 45 + self.formSectionEditViewControllersHeights[formSectionEditViewControllerIndex])
                    controller.mode = mode
                }
            }
            self.layoutFacade()
        }
        if animated {
            UIView.animateWithDuration(1/3, animations: { () -> Void in
                expand()
            })
        }
        else {
            expand()
        }
    }
    
    func updateScrollViewContentSize() {
        var childViewControllersHeight : CGFloat = 0
        for childViewController in childViewControllers as [AnyObject] {
            childViewControllersHeight += (childViewController as UIViewController).view.frame.size.height
        }
        scrollView.contentSize = CGSizeMake(view.frame.size.width, childViewControllersHeight)
    }
    
    func manageCurrentApplication() {
//        NSOperationQueue().addOperationWithBlock { () -> Void in
            EntitySQLiteCoreDataStore(id: "101").manageApplication(self.application)
//        let mainQueue = NSOperationQueue.mainQueue()
//        mainQueue.addOperationWithBlock { () -> Void in
            self.delegate?.applicationFormEditViewControllerDidUpdateApplications(self)

//        }
//        }
    }
    
    func keyboardDidShow(notification: NSNotification) {
        if let userInfo = notification.userInfo {
            if let keyboardFrameValue = userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue {
                let keyboardFrame = keyboardFrameValue.CGRectValue()
                updateScrollViewContentSize()
                scrollView.contentSize = CGSizeMake(scrollView.contentSize.width, scrollView.contentSize.height + keyboardFrame.height)
                if let currentInputField = UIResponder.currentFirstResponder as? UIView {
                    let point = currentInputField.superview!.convertPoint(currentInputField.frame.origin, toView: scrollView)
                    scrollView.setContentOffset(CGPointMake(0, point.y), animated: true)
                }
            }
        }
    }
    
    func keyboardWillHide(notification: NSNotification) {
        if let userInfo = notification.userInfo {
            if let keyboardFrameValue = userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue {
                let keyboardFrame = keyboardFrameValue.CGRectValue()
                scrollView.contentSize = CGSizeMake(scrollView.contentSize.width, scrollView.contentSize.height - keyboardFrame.height)
                scrollView.setContentOffset(CGPointMake(scrollView.contentOffset.x, scrollView.contentOffset.y - keyboardFrame.height < 0 ? 0 : scrollView.contentOffset.y - keyboardFrame.height), animated: true)
            }
        }
    }
    
    override func supportedInterfaceOrientations() -> Int {
        return Int(UIInterfaceOrientationMask.Landscape.rawValue)
    }
    
    deinit {
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardDidShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillHideNotification, object: nil)
    }
}

extension ApplicationFormEditViewController : CompanyEditViewControllerDelegate {
    func companyEditViewController(controller: CompanyEditViewController, didRequestToSaveCompany company: Company) {
        application.company = company
        manageCurrentApplication()
        controller.mode = .Edit
        collapseOrExpandFormSectionEditViewController()
    }
}

extension ApplicationFormEditViewController: ContactEditViewControllerDelegate {
    func contactEditViewController(controller: ContactEditViewController, didRequestToSetPrimaryContactPerson primaryContactPerson: Person, secondaryContactPerson contactPerson: Person) {
        application.companyContactPeople = CompanyContactPeople(primaryContactPerson: primaryContactPerson, secondaryContactPerson: contactPerson)
        manageCurrentApplication()
        controller.mode = .Edit
        collapseOrExpandFormSectionEditViewController()
        reload()
    }
}

extension ApplicationFormEditViewController : CorporateAccountEditViewControllerDelegate {
    func corporateAccountEditViewController(controller: CorporateAccountEditViewController, didRequestToSetCorporateBillingAccount corporateBillingAccount: BillingAccount) {
        application.billingAccount = corporateBillingAccount
        manageCurrentApplication()
        controller.mode = .Edit
        collapseOrExpandFormSectionEditViewController()
    }
}

extension ApplicationFormEditViewController: ServicesEditViewControllerDelegate {
    func servicesEditViewController(controller: ServicesEditViewController, didSelectAccountPortfolioSummaryServices portfolioSummaryServices: [AccountPortfolioSummaryService], accountPaymentManagementServices paymentManagementServices: [AccountPaymentManagementService], accountTransferPurpose transferPurpose: TransferPurpose) {
        application.portfolioSummaryServices = portfolioSummaryServices
        application.paymentManagementServices = paymentManagementServices
        application.transferPurpose = transferPurpose
        manageCurrentApplication()
        controller.mode = .Edit
        collapseOrExpandFormSectionEditViewController()
    }
}

extension ApplicationFormEditViewController: CorporateAdministratorEditViewControllerDelegate, CorporateAdministratorEditViewControllerDatasource {
    func corporateAdministratorEditViewController(controller: CorporateAdministratorEditViewController, didRequestToAddCorporateAdminMaker adminMaker: AdminMaker, adminChecker: AdminChecker) {
        application.adminMaker = adminMaker
        application.adminChecker = adminChecker
        manageCurrentApplication()
        controller.mode = .Edit
        collapseOrExpandFormSectionEditViewController()
    }
    
    func primaryContactPersonForCorporateAdministratorEditViewController(controller: CorporateAdministratorEditViewController) -> Person? {
        return  application.companyContactPeople?.primaryContactPerson
    }
    
    func secondaryContactPersonForCorporateAdministratorEditViewController(controller: CorporateAdministratorEditViewController) -> Person? {
        return  application.companyContactPeople?.secondaryContactPerson
    }
}

extension ApplicationFormEditViewController: UsersEditViewControllerDelegate {
    func usersEditViewController(controller: UsersEditViewController, didRequestToCreateUsers users: [AccountUser]) {
        application.accountUsers = users
        manageCurrentApplication()
        controller.mode = .Edit
        collapseOrExpandFormSectionEditViewController()
    }
}

extension ApplicationFormEditViewController: AccountsEditViewControllerDelegate,  AccountsEditViewControllerDatasource {
    func accountsEditViewController(controller: AccountsEditViewController, didRequestToCreateAccounts accounts: [Account]) {
        application.accounts = accounts
        manageCurrentApplication()
        controller.mode = .Edit
        collapseOrExpandFormSectionEditViewController()
    }
    
    func accountUsersForAccountsEditViewController(controller: AccountsEditViewController) -> [AccountUser] {
        return applicationAccountUsers
    }
}

extension ApplicationFormEditViewController: AuthorizationMatrixEditViewControllerDelegate {
    func authorizationMatrixEditViewController(controller: AuthorizationMatrixEditViewController, didRequestToSetAuthorizationType authorizationType: AuthorizationType) {
        application.authorizationType = authorizationType
        application.customAuthorizationType = customAuthorizationType
        manageCurrentApplication()
        controller.mode = .Edit
        collapseOrExpandFormSectionEditViewController()
    }
}

extension ApplicationFormEditViewController: ClientDeclarationSectionViewControllerDelegate {
    func clientDeclarationSectionViewController(controller: ClientDeclarationSectionViewController, didRequestToSetClientDeclarations clientDeclarations: [Declaration]) {
        application.declarations = clientDeclarations
        manageCurrentApplication()
        controller.mode = .Edit
        collapseOrExpandFormSectionEditViewController()
    }
}

extension ApplicationFormEditViewController: BankAdministrationDetailEditViewControllerDelegate {
    func bankAdministrationDetailEditViewController(controller: BankAdministrationDetailEditViewController, didRequestToSetBankAdministrationDetail bankAdministrationDetail: BankAdminstrationDetail) {
        application.bankAdministrationDetail = bankAdministrationDetail
        manageCurrentApplication()
        controller.mode = .Edit
        collapseOrExpandFormSectionEditViewController()
    }
}

protocol ApplicationFormEditViewControllerDelegate : NSObjectProtocol {
    func applicationFormEditViewControllerDidUpdateApplications(controller: ApplicationFormEditViewController)
}