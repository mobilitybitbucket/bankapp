//
//  ContactEditViewController.swift
//  mUPAM
//
//  Created by Target Soft Systems on 17/11/15.
//  Copyright (c) 2015 Maybank2E. All rights reserved.
//

import UIKit


class ContactEditViewController: ApplicationFormSectionEditViewController, Editable, Reloadable {
    
    weak var delegate: ContactEditViewControllerDelegate?
    
    var primaryContactPersonEditViewController: PersonEditViewController!
    var secondaryContactPersonEditViewController: PersonEditViewController!
    
    let primaryContactPersonTitle = SubTitleLabel()
    let secondaryContactPersonTitle = SubTitleLabel(frame: CGRectMake(10, 0, 250, 30))
    
    override init() {
        super.init(nibName:"ContactEditViewController", bundle: nil)
    }
    
    func addViewContent() {
        primaryContactPersonTitle.textAlignment = NSTextAlignment.Left
        primaryContactPersonTitle.text = self.primaryContactPersonEditViewController.title
        view.addSubview(primaryContactPersonTitle)
        
        secondaryContactPersonTitle.textAlignment = NSTextAlignment.Left
        secondaryContactPersonTitle.text = self.secondaryContactPersonEditViewController.title
        view.addSubview(secondaryContactPersonTitle)
        
        addChildViewController(primaryContactPersonEditViewController)
        view.addSubview(primaryContactPersonEditViewController.view)
        primaryContactPersonEditViewController.didMoveToParentViewController(self)
        
        addChildViewController(secondaryContactPersonEditViewController)
        view.addSubview(secondaryContactPersonEditViewController.view)
        secondaryContactPersonEditViewController.didMoveToParentViewController(self)
        print(view)
    }
    
    convenience init(title: String) {
        self.init()
        self.title = title
        primaryContactPersonEditViewController = PersonEditViewController(title: "PRIMARY CONTACT INFO", additionalFields: [PersonEditViewControllerAdditionalField.Fax])
        secondaryContactPersonEditViewController = PersonEditViewController(title: "SECONDARY CONTACT INFO", additionalFields: [PersonEditViewControllerAdditionalField.Fax])
        addViewContent()
    }
    
    convenience init(title: String, companyContactPeople: CompanyContactPeople) {
        self.init()
        mode = .Edit
        self.title = title
        self.primaryContactPersonEditViewController = PersonEditViewController(title: "PRIMARY CONTACT INFO", person: companyContactPeople.primaryContactPerson, additionalFields: [PersonEditViewControllerAdditionalField.Fax])
        self.secondaryContactPersonEditViewController = PersonEditViewController(title: "SECONDARY CONTACT INFO", person: companyContactPeople.secondaryContactPerson, additionalFields: [PersonEditViewControllerAdditionalField.Fax])
        addViewContent()        
    }
    
    //    required init(coder aDecoder: NSCoder) {
    //        fatalError("init(coder:) has not been implemented")
    //    }
    //
    
    override func viewDidLoad() {
        super.viewDidLoad()
        

                
        // Do any additional setup after loading the view.
    }
    
    
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        //        print(self.view)
        self.layoutFacade()
        //        print(self.view)
    }
    
    func layoutFacade() {
        primaryContactPersonTitle.frame = CGRectMake(10, 0, view.frame.size.width, 30)
        primaryContactPersonEditViewController.view.alignUnder(primaryContactPersonTitle, withLeftPadding: 0, topPadding: 0, width: primaryContactPersonEditViewController.view.frame.size.width, height: primaryContactPersonEditViewController.view.frame.size.height)
        secondaryContactPersonTitle.alignUnder(primaryContactPersonEditViewController.view, withLeftPadding: 10, topPadding: 10, width: secondaryContactPersonTitle.frame.size.width, height: secondaryContactPersonTitle.frame.size.height)
        secondaryContactPersonEditViewController.view.alignUnder(secondaryContactPersonTitle, withLeftPadding: 0, topPadding: 3, width: secondaryContactPersonEditViewController.view.frame.size.width, height: secondaryContactPersonEditViewController.view.frame.size.height)
        //        view.groupVertically([primaryContactPersonTitle,
        //            primaryContactPersonEditViewController.view,
        //            secondaryContactPersonTitle,
        //            secondaryContactPersonEditViewController.view,
        //            ], inUpperLeftWithLeftPadding: 0, topPadding: 0, spacing: 20, width: view.frame.size.width, height: primaryContactPersonEditViewController.view.frame.size.height)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func edit() {
        
    }
    
    func save() {
        if let primary = primaryContactPersonEditViewController{
            if !primary.validateAndThrowResult() {
                return
            }
        }
        
        if let secondary = secondaryContactPersonEditViewController{
            if !secondary.secondaryValidate() {
                return
            }
        }
        
        self.delegate?.contactEditViewController(self, didRequestToSetPrimaryContactPerson: primaryContactPersonEditViewController.person, secondaryContactPerson: secondaryContactPersonEditViewController.person)
    }

    
    func reload() {
        
    }
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    
}

protocol ContactEditViewControllerDelegate : NSObjectProtocol {
    
    func contactEditViewController(controller: ContactEditViewController, didRequestToSetPrimaryContactPerson primaryContactPerson: Person, secondaryContactPerson contactPerson: Person)
}