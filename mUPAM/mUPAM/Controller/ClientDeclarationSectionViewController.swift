//
//  ClientDeclarationSectionViewController.swift
//  mUPAM
//
//  Created by Target Soft Systems on 20/11/15.
//  Copyright (c) 2015 Maybank2E. All rights reserved.
//

import UIKit

class ClientDeclarationSectionViewController: ApplicationFormSectionEditViewController {

    weak var delegate: ClientDeclarationSectionViewControllerDelegate?
    
    let firstPersonClientDeclarationEditViewController: ClientDeclarationEditViewController
    let secondPersonClientDeclarationEditViewController: ClientDeclarationEditViewController

    init(title: String) {
        firstPersonClientDeclarationEditViewController = ClientDeclarationEditViewController()
        secondPersonClientDeclarationEditViewController = ClientDeclarationEditViewController()
        super.init(nibName: "ClientDeclarationSectionViewController", bundle: nil)
        super.title = title
    }
    
    init(title: String, clientDeclarations:[Declaration]) {
        
        if let firstClientDeclaration = clientDeclarations.filter({ (clientDeclaration) -> Bool in
            return clientDeclaration.id == "1"
        }).first {
            firstPersonClientDeclarationEditViewController = ClientDeclarationEditViewController(clientDeclaration: firstClientDeclaration)
        }
        else {
            firstPersonClientDeclarationEditViewController = ClientDeclarationEditViewController()
        }
        if let secondClientDeclaration = clientDeclarations.filter({ (clientDeclaration) -> Bool in
            return clientDeclaration.id == "2"
        }).first  {
            secondPersonClientDeclarationEditViewController = ClientDeclarationEditViewController(clientDeclaration: secondClientDeclaration)
        }
        else {
            secondPersonClientDeclarationEditViewController = ClientDeclarationEditViewController()
        }
        super.init(nibName: "ClientDeclarationSectionViewController", bundle: nil)
        if clientDeclarations.count > 0 {
            mode = .Edit
        }
        super.title = title
    }
    
//    required init(coder aDecoder: NSCoder) {
//        fatalError("init(coder:) has not been implemented")
//    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addChildViewController(firstPersonClientDeclarationEditViewController)
        view.addSubview(firstPersonClientDeclarationEditViewController.view)
        firstPersonClientDeclarationEditViewController.didMoveToParentViewController(self)
        
        addChildViewController(secondPersonClientDeclarationEditViewController)
        view.addSubview(secondPersonClientDeclarationEditViewController.view)
        secondPersonClientDeclarationEditViewController.didMoveToParentViewController(self)
        // Do any additional setup after loading the view.
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.layoutFacade()
    }
    
    func layoutFacade() {
        view.groupVertically([firstPersonClientDeclarationEditViewController.view, secondPersonClientDeclarationEditViewController.view], inUpperLeftWithLeftPadding: 0, topPadding: 0, spacing: 10, width: view.frame.size.width, height: firstPersonClientDeclarationEditViewController.view.frame.size.height)
    }
    
    func edit() {
        mode = .Edit
    }
    
    func save() {
        let first=firstPersonClientDeclarationEditViewController
        if (!first.validateAndThrowResult()) {
            return
        }
        var clientDeclarations = [Declaration]()
        if let firstPersonClientDeclaration = firstPersonClientDeclarationEditViewController.declaration {
            clientDeclarations.append(Declaration(id: "\(1)", name: firstPersonClientDeclaration.name, photo: firstPersonClientDeclaration.photo, signature: firstPersonClientDeclaration.signature, date: firstPersonClientDeclaration.date))
        }
        if let secondPersonClientDeclaration = secondPersonClientDeclarationEditViewController.declaration {
            clientDeclarations.append(Declaration(id: "\(2)", name: secondPersonClientDeclaration.name, photo: secondPersonClientDeclaration.photo, signature: secondPersonClientDeclaration.signature, date: secondPersonClientDeclaration.date))
        }
        if (clientDeclarations.count > 0) {
            self.delegate?.clientDeclarationSectionViewController(self, didRequestToSetClientDeclarations: clientDeclarations)
        }
        
    }
    
    override func supportedInterfaceOrientations() -> Int {
        return Int(UIInterfaceOrientationMask.Landscape.rawValue)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

protocol ClientDeclarationSectionViewControllerDelegate : NSObjectProtocol {
    func clientDeclarationSectionViewController(controller: ClientDeclarationSectionViewController, didRequestToSetClientDeclarations clientDeclarations: [Declaration])
}