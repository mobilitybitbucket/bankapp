//
//  AppDelegate.swift
//  mUPAM
//
//  Created by Target Soft Systems on 23/10/15.
//  Copyright (c) 2015 Maybank2E. All rights reserved.
//

import UIKit
import HockeySDK;
import QuartzCore;

let themeColor = UIColor(red: 250.0/255.0, green: 209.0/255.0, blue: 13.0/255.0, alpha: 1.0)

func showProgress() {
    print("show")
    let appDelegate = UIApplication.sharedApplication().delegate as AppDelegate
    if(appDelegate.strLabel.text=="Sending Mail")
    {
    appDelegate.activityIndicatorView.startAnimating()
    appDelegate.window?.userInteractionEnabled = false
    appDelegate.window?.subviews.last!.addSubview(appDelegate.messageFrame)
    }
    else
    {
         appDelegate.window?.subviews.last!.addSubview(appDelegate.messageFrame)
    }
}
func hideProgress() {
    let appDelegate = UIApplication.sharedApplication().delegate as AppDelegate
    appDelegate.messageFrame.removeFromSuperview()
    appDelegate.window?.userInteractionEnabled = true
}

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    let activityIndicatorView = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.WhiteLarge)
    var messageFrame = UIView()
    var strLabel = UILabel()
    
   var applications = [Application]()
    /*
    Application(applicationId: "101", category: ApplicationCategory (region: Region(id: "102", name: "East Malaysia", packages: [Package(id: "MYR 40", name: "Basic Enquiry"), Package(id: "MYR 60", name: "Domestic & Internation Payment"), Package(id: "MYR 80", name: "Domestic & Internation Payment")]), package: Package(id: "102", name: "East Malaysia"), segment: Segment(id: "104", name: "Embassy"), cifNo: "315024"), company: Company(name: "Target Soft Systems", registrationNumber: "566464", businessAddress: Address(lowLevelSpatialReference: "8/3, Sarojini Street",highLevelSpatialReference: "T. Nagar, Chennai", state: State(id: "101", name: "Tamil Nadu"), country: Country(id: "102", name: "India")), businessNature: "Software Developement cum Training", email: EmailContact(id: "101", value: "raj@targetsoft.in"), telephone: TelephoneContact(id: "101", value: "9382383393"), fax: FaxContact(id: "101", value: "04130000000"), website: "www.targetsoft.in"), companyContactPeople: CompanyContactPeople(primaryContactPerson: Person(id: "101", name: "Prithvi Raj", designation: "Managing Director", email: EmailContact(id: "101", value: "raj@targetsoft.in"), telephone: TelephoneContact(id: "101", value: "04130000000"), fax: FaxContact(id: "101", value: "04130000000"), mobile: MobileContact(id: "101", value: "9382383393")), secondaryContactPerson: Person(id: "102", name: "Manikandan", designation: "Managing Director", email: EmailContact(id: "102", value: "manikandan@targetsoft.in"), telephone: TelephoneContact(id: "102", value: "04130000000"), fax: FaxContact(id: "102", value: "04130000000"), mobile: MobileContact(id: "102", value: "9382383393"))), billingAccount: BillingAccount(id: "10021541515", isNonResident: true), portfolioSummaryServices: [], paymentManagementServices: [], transferPurpose: TransferPurpose(id: "101", title: "transfer purpose") , adminMaker: AdminMaker(id: "101", name: "Prithvi Raj", designation: "Managing Director", email: EmailContact(id: "101", value: "raj@targetsoft.in"), telephone: TelephoneContact(id: "101", value: "04130000000"), fax: FaxContact(id: "101", value: "04130000000"), mobile: MobileContact(id: "101", value: "9382383393")), adminChecker: AdminChecker(id: "102", name: "Manikandan", designation: "Managing Director", email: EmailContact(id: "102", value: "manikandan@targetsoft.in"), telephone: TelephoneContact(id: "102", value: "04130000000"), fax: FaxContact(id: "102", value: "04130000000"), mobile: MobileContact(id: "102", value: "9382383393")), accountUsers:[AccountUser(id: "101", name: "Raj", designation: "Accountant", email: EmailContact(id: "101", value: "darputharaj@yahoo.com"), telephone: TelephoneContact(id: "101", value: "04132254715"), fax: FaxContact(id: "101", value: "04132254715"), mobile: MobileContact(id: "101", value: "9003531715"), verificationReference: VerificationReference(type: VerificationReferenceType(id: "101", name: "Passport"), verificationReferenceId: "2154564"), right: AccountUserRight(role: AccountUserRole(id: "101", title: "Admin"), permissions: [AccountUserPermission(id: "101", title: "Can edit")], canAuthorizerViewDetails: true, approvalGroup: AccountUserApprovalGroup(id: "101", name: "A"))), AccountUser(id: "102", name: "Dass", designation: "Accountant", email: EmailContact(id: "101", value: "darputharaj@yahoo.com"), telephone: TelephoneContact(id: "101", value: "04132254715"), fax: FaxContact(id: "101", value: "04132254715"), mobile: MobileContact(id: "101", value: "9003531715"), verificationReference: VerificationReference(type: VerificationReferenceType(id: "101", name: "Passport"), verificationReferenceId: "2154564"), right: AccountUserRight(role: AccountUserRole(id: "101", title: "Admin"), permissions: [AccountUserPermission(id: "101", title: "Can edit")], canAuthorizerViewDetails: true, approvalGroup: AccountUserApprovalGroup(id: "101", name: "A")))], accounts: [Account(id: "10214", modules: [], users: [AccountUser(id: "101", name: "Raj", designation: "Accountant", email: EmailContact(id: "101", value: "darputharaj@yahoo.com"), telephone: TelephoneContact(id: "101", value: "04132254715"), fax: FaxContact(id: "101", value: "04132254715"), mobile: MobileContact(id: "101", value: "9003531715"), verificationReference: VerificationReference(type: VerificationReferenceType(id: "101", name: "Passport"), verificationReferenceId: "2154564"), right: AccountUserRight(role: AccountUserRole(id: "101", title: "Admin"), permissions: [AccountUserPermission(id: "101", title: "Can edit")], canAuthorizerViewDetails: true, approvalGroup: AccountUserApprovalGroup(id: "101", name: "A"))), AccountUser(id: "102", name: "Dass", designation: "Accountant", email: EmailContact(id: "101", value: "darputharaj@yahoo.com"), telephone: TelephoneContact(id: "101", value: "04132254715"), fax: FaxContact(id: "101", value: "04132254715"), mobile: MobileContact(id: "101", value: "9003531715"), verificationReference: VerificationReference(type: VerificationReferenceType(id: "101", name: "Passport"), verificationReferenceId: "2154564"), right: AccountUserRight(role: AccountUserRole(id: "101", title: "Admin"), permissions: [AccountUserPermission(id: "101", title: "Can edit")], canAuthorizerViewDetails: true, approvalGroup: AccountUserApprovalGroup(id: "101", name: "A")))])], authorizationType: AuthorizationType(id: "101", name: "Single"), customAuthorizationType: nil, declarations: [Declaration(id: "101", name: "Prithvi raj", signature: "prithviraj", date: NSDate()), Declaration(id: "102", name: "Manikandan", signature: "manikandan", date: NSDate())], bankAdministrationDetail: BankAdminstrationDetail(id: "101", officerName: "Kumar", telephoneNumber: "9382383393", providentFundNumber: "101", initiationBranch: Branch(id: "101", name: "Main Branch"), system: System(id: "101", name: "RCMS"), date: NSDate()))
*/
   // ]
    
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        
      
//        activityIndicatorView.color=UIColor.blackColor()
//        activityIndicatorView.hidesWhenStopped = true
//        activityIndicatorView.layer.shadowOffset = CGSizeMake(10, 10)
//       
//       activityIndicatorView.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
//        strLabel = UILabel(frame: CGRect(x: 50, y: 0, width: 200, height: 50))
//        strLabel.text = "Sending Mail"
//        strLabel.font=UIFont(name: "Kievit-Regular", size: 22)!
//        strLabel.textColor = UIColor.blackColor()
//        messageFrame = UIView(frame: CGRect(x: UIScreen.mainScreen().bounds.midX - 90, y: UIScreen.mainScreen().bounds.midY - 25 , width: 180, height: 50))
//        messageFrame.layer.cornerRadius = 15
//        messageFrame.backgroundColor = UIColor(red: 252.0/255.0, green: 191.0/255.0, blue: 8.0/255.0, alpha: 1.0)
//        messageFrame.addSubview(activityIndicatorView)
//        messageFrame.addSubview(strLabel)
        BITHockeyManager.sharedHockeyManager().configureWithIdentifier("2ddfab14a2d340f09746933a2103aa04")
        // Do some additional configuration if needed here
        BITHockeyManager.sharedHockeyManager().startManager();
        BITHockeyManager.sharedHockeyManager().authenticator.authenticateInstallation();

//        PixateFreestyle.initializePixateFreestyle()
        // Override point for customization after application launch.
         
        window = UIWindow(frame: UIScreen.mainScreen().bounds)
        
        let dashboardViewController = DashboardViewController(nibName:"DashboardViewController", bundle: nil)
        dashboardViewController.delegate = self
        window?.rootViewController = dashboardViewController
        window?.makeKeyAndVisible()
        reloadPersistedApplications()
        applyStyle()
        return true
    }
    
    func applyStyle() {
        window?.tintColor = UIColor.blackColor()
        let navigationBarButtonTitleTextAttributes = [
            NSFontAttributeName : UIFont(name: "SFUIDisplay-Medium", size: 18)!,
            NSForegroundColorAttributeName : UIColor.blackColor()
        ]
        let navigationBarTitleTextAttributes = [
            NSFontAttributeName : UIFont(name: "SFUIDisplay-Medium", size: 24)!,
            NSForegroundColorAttributeName : UIColor.blackColor()
        ]
        
        UIBarButtonItem.appearance().tintColor = UIColor.blackColor()
        UIBarButtonItem.appearance().setTitleTextAttributes(navigationBarButtonTitleTextAttributes, forState: UIControlState.Normal)
        
        UINavigationBar.appearance().barTintColor = themeColor
        UINavigationBar.appearance().titleTextAttributes = navigationBarTitleTextAttributes
        
        //        UISwitch.appearance().onTintColor = UIColor(red: 252.0/255.0, green: 130.0/255.0, blue: 6.0/255.0, alpha: 1.0)
        UISwitch.appearance().onTintColor = themeColor
        
        UILabel.appearance().font = UIFont(name: "SFUIDisplay-Medium", size: 14)!
        UILabel.my_appearanceWhenContainedIn(UITableViewHeaderFooterView.self).font = UIFont(name: "SFUIDisplay-Semibold", size: 19)!
        
    }

    func reloadPersistedApplications() {
        applications = []
        let persistedApplications = EntitySQLiteCoreDataStore(id: "101").fetchApplications()
        for application in persistedApplications {
            applications.append(application)
        }
    }
    
    
    func progressindicator(value:String)
    {
        activityIndicatorView.color=UIColor.blackColor()
        activityIndicatorView.hidesWhenStopped = true
        activityIndicatorView.layer.shadowOffset = CGSizeMake(10, 10)
        //activityIndicatorView.center = CGPoint(x: 0, y: 0)
        activityIndicatorView.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
        strLabel = UILabel(frame: CGRect(x: 50, y: 0, width: 200, height: 50))
        strLabel.text = value
        strLabel.font=UIFont(name: "Kievit-Regular", size: 22)!
        strLabel.textColor = UIColor.blackColor()
        messageFrame = UIView(frame: CGRect(x: UIScreen.mainScreen().bounds.midX - 90, y: UIScreen.mainScreen().bounds.midY - 25 , width: 230, height: 50))
        messageFrame.layer.cornerRadius = 15
        messageFrame.backgroundColor = themeColor
        messageFrame.addSubview(activityIndicatorView)
        messageFrame.addSubview(strLabel)
        if(strLabel.text=="Sending Mail")
        {
            
            
        
            activityIndicatorView.startAnimating()
           //self.window?.userInteractionEnabled = false
            self.window?.subviews.last!.addSubview(messageFrame)
            UIView.animateWithDuration(2.00, animations: {self.messageFrame.alpha=0.0}
                , completion: {(value: Bool) in
                    self.messageFrame.hidden = true
                    
            })
        }
        else {
            activityIndicatorView.stopAnimating()
            strLabel.textAlignment=NSTextAlignment.Center
            self.window?.subviews.last!.addSubview(messageFrame)
            UIView.animateWithDuration(2.00, animations: {self.messageFrame.alpha=0.0}
                , completion: {(value: Bool) in
                    self.messageFrame.hidden = true
            })
           
        }
        
        
        
        /*To hide*/
//        [UIView animateWithDuration:0.25 animations:^{
//            sliderView.frame =  CGRectMake(130, 30, 0, 0);
//            [sliderView setAlpha:0.0f];
//            } completion:^(BOOL finished) {
//            [sliderView setHidden:YES];
//            }];

        
    }
    
    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

extension AppDelegate : DashboardViewControllerDelegate {
    
    func dashboardViewControllerDidRequestToNavigateToManageApplicationForms(controller: DashboardViewController) {
        let splitViewController = UISplitViewController()
        let applicationsViewController = ApplicationsViewController(title: "Applications", applications: applications)
        applicationsViewController.delegate = self
        splitViewController.viewControllers = [UINavigationController(rootViewController: applicationsViewController), UINavigationController(rootViewController: UIViewController())]
        window?.rootViewController?.presentViewController(splitViewController, animated: true, completion: { () -> Void in
            
        })

    }
}

extension AppDelegate : ApplicationsViewControllerDelegate {
    func applicationsViewControllerDidUpdateApplications(controller: ApplicationsViewController) {
        reloadPersistedApplications()
    }
}

extension UIResponder {
    // Swift 1.2 finally supports static vars!. If you use 1.1 see:
    // http://stackoverflow.com/a/24924535/385979
    
    private struct SubStruct { static var _currentFirstResponder: UIResponder? = nil }
    
    class var currentFirstResponder: UIResponder? {
        get {
            SubStruct._currentFirstResponder = nil
            UIApplication.sharedApplication().sendAction("findFirstResponder:", to: nil, from: nil, forEvent: nil)
            return SubStruct._currentFirstResponder
        }
        set {
            SubStruct._currentFirstResponder = newValue
        }
    }
    
    internal func findFirstResponder(sender: AnyObject) {
        SubStruct._currentFirstResponder = self
    }
}