//
//  Appearance.m
//  mUPAM
//
//  Created by Target Soft Systems on 7/17/16.
//  Copyright (c) 2016 Maybank2E. All rights reserved.
//

#import "Appearance.h"

@implementation UIView (Appearance)

+ (instancetype)my_appearanceWhenContainedIn:(Class<UIAppearanceContainer>)containerClass {
    return [self appearanceWhenContainedIn:containerClass, nil];
}

@end