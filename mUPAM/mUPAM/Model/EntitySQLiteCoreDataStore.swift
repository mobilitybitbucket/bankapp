//
//  EntityDataStore.swift
//  mUPAM
//
//  Created by Target Soft Systems on 01/01/16.
//  Copyright (c) 2016 Maybank2E. All rights reserved.
//

import Foundation
import CoreData

class EntitySQLiteCoreDataStore {
    
    let persistentStoreCoordinator: NSPersistentStoreCoordinator
    let managedObjectModel: NSManagedObjectModel
    let managedObjectContext: NSManagedObjectContext
    
    init(id: String) {
        managedObjectModel = NSManagedObjectModel (contentsOfURL: NSURL.fileURLWithPath(NSBundle.mainBundle() .pathForResource("Entity", ofType: "momd")!)!)!
        persistentStoreCoordinator = NSPersistentStoreCoordinator(managedObjectModel: managedObjectModel)
        let path = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.UserDomainMask, true).first as String
        persistentStoreCoordinator.addPersistentStoreWithType(NSSQLiteStoreType, configuration: nil, URL: NSURL.fileURLWithPath(path.stringByAppendingPathComponent("\(id).sqlite")), options: nil, error: nil)
        managedObjectContext = NSManagedObjectContext(concurrencyType: NSOperationQueue.currentQueue() == NSOperationQueue.mainQueue() ? NSManagedObjectContextConcurrencyType.MainQueueConcurrencyType : NSManagedObjectContextConcurrencyType.PrivateQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = persistentStoreCoordinator
    }
    
    func manageAddress(address: Address) {
        
    }

    func fetchApplications() -> [Application] {
        var applications: [Application] = []
        let errorPointer = NSErrorPointer()
        if let managedApplications = managedObjectContext.executeFetchRequest(NSFetchRequest(entityName: "Application"), error: errorPointer) as? [ManagedApplication] {
            for managedApplication in managedApplications {
                let application = Application(application: managedApplication)
                applications.append(application)
            }
        }
        return applications
    }
    
    func manageApplication(application: Application) {
        var managedApplication = managedObjectContext.editableObjectOfEntity("Application", withPredicate: NSPredicate(format: "applicationId==\(application.applicationId)")!) as ManagedApplication
//        let managedRegion = NSEntityDescription.insertNewObjectForEntityForName("Region", inManagedObjectContext: managedObjectContext) as ManagedRegion
//        managedRegion.manageRegion(application.category.region)
//        let managedPackage = NSEntityDescription.insertNewObjectForEntityForName("Package", inManagedObjectContext: managedObjectContext) as ManagedPackage
//        managedPackage.managePackage(application.category.package)
//        let managedSegment = NSEntityDescription.insertNewObjectForEntityForName("Segment", inManagedObjectContext: managedObjectContext) as ManagedSegment
//        managedSegment.manageSegment(application.category.segment)
//        let managedApplicationCategory = NSEntityDescription.insertNewObjectForEntityForName("ApplicationCategory", inManagedObjectContext: managedObjectContext) as ManagedApplicationCategory
//        managedApplicationCategory.region = managedRegion
//        managedApplicationCategory.package = managedPackage
//        managedApplicationCategory.segment = managedSegment
//        managedApplicationCategory.cifNo = application.category.cifNo
//        managedApplication.category = managedApplicationCategory
        managedApplication.manageApplication(application)
        managedObjectContext.save(nil)
        fetchApplications()
        
    }
}

extension NSManagedObjectContext {
    
    func newObjectOfEntity(entity: String) -> AnyObject {
        return NSEntityDescription.insertNewObjectForEntityForName(entity, inManagedObjectContext: self)
    }
    
    func editableObjectOfEntity(entity: String, withRelation relation: String, onObject object: NSManagedObject) -> AnyObject {
        let fetchRequest = NSFetchRequest(entityName: entity)
        fetchRequest.predicate = NSPredicate(format: "\(relation) = %@", object)
        if let existingManagedObject: AnyObject = executeFetchRequest(fetchRequest, error: nil)?.first {
            return existingManagedObject
        }
        else {
            return NSEntityDescription.insertNewObjectForEntityForName(entity, inManagedObjectContext: self)
        }
    }
    
    func editableObjectOfEntity(entity: String, withRelation relation: String, onObject object: NSManagedObject, #predicate: NSPredicate) -> AnyObject {
        let fetchRequest = NSFetchRequest(entityName: entity)
        let relationPredicate = NSPredicate(format: "\(relation) = %@", object)!
        fetchRequest.predicate = NSCompoundPredicate.andPredicateWithSubpredicates([relationPredicate, predicate]) as NSPredicate
        
        if let existingManagedObject: AnyObject = executeFetchRequest(fetchRequest, error: nil)?.first {
            return existingManagedObject
        }
        else {
            return NSEntityDescription.insertNewObjectForEntityForName(entity, inManagedObjectContext: self)
        }
    }
    
    func editableObjectOfEntity(entity: String, withPredicate predicate: NSPredicate) -> AnyObject {
        let fetchRequest = NSFetchRequest(entityName: entity)
        fetchRequest.predicate = predicate
        if let existingManagedObject: AnyObject = executeFetchRequest(fetchRequest, error: nil)?.first {
            return existingManagedObject
        }
        else {
            return NSEntityDescription.insertNewObjectForEntityForName(entity, inManagedObjectContext: self)
        }
    }
}
