//
//  Entity.swift
//  mUPAM
//
//  Created by Target Soft Systems on 05/11/15.
//  Copyright (c) 2015 Maybank2E. All rights reserved.
//

import Foundation
import CoreData

protocol DictionaryEncoding {
    func dictionaryEncoded() -> [String : AnyObject]
}

class State : DictionaryEncoding {

    let id: String
    let name: String
    
    
    init(id: String, name: String) {
        self.id = id
        self.name = name
    }
    
    init(state: ManagedState) {
        id = state.id
        name = state.name
    }
    
    func dictionaryEncoded() -> [String : AnyObject] {
        return ["id" : id, "name" : name]
    }
}
class ManagedState : NSManagedObject {
    @NSManaged var id: String
    @NSManaged var name: String
    
    func manageState(state: State) {
        id = state.id
        name = state.name
    }
}

class Country : DictionaryEncoding {
    let id: String
    let name: String
    
    init(id: String, name: String) {
        self.id = id
        self.name = name
    }
    
    init(country: ManagedCountry) {
        id = country.id
        name = country.name
    }
    
    func dictionaryEncoded() -> [String : AnyObject] {
        return ["id" : id, "name" : name]
    }
    
//    func jsonEncodedString() -> String? {
//        if let _dictionaryEncoded = dictionaryEncoded() {
//            if let data = NSJSONSerialization.dataWithJSONObject(dictionaryEncoded(), options: NSJSONWritingOptions.PrettyPrinted, error: nil) {
//                return NSString(data: data, encoding: NSUTF8StringEncoding)
//            }
//        }
//        return nil
//    }
}

class ManagedCountry : NSManagedObject {
    @NSManaged var id: String
    @NSManaged var name: String
    
    func manageCountry(country: Country) {
        id = country.id
        name = country.name
    }
}

class Address : DictionaryEncoding {
    
    let lowLevelSpatialReference: String
    let highLevelSpatialReference: String
    let state: State
    let country: Country
    
    init(address: ManagedAddress) {
        lowLevelSpatialReference = address.lowLevelSpatialReference
        highLevelSpatialReference = address.highLevelSpatialReference
        state = State(state: address.state)
        country = Country(country: address.country)
    }
    
    init(lowLevelSpatialReference: String, highLevelSpatialReference: String, state: State, country: Country) {
        self.lowLevelSpatialReference = lowLevelSpatialReference
        self.highLevelSpatialReference = highLevelSpatialReference
        self.state = state
        self.country = country
    }
    func dictionaryEncoded() -> [String : AnyObject] {
        var address = [String : AnyObject]()
        address["lowLevelSpatialReference"] = lowLevelSpatialReference
        address["highLevelSpatialReference"] = highLevelSpatialReference
        address["state"] = state.dictionaryEncoded()
        address["country"] = country.dictionaryEncoded()
        return address
    }
}
class ManagedAddress : NSManagedObject {
    @NSManaged var lowLevelSpatialReference: String
    @NSManaged var highLevelSpatialReference: String
    @NSManaged var state: ManagedState
    @NSManaged var country: ManagedCountry
    
    func manageAddress(address: Address) {
        lowLevelSpatialReference = address.lowLevelSpatialReference
        highLevelSpatialReference = address.highLevelSpatialReference
        if let _managedObjectContext = self.managedObjectContext {
            state = _managedObjectContext.newObjectOfEntity("State") as ManagedState
            state.manageState(address.state)
            country = _managedObjectContext.newObjectOfEntity("Country") as ManagedCountry
            country.manageCountry(address.country)
        }
    }
}

class Contact : DictionaryEncoding {
    let id: String
    let value: String
    
    init(contact: ManagedContact) {
        id = contact.id
        value = contact.value
    }
    
    init(id: String, value: String) {
        self.id = id
        self.value = value
    }
    
    func dictionaryEncoded() -> [String : AnyObject] {
        var contact = [String : AnyObject]()
        contact["id"] = id
        contact["value"] = value
        return contact
    }
}
class ManagedContact : NSManagedObject {
    @NSManaged var id: String
    @NSManaged var value: String
    
    func manageContact(contact: Contact) {
        id = contact.id
        value = contact.value
    }
}

class EmailContact : Contact {
    
    init(contact: ManagedEmailContact) {
        super.init(contact: contact)
    }
    
    override init(id: String, value: String) {
        super.init(id: id, value: value)
    }
}
class ManagedEmailContact : ManagedContact {
    
    func manageEmailContact(contact: EmailContact) {
        super.manageContact(contact)
    }
}

class TelephoneContact : Contact {
    
    init(contact: ManagedTelephoneContact) {
        super.init(contact: contact)
    }
    
    override init(id: String, value: String) {
        super.init(id: id, value: value)
    }
}
class ManagedTelephoneContact : ManagedContact {
    
    func manageTelephoneContact(contact: TelephoneContact) {
        super.manageContact(contact)
    }
}

class MobileContact : Contact {
    
    init(contact: ManagedMobileContact) {
        super.init(contact: contact)
    }
    
    override init(id: String, value: String) {
        super.init(id: id, value: value)
    }
}
class ManagedMobileContact : ManagedContact {
    
    func manageMobileContact(contact: MobileContact) {
        super.manageContact(contact)
    }
}

class FaxContact : Contact {
    
    init(contact: ManagedFaxContact) {
        super.init(contact: contact)
    }
    
    override init(id: String, value: String) {
        super.init(id: id, value: value)
    }
}
class ManagedFaxContact : ManagedContact {
    
    func manageFaxContact(contact: FaxContact) {
        super.manageContact(contact)
    }
}


class Person : DictionaryEncoding {
    let id: String
    let name: String
    let designation: String
    let email: EmailContact
    let telephone: TelephoneContact
    let fax: FaxContact?
    let mobile: MobileContact
    let verificationReference: VerificationReference?
    
    init(id: String, name: String, designation: String, email: EmailContact, telephone: TelephoneContact, fax: FaxContact?, mobile: MobileContact, verificationReference: VerificationReference?) {
        self.id = id
        self.name = name
        self.designation = designation
        self.email = email
        self.telephone = telephone
        self.fax = fax
        self.mobile = mobile
        self.verificationReference = verificationReference
    }
    
    init(person: ManagedPerson) {
        id = person.id
        name = person.name
        designation = person.designation
        email = EmailContact(contact: person.email)
        telephone = TelephoneContact(contact: person.telephone)
        if let _fax = person.fax {
            fax = FaxContact(contact: _fax)
        }
        if let _verficationReference = person.verificationReference {
            verificationReference = VerificationReference(verificationReference: _verficationReference)
        }
        mobile = MobileContact(contact: person.mobile)
    }
    
    func dictionaryEncoded() -> [String : AnyObject] {
        var person = [String : AnyObject]()
        person["id"] = id
        person["name"] = name
        person["designation"] = designation
        person["email"] = email.dictionaryEncoded()
        person["telephone"] = telephone.dictionaryEncoded()
        if let _fax = fax {
            person["fax"] = _fax.dictionaryEncoded()
        }
        if let _verificationReference = verificationReference {
            person["verificationReference"] = _verificationReference.dictionaryEncoded()
        }
        person["mobile"] = mobile.dictionaryEncoded()
        return person
    }
}
class ManagedPerson : NSManagedObject {
    @NSManaged var id: String
    @NSManaged var name: String
    @NSManaged var designation: String
    @NSManaged var email: ManagedEmailContact
    @NSManaged var telephone: ManagedTelephoneContact
    @NSManaged var fax: ManagedFaxContact?
    @NSManaged var mobile: ManagedMobileContact
    @NSManaged var verificationReference: ManagedVerificationReference?

    func managePerson(person: Person) {
        id = person.id
        name = person.name
        designation = person.designation
        if let _managedObjectContext = self.managedObjectContext {
            email = _managedObjectContext.newObjectOfEntity("EmailContact") as ManagedEmailContact
            email.manageEmailContact(person.email)
            telephone = NSEntityDescription.insertNewObjectForEntityForName("TelephoneContact", inManagedObjectContext: _managedObjectContext) as ManagedTelephoneContact
            telephone.manageTelephoneContact(person.telephone)
            if let _fax = person.fax {
                fax = NSEntityDescription.insertNewObjectForEntityForName("FaxContact", inManagedObjectContext: _managedObjectContext) as? ManagedFaxContact
                fax!.manageFaxContact(_fax)
            }
            mobile = NSEntityDescription.insertNewObjectForEntityForName("MobileContact", inManagedObjectContext: _managedObjectContext) as ManagedMobileContact
            mobile.manageMobileContact(person.mobile)
            if let _verficationReference = person.verificationReference {
                verificationReference = NSEntityDescription.insertNewObjectForEntityForName("VerificationReference", inManagedObjectContext: _managedObjectContext) as? ManagedVerificationReference
                verificationReference!.manageVerificationReference(_verficationReference)
            }
        }
    }
}

class Company : DictionaryEncoding {
    
    let name: String
    let registrationNumber: String
    let businessAddress: Address
    let businessNature: String
    let email: EmailContact?
    let telephone: TelephoneContact
    let fax: FaxContact
    let website: String?

    init(company: ManagedCompany) {
        name = company.name
        registrationNumber = company.registrationNumber
        businessAddress = Address(address: company.businessAddress)
        businessNature = company.businessNature
        if let managedEmail = company.email {
            email = EmailContact(contact:managedEmail)
        }
        telephone = TelephoneContact(contact:company.telephone)
        fax = FaxContact(contact:company.fax)
        website = company.website
    }
    
    init(name: String, registrationNumber: String, businessAddress: Address, businessNature: String, email: EmailContact?, telephone: TelephoneContact, fax: FaxContact, website: String?) {
        self.name = name
        self.registrationNumber = registrationNumber
        self.businessAddress = businessAddress
        self.businessNature = businessNature
        self.email = email
        self.telephone = telephone
        self.fax = fax
        self.website = website
    }
    
    func dictionaryEncoded() -> [String : AnyObject] {
        var company = [String : AnyObject]()
        company["name"] = name
        company["businessRegistrationNo"] = registrationNumber
        company["businessAddress"] = businessAddress.dictionaryEncoded()
        company["businessNature"] = businessNature
        if let _email = email {
            company["email"] = _email.dictionaryEncoded()
        }
        company["telephone"] = telephone.dictionaryEncoded()
        company["fax"] = fax.dictionaryEncoded()
        if let _website = website {
            company["website"] = _website
        }
        return company
    }
}

class ManagedCompany : NSManagedObject {
    @NSManaged var name: String
    @NSManaged var registrationNumber: String
    @NSManaged var businessAddress: ManagedAddress
    @NSManaged var businessNature: String
    @NSManaged var email: ManagedEmailContact?
    @NSManaged var telephone: ManagedTelephoneContact
    @NSManaged var fax: ManagedFaxContact
    @NSManaged var website: String?
    
    func manageCompany(company: Company) {
        name = company.name
        registrationNumber = company.registrationNumber
        businessNature = company.businessNature
        website = company.website

        if let _managedObjectContext = self.managedObjectContext {
            businessAddress = NSEntityDescription.insertNewObjectForEntityForName("Address", inManagedObjectContext: _managedObjectContext) as ManagedAddress
            businessAddress.manageAddress(company.businessAddress)
            if let companyEmail = company.email {
                email = NSEntityDescription.insertNewObjectForEntityForName("EmailContact", inManagedObjectContext: _managedObjectContext) as? ManagedEmailContact
                email!.manageEmailContact(companyEmail)
            }
            telephone = NSEntityDescription.insertNewObjectForEntityForName("TelephoneContact", inManagedObjectContext: _managedObjectContext) as ManagedTelephoneContact
            telephone.manageTelephoneContact(company.telephone)
            fax = NSEntityDescription.insertNewObjectForEntityForName("FaxContact", inManagedObjectContext: _managedObjectContext) as ManagedFaxContact
            fax.manageFaxContact(company.fax)
        }
    }
}

class CompanyContactPeople {
    
    let primaryContactPerson: Person
    let secondaryContactPerson: Person
    
    init(primaryContactPerson: Person, secondaryContactPerson: Person) {
        self.primaryContactPerson = primaryContactPerson
        self.secondaryContactPerson = secondaryContactPerson
    }
    
    init(companyContactPeople: ManagedCompanyContactPeople) {
        primaryContactPerson = Person(person: companyContactPeople.primaryContactPerson)
        secondaryContactPerson = Person(person: companyContactPeople.secondaryContactPerson)
    }
}

class ManagedCompanyContactPeople : NSManagedObject {
    @NSManaged var primaryContactPerson: ManagedPerson
    @NSManaged var secondaryContactPerson: ManagedPerson
    
    func manageCompanyContactPeople(companyContactPeople: CompanyContactPeople) {
        if let _managedObjectContext = self.managedObjectContext {
            primaryContactPerson = NSEntityDescription.insertNewObjectForEntityForName("Person", inManagedObjectContext: _managedObjectContext) as ManagedPerson
            primaryContactPerson.managePerson(companyContactPeople.primaryContactPerson)
            secondaryContactPerson = NSEntityDescription.insertNewObjectForEntityForName("Person", inManagedObjectContext: _managedObjectContext) as ManagedPerson
            secondaryContactPerson.managePerson(companyContactPeople.secondaryContactPerson)
        }
    }
}

class AccountPortfolioSummaryService : DictionaryEncoding {
    
    let id: String
    let title: String
    
    init(id: String, title: String) {
        self.id = id
        self.title = title
    }
    init(accountPortfolioSummaryService: ManagedAccountPortfolioSummaryService) {
        id = accountPortfolioSummaryService.id
        title = accountPortfolioSummaryService.title
    }
    
    func dictionaryEncoded() -> [String : AnyObject] {
        var accountPortfolioSummaryService = [String : AnyObject]()
        accountPortfolioSummaryService["id"] = id
        accountPortfolioSummaryService["title"] = title
        return accountPortfolioSummaryService
    }
}

class ManagedAccountPortfolioSummaryService : NSManagedObject {
    @NSManaged var id: String
    @NSManaged var title: String
    
    func manageAccountPortfolioSummaryService(accountPortfolioSummaryService: AccountPortfolioSummaryService) {
        id = accountPortfolioSummaryService.id
        title = accountPortfolioSummaryService.title
    }
}

func == (lhs: AccountPortfolioSummaryService, rhs: AccountPortfolioSummaryService) -> Bool {
    return lhs.id == rhs.id
}

func == (lhs: SaturatoryBody, rhs: SaturatoryBody) -> Bool {
    return lhs.id == rhs.id
}

class SaturatoryBody : DictionaryEncoding {
    let id: String
    let name: String
    
    init(id: String, name: String) {
        self.id = id
        self.name = name
    }
    
    init(saturatoryBody: ManagedSaturatoryBody) {
        id = saturatoryBody.id
        name = saturatoryBody.name
    }
    
    func dictionaryEncoded() -> [String : AnyObject] {
        var saturatoryBody = [String : AnyObject]()
        saturatoryBody["id"] = id
        saturatoryBody["name"] = name
        return saturatoryBody
    }
}

class ManagedSaturatoryBody : NSManagedObject {
    @NSManaged var id: String
    @NSManaged var name: String
    
    func manageSaturatoryBody(saturatoryBody: SaturatoryBody) {
        id = saturatoryBody.id
        name = saturatoryBody.name
    }
}

func == (lhs: Employer, rhs: Employer) -> Bool {
    return lhs.id == rhs.id
}

class Employer : DictionaryEncoding {
    let id: String
    let name: String
    
    init(id: String, title: String) {
        self.id = id
        self.name = title
    }
    
    init(employer: ManagedEmployer) {
        id = employer.id
        name = employer.name
    }
    
    func dictionaryEncoded() -> [String : AnyObject] {
        var employer = [String : AnyObject]()
        employer["id"] = id
        employer["name"] = name
        return employer
    }
}

class ManagedEmployer : NSManagedObject {
    @NSManaged var id: String
    @NSManaged var name: String
    
    func manageEmployer(employer: Employer) {
        id = employer.id
        name = employer.name
    }
}

func == (lhs: SaturatoryBodyPaymentInfo, rhs: SaturatoryBodyPaymentInfo) -> Bool {
    return lhs.id == rhs.id
}

class SaturatoryBodyPaymentInfo : DictionaryEncoding {
    let id: String
    let saturatoryBody: SaturatoryBody
    let employer: Employer
    
    init(id: String, saturatoryBody: SaturatoryBody, employer: Employer) {
        self.id = id
        self.saturatoryBody = saturatoryBody
        self.employer = employer
    }
    
    init(saturatoryBodyPaymentInfo: ManagedSaturatoryBodyPaymentInfo) {
        id = saturatoryBodyPaymentInfo.id
        saturatoryBody = SaturatoryBody(saturatoryBody: saturatoryBodyPaymentInfo.saturatoryBody)
        employer = Employer(employer: saturatoryBodyPaymentInfo.employer)
    }
    
    func dictionaryEncoded() -> [String : AnyObject] {
        var saturatoryBodyPaymentInfo = [String : AnyObject]()
        saturatoryBodyPaymentInfo["id"] = id
        saturatoryBodyPaymentInfo["saturatoryBody"] = saturatoryBody.dictionaryEncoded()
        saturatoryBodyPaymentInfo["employer"] = employer.dictionaryEncoded()
        return saturatoryBodyPaymentInfo
    }
}

class ManagedSaturatoryBodyPaymentInfo : NSManagedObject {
    @NSManaged var id: String
    @NSManaged var saturatoryBody: ManagedSaturatoryBody
    @NSManaged var employer: ManagedEmployer
    
    func manageSaturatoryBodyPaymentInfo(saturatoryBodyPaymentInfo: SaturatoryBodyPaymentInfo) {
        id = saturatoryBodyPaymentInfo.id
        if let _managedObjectContext = self.managedObjectContext {
            saturatoryBody = NSEntityDescription.insertNewObjectForEntityForName("SaturatoryBody", inManagedObjectContext: _managedObjectContext) as ManagedSaturatoryBody
            saturatoryBody.manageSaturatoryBody(saturatoryBodyPaymentInfo.saturatoryBody)
            employer = NSEntityDescription.insertNewObjectForEntityForName("Employer", inManagedObjectContext: _managedObjectContext) as ManagedEmployer
            employer.manageEmployer(saturatoryBodyPaymentInfo.employer)
        }
    }
}

class AccountModule : DictionaryEncoding {
    
    let id: String
    let title: String
    
    init(id: String, title: String) {
        self.id = id
        self.title = title
    }
    
    init(accountModule: ManagedAccountModule) {
        id = accountModule.id
        title = accountModule.title
    }
    
    func dictionaryEncoded() -> [String : AnyObject] {
        var accountModule = [String : AnyObject]()
        accountModule["id"] = id
        accountModule["title"] = title
        return accountModule
    }
}

class ManagedAccountModule : NSManagedObject {
    @NSManaged var id: String
    @NSManaged var title: String
    
    func manageAccountModule(accountModule: AccountModule) {
        id = accountModule.id
        title = accountModule.title
    }
}

func == (lhs: AccountModule, rhs: AccountModule) -> Bool {
    return lhs.id == rhs.id
}

class AccountPaymentManagementService : DictionaryEncoding {
    
    let id: String
    let title: String
    
    init(id: String, title: String) {
        self.id = id
        self.title = title
    }
    
    init(accountPaymentManagementService: ManagedAccountPaymentManagementService) {
        id = accountPaymentManagementService.id
        title = accountPaymentManagementService.title
    }
    
    func dictionaryEncoded() -> [String : AnyObject] {
        var accountPaymentManagementService = [String : AnyObject]()
        accountPaymentManagementService["id"] = id
        accountPaymentManagementService["title"] = title
        return accountPaymentManagementService
    }
}
class ManagedAccountPaymentManagementService : NSManagedObject {
    @NSManaged var id: String
    @NSManaged var title: String
    
    func manageAccountPaymentManagementService(accountPaymentManagementService: AccountPaymentManagementService) {
        id = accountPaymentManagementService.id
        title = accountPaymentManagementService.title
    }
}

func == (lhs: AccountPaymentManagementService, rhs: AccountPaymentManagementService) -> Bool {
    return lhs.id == rhs.id
}

class TransferPurpose : DictionaryEncoding {
    
    let id: String
    let title: String
    
    init(id: String, title: String) {
        self.id = id
        self.title = title
    }
    
    init(transferPurpose: ManagedTransferPurpose) {
        id = transferPurpose.id
        title = transferPurpose.title
    }
    
    func dictionaryEncoded() -> [String : AnyObject] {
        var transferPurpose = [String : AnyObject]()
        transferPurpose["id"] = id
        transferPurpose["title"] = title
        return transferPurpose
    }
}
class ManagedTransferPurpose : NSManagedObject {
    @NSManaged var id: String
    @NSManaged var title: String
    
    func manageTransferPurpose(transferPurpose: TransferPurpose) {
        id = transferPurpose.id
        title = transferPurpose.title
    }
}

class AdminMaker : Person {
    
    override init(id: String, name: String, designation: String, email: EmailContact, telephone: TelephoneContact, fax: FaxContact?, mobile: MobileContact, verificationReference: VerificationReference?) {
        super.init(id: id, name: name, designation: designation, email: email, telephone: telephone, fax: fax, mobile: mobile, verificationReference: verificationReference)
    }
    
    init(adminMaker: ManagedAdminMaker) {
        super.init(person: adminMaker)
    }
}

class ManagedAdminMaker : ManagedPerson {
    
    func manageAdminMaker(adminMaker: AdminMaker) {
        super.managePerson(adminMaker)
    }
}

class AdminChecker : Person {
    
    override init(id: String, name: String, designation: String, email: EmailContact, telephone: TelephoneContact, fax: FaxContact?, mobile: MobileContact, verificationReference: VerificationReference?) {
        super.init(id: id, name: name, designation: designation, email: email, telephone: telephone, fax: fax, mobile: mobile, verificationReference: verificationReference)
    }
    
    init(adminChecker: ManagedAdminChecker) {
        super.init(person: adminChecker)
    }
}

class ManagedAdminChecker : ManagedPerson {
    
    func manageAdminChecker(adminChecker: AdminChecker) {
        super.managePerson(adminChecker)
    }
}

class BillingAccount : DictionaryEncoding {
    
    let id: String
    let isNonResident: Bool
    
    init(id: String, isNonResident: Bool) {
        self.id = id
        self.isNonResident = isNonResident
    }
    
    init(billingAcount: ManagedBillingAccount) {
        id = billingAcount.id
        isNonResident = billingAcount.isNonResident
    }
    
    func dictionaryEncoded() -> [String : AnyObject] {
        var billingAccount = [String : AnyObject]()
        billingAccount["id"] = id
        billingAccount["isNonResident"] = isNonResident
        return billingAccount
    }
}
class ManagedBillingAccount : NSManagedObject {
    @NSManaged var id: String
    @NSManaged var isNonResident: Bool
    
    func manageBillingAccount(billingAccount: BillingAccount) {
        id = billingAccount.id
        isNonResident = billingAccount.isNonResident
    }
}

class VerificationReferenceType : DictionaryEncoding {
    
    let id: String
    let name: String
    
    init(id: String, name: String) {
        self.id = id
        self.name = name
    }
    
    init(verificationReferenceType: ManagedVerificationReferenceType) {
        print(verificationReferenceType.id)
        id = verificationReferenceType.id
        name = verificationReferenceType.name
    }
    
    func dictionaryEncoded() -> [String : AnyObject] {
        var verificationReferenceType = [String : AnyObject]()
        verificationReferenceType["id"] = id
        verificationReferenceType["name"] = name
        return verificationReferenceType
    }
}
class ManagedVerificationReferenceType : NSManagedObject {
    @NSManaged var id: String
    @NSManaged var name: String
    
    func manageVerificationReferenceType(verificationReferenceType: VerificationReferenceType) {
        id = verificationReferenceType.id
        name = verificationReferenceType.name
    }
}

class VerificationReference : DictionaryEncoding {
    
    let type: VerificationReferenceType
    let verificationReferenceId: String
    
    init(type: VerificationReferenceType, verificationReferenceId: String) {
        self.type = type
        self.verificationReferenceId = verificationReferenceId
    }
    
    init(verificationReference: ManagedVerificationReference) {
        type = VerificationReferenceType(verificationReferenceType: verificationReference.type)
        verificationReferenceId = verificationReference.verificationReferenceId
    }
    
    func dictionaryEncoded() -> [String : AnyObject] {
        var verificationReference = [String : AnyObject]()
        verificationReference["type"] = type.dictionaryEncoded()
        verificationReference["verificationReferenceId"] = verificationReferenceId
        return verificationReference
    }
}

class ManagedVerificationReference : NSManagedObject {
    @NSManaged var type: ManagedVerificationReferenceType
    @NSManaged var verificationReferenceId: String
    
    func manageVerificationReference(verificationReference: VerificationReference?) {
        if let _managedObjectContext = self.managedObjectContext {
            if let _verificationReference = verificationReference {
                type = NSEntityDescription.insertNewObjectForEntityForName("VerificationReferenceType", inManagedObjectContext: _managedObjectContext) as ManagedVerificationReferenceType
                type.manageVerificationReferenceType(_verificationReference.type)
                verificationReferenceId = _verificationReference.verificationReferenceId
            }
        }
    }
}

class AccountUserRole : DictionaryEncoding {
    
    let id: String
    let title: String
    
    init(id: String, title: String) {
        self.id = id
        self.title = title
    }
    
    init(accountUserRole: ManagedAccountUserRole) {
        id = accountUserRole.id
        title = accountUserRole.title
    }
    
    func dictionaryEncoded() -> [String : AnyObject] {
        var accountUserRole = [String : AnyObject]()
        accountUserRole["id"] = id
        accountUserRole["title"] = title
        return accountUserRole
    }
}

class ManagedAccountUserRole : NSManagedObject {
    @NSManaged var id: String
    @NSManaged var title: String
    
    func manageAccountUserRole(accountUserRole: AccountUserRole) {
        id = accountUserRole.id
        title = accountUserRole.title
    }
}

func == (lhs: AccountUserRole?, rhs: AccountUserRole?) -> Bool {
    if let _lhs = lhs {
        if let _rhs = rhs {
            return _lhs.id == _rhs.id
        }
    }
    else if let _ = rhs {
        
    }
    else {
        return true
    }
    return false
}

class AccountUserPermission : DictionaryEncoding, Equatable {
    
    let id: String
    let title: String
    
    init(id: String, title: String) {
        self.id = id
        self.title = title
    }
    
    init(accountUserPermission: ManagedAccountUserPermission) {
        id = accountUserPermission.id
        title = accountUserPermission.title
    }
    
    func dictionaryEncoded() -> [String : AnyObject] {
        var accountUserPermission = [String : AnyObject]()
        accountUserPermission["id"] = id
        accountUserPermission["title"] = title
        return accountUserPermission
    }
}

class ManagedAccountUserPermission : NSManagedObject {
    @NSManaged var id: String
    @NSManaged var title: String
    
    func manageAccountUserPermission(accountUserPermission: AccountUserPermission) {
        id = accountUserPermission.id
        title = accountUserPermission.title
    }
}

func == (lhs: AccountUserPermission, rhs: AccountUserPermission) -> Bool {
    return lhs.id == rhs.id
}

class AccountUserApprovalGroup : DictionaryEncoding {
    
    let id: String
    let name: String
    
    init(id: String, name: String) {
        self.id = id
        self.name = name
    }
    
    init(accountUserApprovalGroup: ManagedAccountUserApprovalGroup) {
        id = accountUserApprovalGroup.id
        name = accountUserApprovalGroup.name
    }
    
    func dictionaryEncoded() -> [String : AnyObject] {
        var accountUserApprovalGroup = [String : AnyObject]()
        accountUserApprovalGroup["id"] = id
        accountUserApprovalGroup["name"] = name
        return accountUserApprovalGroup
    }
}

class ManagedAccountUserApprovalGroup : NSManagedObject {
    @NSManaged var id: String
    @NSManaged var name: String
    
    func manageAccountUserApprovalGroup(accountUserApprovalGroup: AccountUserApprovalGroup) {
        id = accountUserApprovalGroup.id
        name = accountUserApprovalGroup.name
    }
}

func == (lhs: AccountUserApprovalGroup?, rhs: AccountUserApprovalGroup?) -> Bool {
    if let _lhs = lhs {
        if let _rhs = rhs {
            return _lhs.id == _rhs.id
        }
    }
    else if let _ = rhs {
        
    }
    else {
        return true
    }
    return false
}

class AccountUserRight : DictionaryEncoding {
    
    let role: AccountUserRole?
    let permissions: [AccountUserPermission]
    let canAuthorizerViewDetails: Bool
    let approvalGroup: AccountUserApprovalGroup?
    
    init(role: AccountUserRole?, permissions: [AccountUserPermission], canAuthorizerViewDetails: Bool, approvalGroup: AccountUserApprovalGroup?) {
        self.role = role
        self.permissions = permissions
        self.canAuthorizerViewDetails = canAuthorizerViewDetails
        self.approvalGroup = approvalGroup
    }
    
    init(accountUserRight: ManagedAccountUserRight) {
        if let _role = accountUserRight.role {
            role = AccountUserRole(accountUserRole: _role)
        }
        else {
            role = nil
        }
        permissions = []
        for managedPermission in accountUserRight.permissions {
            permissions.append(AccountUserPermission(accountUserPermission: managedPermission as ManagedAccountUserPermission))
        }
        canAuthorizerViewDetails = accountUserRight.canAuthorizerViewDetails
        if let _approvalGroup = accountUserRight.approvalGroup {
            approvalGroup = AccountUserApprovalGroup(accountUserApprovalGroup: _approvalGroup)
        }
        else {
            approvalGroup = nil
        }
    }
    
    func dictionaryEncoded() -> [String : AnyObject] {
        var accountUserRight = [String : AnyObject]()
        if let _role = role {
            accountUserRight["role"] = _role.dictionaryEncoded()
        }
        var permissionsDicts = [[String : AnyObject]]()
        for permission in permissions {
            permissionsDicts.append(permission.dictionaryEncoded())
        }
        accountUserRight["permissions"] = permissionsDicts
        accountUserRight["canAuthorizerViewDetails"] = canAuthorizerViewDetails
        if let _approvalGroup = approvalGroup {
            accountUserRight["approvalGroup"] = _approvalGroup.dictionaryEncoded()
        }
        return accountUserRight
    }
}
class ManagedAccountUserRight : NSManagedObject {
    @NSManaged var role: ManagedAccountUserRole?
    @NSManaged var permissions: NSSet
    @NSManaged var canAuthorizerViewDetails: Bool
    @NSManaged var approvalGroup: ManagedAccountUserApprovalGroup?
    
    func manageAccountUserRight(accountUserRight: AccountUserRight) {
        if let _managedObjectContext = self.managedObjectContext {
            if let _role = accountUserRight.role {
                role = NSEntityDescription.insertNewObjectForEntityForName("AccountUserRole", inManagedObjectContext: _managedObjectContext) as ManagedAccountUserRole
                role!.manageAccountUserRole(_role)
            }
            else {
                role = nil
            }
            var _permissions = [ManagedAccountUserPermission]()
            for accountUserPermission in accountUserRight.permissions {
                let managedAccountUserPermission = NSEntityDescription.insertNewObjectForEntityForName("AccountUserPermission", inManagedObjectContext: _managedObjectContext) as ManagedAccountUserPermission
                managedAccountUserPermission.manageAccountUserPermission(accountUserPermission)
                _permissions.append(managedAccountUserPermission)
            }
            permissions = NSSet(array: _permissions)
            canAuthorizerViewDetails = accountUserRight.canAuthorizerViewDetails
            if let _approvalGroup = accountUserRight.approvalGroup {
                approvalGroup = NSEntityDescription.insertNewObjectForEntityForName("AccountUserApprovalGroup", inManagedObjectContext: _managedObjectContext) as ManagedAccountUserApprovalGroup
                approvalGroup!.manageAccountUserApprovalGroup(_approvalGroup)
            }
            else {
                approvalGroup = nil
            }
        }
    }
}

class AccountUser : Person {
    
    let right: AccountUserRight
    
    init(id: String, name: String, designation: String, email: EmailContact, telephone: TelephoneContact, fax: FaxContact?, mobile: MobileContact, verificationReference: VerificationReference?, right: AccountUserRight) {
        
        self.right = right
        
        super.init(id: id, name: name, designation: designation, email: email, telephone: telephone, fax: fax, mobile: mobile, verificationReference: verificationReference)
    }
    
    init(accountUser: ManagedAccountUser) {
        right = AccountUserRight(accountUserRight: accountUser.right)
        super.init(person: accountUser as ManagedPerson)
    }
    
    override func dictionaryEncoded() -> [String : AnyObject] {
        var accountUser = super.dictionaryEncoded()
        accountUser["right"] = right.dictionaryEncoded()
        return accountUser
    }
}

class ManagedAccountUser : ManagedPerson {
    @NSManaged var right: ManagedAccountUserRight
    
    func manageAccountUser(accountUser: AccountUser) {
        if let _managedObjectContext = self.managedObjectContext {
            verificationReference = NSEntityDescription.insertNewObjectForEntityForName("VerificationReference", inManagedObjectContext: _managedObjectContext) as ManagedVerificationReference
            verificationReference?.manageVerificationReference(accountUser.verificationReference)
            right = NSEntityDescription.insertNewObjectForEntityForName("AccountUserRight", inManagedObjectContext: _managedObjectContext) as ManagedAccountUserRight
            right.manageAccountUserRight(accountUser.right)
            super.managePerson(accountUser)
        }
    }
}


func == (lhs: AccountUser, rhs: AccountUser) -> Bool {
    return lhs.id == rhs.id
}


class Declaration : DictionaryEncoding {
    
    let id: String
    let name: String
    let photo: UIImage
    let signature: UIImage
    let date: NSDate
    
    init(id: String, name: String, photo: UIImage, signature: UIImage, date: NSDate) {
        self.id = id
        self.name = name
        self.photo = photo
        self.signature = signature
        self.date = date
    }
    
    init(declaration: ManagedDeclaration) {
        self.id = declaration.id
        self.name = declaration.name
        self.photo = UIImage(data: declaration.photo)!
        self.signature = UIImage(data: declaration.signature)!
        self.date = declaration.date
    }
    
    func dictionaryEncoded() -> [String : AnyObject] {
        var declaration = [String : AnyObject]()
        declaration["id"] = id
        declaration["name"] = name
        declaration["signature"] = UIImageJPEGRepresentation(signature, 0).base64EncodedStringWithOptions(NSDataBase64EncodingOptions.allZeros)
        declaration["photo"] = UIImageJPEGRepresentation(photo, 0).base64EncodedStringWithOptions(NSDataBase64EncodingOptions.allZeros)
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        declaration["date"] = dateFormatter.stringFromDate(date)
        return declaration
    }
}

class ManagedDeclaration : NSManagedObject {
    @NSManaged var id: String
    @NSManaged var name: String
    @NSManaged var photo: NSData
    @NSManaged var signature: NSData
    @NSManaged var date: NSDate
    
    func manageDeclaration(declaration: Declaration) {
        id = declaration.id
        name = declaration.name
        photo = UIImageJPEGRepresentation(declaration.photo, CGFloat(1.0))
        signature = UIImageJPEGRepresentation(declaration.signature, CGFloat(1.0))
        date = declaration.date
    }
}

class Account : DictionaryEncoding {
    
    let id: String
    let modules: [AccountModule]
    let users: [AccountUser]
    
    var saturatoryBodyPaymentInfos: [SaturatoryBodyPaymentInfo]?
    
    init(id: String, modules: [AccountModule], users: [AccountUser]) {
        self.id = id
        self.modules = modules
        self.users = users
    }
    
    convenience init(id: String, modules: [AccountModule], users: [AccountUser], saturatoryBodyPaymentInfos: [SaturatoryBodyPaymentInfo]) {
        self.init(id: id, modules: modules, users: users)
        self.saturatoryBodyPaymentInfos = saturatoryBodyPaymentInfos
    }
    
    init(account: ManagedAccount) {
        self.id = account.id
        modules = []
        for managedModule in account.modules {
            modules.append(AccountModule(accountModule: managedModule as ManagedAccountModule))
        }
        users = []
        for managedUser in account.users {
            users.append(AccountUser(accountUser: managedUser as ManagedAccountUser))
        }
        if account.saturatoryBodyPaymentInfos.count > 0 {
            var _saturatoryBodyPaymentInfos = [SaturatoryBodyPaymentInfo]()
            for saturatoryBodyPaymentInfo in account.saturatoryBodyPaymentInfos {
                _saturatoryBodyPaymentInfos.append(SaturatoryBodyPaymentInfo(saturatoryBodyPaymentInfo: saturatoryBodyPaymentInfo as ManagedSaturatoryBodyPaymentInfo))
            }
            saturatoryBodyPaymentInfos = _saturatoryBodyPaymentInfos
        }
    }
    
    func dictionaryEncoded() -> [String : AnyObject] {
        var account = [String : AnyObject]()
        account["id"] = id
        var modulesDicts = [[String : AnyObject]]()
        for module in modules{
            modulesDicts.append(module.dictionaryEncoded())
        }
        account["modules"] = modulesDicts
        var usersDicts = [[String : AnyObject]]()
        for user in users{
            usersDicts.append(user.dictionaryEncoded())
        }
        account["users"] = usersDicts
        var saturatoryBodyPaymentInfoDicts = [[String : AnyObject]]()
        if let _saturatoryBodyPaymentInfos = saturatoryBodyPaymentInfos {
            for saturatoryBodyPaymentInfo in _saturatoryBodyPaymentInfos {
                saturatoryBodyPaymentInfoDicts.append(saturatoryBodyPaymentInfo.dictionaryEncoded())
            }
        }
        account["saturatoryBodyPaymentInfo"] = saturatoryBodyPaymentInfoDicts
        return account
    }
}

class ManagedAccount : NSManagedObject {
    @NSManaged var id: String
    @NSManaged var modules: NSSet
    @NSManaged var users: NSSet
    @NSManaged var saturatoryBodyPaymentInfos: NSSet

    func manageAccount(account: Account) {
        id = account.id
        modules = NSSet()
        if let _managedObjectContext = self.managedObjectContext {
            var _modules = [ManagedAccountModule]()
            for accountModule in account.modules {
                let managedAccountModule = NSEntityDescription.insertNewObjectForEntityForName("AccountModule", inManagedObjectContext: _managedObjectContext) as ManagedAccountModule
                managedAccountModule.manageAccountModule(accountModule)
                _modules.append(managedAccountModule)
            }
            modules = NSSet(array: _modules)
            var _users = [ManagedAccountUser]()
            for accountUser in account.users {
                let managedAccountUser = NSEntityDescription.insertNewObjectForEntityForName("AccountUser", inManagedObjectContext: _managedObjectContext) as ManagedAccountUser
                managedAccountUser.manageAccountUser(accountUser)
                _users.append(managedAccountUser)
            }
            users = NSSet(array: _users)
            
            var _saturatoryBodyPaymentInfos = [ManagedSaturatoryBodyPaymentInfo]()
            if let __saturatoryBodyPaymentInfos = account.saturatoryBodyPaymentInfos {
                for saturatoryBodyPaymentInfo in __saturatoryBodyPaymentInfos {
                    let managedSaturatoryBodyPaymentInfo = NSEntityDescription.insertNewObjectForEntityForName("SaturatoryBodyPaymentInfo", inManagedObjectContext: _managedObjectContext) as ManagedSaturatoryBodyPaymentInfo
                    managedSaturatoryBodyPaymentInfo.manageSaturatoryBodyPaymentInfo(saturatoryBodyPaymentInfo)
                    _saturatoryBodyPaymentInfos.append(managedSaturatoryBodyPaymentInfo)
                }
            }
            saturatoryBodyPaymentInfos = NSSet(array: _saturatoryBodyPaymentInfos)
        }
    }
}

func == (lhs: Account, rhs: Account) -> Bool {
    return lhs.id == rhs.id
}


class ApplicationCategory /* : RLMObject */ : DictionaryEncoding {

    let region: Region
    let package: Package
    let segment: Segment
    let cifNo: String?
    
    init(applicationCategory: ManagedApplicationCategory) {
        self.region = Region(region: applicationCategory.region)
        self.package = Package(package: applicationCategory.package)
        self.segment = Segment(segment: applicationCategory.segment)
        self.cifNo = applicationCategory.cifNo
    }
    
    init(region: Region, package: Package, segment: Segment, cifNo: String?) {
        self.region = region
        self.package = package
        self.segment = segment
        self.cifNo = cifNo
//        super.init()
    }
    
    func dictionaryEncoded() -> [String : AnyObject] {
        var applicationCategory = [String : AnyObject]()
        applicationCategory["region"] = region.dictionaryEncoded()
        applicationCategory["package"] = package.dictionaryEncoded()
        applicationCategory["segment"] = segment.dictionaryEncoded()
        if let _cifNo = cifNo {
            applicationCategory["cifNo"] = _cifNo
        }
        return applicationCategory
    }
}

class ManagedApplicationCategory : NSManagedObject {
    @NSManaged var region: ManagedRegion
    @NSManaged var package: ManagedPackage
    @NSManaged var segment: ManagedSegment
    @NSManaged var cifNo: String?
    
    func manageApplicationCategory(applicationCategory: ApplicationCategory) {
        if let _managedObjectContext = self.managedObjectContext {
            region = NSEntityDescription.insertNewObjectForEntityForName("Region", inManagedObjectContext: _managedObjectContext) as ManagedRegion
            region.manageRegion(applicationCategory.region)
            package = NSEntityDescription.insertNewObjectForEntityForName("Package", inManagedObjectContext: _managedObjectContext) as ManagedPackage
            package.managePackage(applicationCategory.package)
            segment = NSEntityDescription.insertNewObjectForEntityForName("Segment", inManagedObjectContext: _managedObjectContext) as ManagedSegment
            segment.manageSegment(applicationCategory.segment)
        }
        if let _cifNo = applicationCategory.cifNo {
            cifNo = _cifNo
        }
    }
}

class Segment /* : RLMObject  */ : DictionaryEncoding {
    
    let id: String
    let name: String
    
    init(segment: ManagedSegment) {
        self.id = segment.id
        self.name = segment.name
    }
    
    init(id: String, name: String) {
        self.id = id
        self.name = name
//        super.init()
    }
    
    func dictionaryEncoded() -> [String : AnyObject] {
        var segment = [String : AnyObject]()
        segment["id"] = id
        segment["name"] = name
        return segment
    }
}
class ManagedSegment : NSManagedObject {
    @NSManaged var id: String
    @NSManaged var name: String
    
    func manageSegment(segment: Segment) {
        id = segment.id
        name = segment.name
    }
}

func == (lhs: Segment, rhs: Segment) -> Bool {
    return lhs.id == rhs.id
}


class Region  /* : RLMObject */ : DictionaryEncoding {
    
    let id: String
    let name: String
    var packages: [Package]?
    
    init(region: ManagedRegion) {
        self.id = region.id
        self.name = region.name
    }
    
    init(id: String, name: String) {
        self.id = id
        self.name = name
//        super.init()
    }
    
    convenience init(id: String, name: String, packages:[Package]) {
//        self.id = id
//        self.name = name
        self.init(id: id, name: name)
        self.packages = packages
    }
    
    func dictionaryEncoded() -> [String : AnyObject] {
        var region = [String : AnyObject]()
        region["id"] = id
        region["name"] = name
        return region
    }
}
class ManagedRegion : NSManagedObject {
    @NSManaged var id: String
    @NSManaged var name: String
    @NSManaged var packages: [Package]?
    
    func manageRegion(region: Region) {
        id = region.id
        name = region.name
    }
}

func == (lhs: Region, rhs: Region) -> Bool {
    return lhs.id == rhs.id
}

class Package /* : RLMObject */ : DictionaryEncoding {
    
    let id: String
    let name: String
    
    init(package: ManagedPackage) {
        self.id = package.id
        self.name = package.name
    }
    
    init(id: String, name: String) {
        self.id = id
        self.name = name
//        super.init()
    }
    
    func dictionaryEncoded() -> [String : AnyObject] {
        var package = [String : AnyObject]()
        package["id"] = id
        package["name"] = name
        return package
    }
}

class ManagedPackage : NSManagedObject {
    @NSManaged var id: String
    @NSManaged var name: String
    
    func managePackage(package: Package) {
        id = package.id
        name = package.name
    }
}

func == (lhs: Package, rhs: Package) -> Bool {
    return lhs.id == rhs.id
}

class AuthorizationType : DictionaryEncoding {
    
    let id: String
    let name: String
    
    init(id: String, name: String) {
        self.id = id
        self.name = name
    }
    
    init(authorizationType: ManagedAuthorizationType) {
        id = authorizationType.id
        name = authorizationType.name
    }
    
    func dictionaryEncoded() -> [String : AnyObject] {
        var authorizationType = [String : AnyObject]()
        authorizationType["id"] = id
        authorizationType["name"] = name
        return authorizationType
    }
    
}
class ManagedAuthorizationType : NSManagedObject {
    @NSManaged var id: String
    @NSManaged var name: String
    
    func manageAuthorizationType(authorizationType:AuthorizationType) {
        id = authorizationType.id
        name = authorizationType.name
    }
}

func == (lhs: AuthorizationType, rhs: AuthorizationType) -> Bool {
    return lhs.id == rhs.id
}

class Branch : DictionaryEncoding {
    let id: String
    let name: String
    
    init(id: String, name: String) {
        self.id = id
        self.name = name
    }
    
    init(branch: ManagedBranch) {
        id = branch.id
        name = branch.name
    }
    
    func dictionaryEncoded() -> [String : AnyObject] {
        var branch = [String : AnyObject]()
        branch["id"] = id
        branch["name"] = name
        return branch
    }
}

class ManagedBranch : NSManagedObject {
    @NSManaged var id: String
    @NSManaged var name: String
    
    func manageBranch(branch:Branch) {
        id = branch.id
        name = branch.name
    }
}

class System {
    let id: String
    let name: String
    
    init(id: String, name: String) {
        self.id = id
        self.name = name
    }
    
    init(system: ManagedSystem) {
        id = system.id
        name = system.name
    }
    
    func dictionaryEncoded() -> [String : AnyObject] {
        var system = [String : AnyObject]()
        system["id"] = id
        system["name"] = name
        return system
    }
}

class ManagedSystem : NSManagedObject {
    @NSManaged var id: String
    @NSManaged var name: String
    
    func manageSystem(system:System) {
        id = system.id
        name = system.name
    }
}

class BankAdminstrationDetail : DictionaryEncoding {
    
    let id: String
    let officerName: String
    let providentFundNumber: String
    let initiationBranch: Branch
    let telephoneNumber: String
    let system: System
    let date: NSDate
    
    init(id: String, officerName:String, telephoneNumber: String, providentFundNumber: String, initiationBranch: Branch, system: System, date: NSDate) {
        self.id = id
        self.officerName = officerName
        self.telephoneNumber = telephoneNumber
        self.providentFundNumber = providentFundNumber
        self.initiationBranch = initiationBranch
        self.system = system
        self.date = date
    }
    
    init(bankAdminstrationDetail: ManagedBankAdminstrationDetail) {
        self.id = bankAdminstrationDetail.id
        self.officerName = bankAdminstrationDetail.officerName
        self.telephoneNumber = bankAdminstrationDetail.telephoneNumber
        self.providentFundNumber = bankAdminstrationDetail.providentFundNumber
        self.initiationBranch = Branch(branch: bankAdminstrationDetail.initiationBranch)
        self.system = System(system: bankAdminstrationDetail.system)
        self.date = bankAdminstrationDetail.date
    }
    
    func dictionaryEncoded() -> [String : AnyObject] {
        var bankAdminstrationDetail = [String : AnyObject]()
        bankAdminstrationDetail["id"] =  id
        bankAdminstrationDetail["officerName"] =  officerName
        bankAdminstrationDetail["telephoneNumber"] =  telephoneNumber
        bankAdminstrationDetail["providentFundNumber"] =  providentFundNumber
        bankAdminstrationDetail["initiationBranch"] =  initiationBranch.dictionaryEncoded()
        bankAdminstrationDetail["system"] =  system.dictionaryEncoded()
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "YYYY-MM-dd HH:mm:ss"
        bankAdminstrationDetail["date"] = dateFormatter.stringFromDate(date)
        return bankAdminstrationDetail
    }
}

class ManagedBankAdminstrationDetail : NSManagedObject {
    
    @NSManaged var id: String
    @NSManaged var officerName: String
    @NSManaged var providentFundNumber: String
    @NSManaged var initiationBranch: ManagedBranch
    @NSManaged var telephoneNumber: String
    @NSManaged var system: ManagedSystem
    @NSManaged var date: NSDate
    
    func manageBankAdminstrationDetail(bankAdminstrationDetail: BankAdminstrationDetail) {
        id = bankAdminstrationDetail.id
        officerName = bankAdminstrationDetail.officerName
        providentFundNumber = bankAdminstrationDetail.providentFundNumber
        telephoneNumber = bankAdminstrationDetail.telephoneNumber
        date = bankAdminstrationDetail.date
        if let _managedObjectContext = self.managedObjectContext {
            initiationBranch = NSEntityDescription.insertNewObjectForEntityForName("Branch", inManagedObjectContext: _managedObjectContext) as ManagedBranch
            initiationBranch.manageBranch(bankAdminstrationDetail.initiationBranch)
            system = NSEntityDescription.insertNewObjectForEntityForName("System", inManagedObjectContext: _managedObjectContext) as ManagedSystem
            system.manageSystem(bankAdminstrationDetail.system)
        }
    }
    
}

class Application /* : RLMObject */ : DictionaryEncoding {
    
    let applicationId: String
    
    var referenceNumber: String? {
        
        if let _company = company? {
            var _referenceNumber = "MY/\(_company.registrationNumber)"
            if let date = bankAdministrationDetail?.date {
                let dateFormatter = NSDateFormatter()
                dateFormatter.dateFormat = "ddMMyyyy"
                _referenceNumber += "/\(dateFormatter.stringFromDate(date))"
            }
            return _referenceNumber
        }
        return nil
    }
    
    let category: ApplicationCategory
    var company: Company?
    var companyContactPeople: CompanyContactPeople?
    var billingAccount: BillingAccount?
    var portfolioSummaryServices: [AccountPortfolioSummaryService]?
    var paymentManagementServices: [AccountPaymentManagementService]?
    var transferPurpose: TransferPurpose?
    var adminMaker: AdminMaker?
    var adminChecker: AdminChecker?
    var accountUsers: [AccountUser]?
    var accounts: [Account]?
    var authorizationType: AuthorizationType?
    var customAuthorizationType: String?
    var declarations: [Declaration]?
    var bankAdministrationDetail: BankAdminstrationDetail?
    
    init(applicationId: String, category: ApplicationCategory) {
        self.applicationId = applicationId
        self.category = category
//        super.init()
    }
    
    init(applicationId: String, category: ApplicationCategory, company: Company, companyContactPeople: CompanyContactPeople?, billingAccount: BillingAccount, portfolioSummaryServices: [AccountPortfolioSummaryService], paymentManagementServices: [AccountPaymentManagementService], transferPurpose: TransferPurpose, adminMaker: AdminMaker, adminChecker: AdminChecker, accountUsers: [AccountUser], accounts: [Account], authorizationType: AuthorizationType, customAuthorizationType: String?, declarations: [Declaration], bankAdministrationDetail: BankAdminstrationDetail) {
        self.applicationId = applicationId
        self.category = category
        self.company = company
        self.companyContactPeople = companyContactPeople
        self.billingAccount = billingAccount
        self.portfolioSummaryServices = portfolioSummaryServices
        self.paymentManagementServices = paymentManagementServices
        self.transferPurpose = transferPurpose
        self.adminMaker = adminMaker
        self.adminChecker = adminChecker
        self.accountUsers = accountUsers
        self.accounts = accounts
        self.authorizationType = authorizationType
        self.customAuthorizationType = customAuthorizationType
        self.declarations = declarations
        self.bankAdministrationDetail = bankAdministrationDetail
//        super.init()
    }
    
    init(application: ManagedApplication) {
        
        self.applicationId = application.applicationId
        self.category = ApplicationCategory(applicationCategory: application.category)
        if let managedCompany = application.company {
            self.company = Company(company: managedCompany)
        }
        if let managedCompanyContactPeople = application.companyContactPeople {
            self.companyContactPeople = CompanyContactPeople(companyContactPeople: managedCompanyContactPeople)
        }
        if let managedBilllingAccount = application.billingAccount {
            self.billingAccount = BillingAccount(billingAcount: managedBilllingAccount)
        }
        if let managedPortfolioSummaryServices = application.portfolioSummaryServices {
            portfolioSummaryServices = []
            for managedPortfolioSummaryService in managedPortfolioSummaryServices {
                portfolioSummaryServices?.append(AccountPortfolioSummaryService(accountPortfolioSummaryService: managedPortfolioSummaryService as ManagedAccountPortfolioSummaryService))
            }
        }
        if let managedPaymentManagementServices = application.paymentManagementServices {
            paymentManagementServices = []
            for managedPaymentManagementService in managedPaymentManagementServices {
                paymentManagementServices?.append(AccountPaymentManagementService(accountPaymentManagementService: managedPaymentManagementService as ManagedAccountPaymentManagementService))
            }
        }
        if let managedTransferPurpose = application.transferPurpose {
            self.transferPurpose = TransferPurpose(transferPurpose: managedTransferPurpose)
        }
        if let managedAdminMaker = application.adminMaker {
            self.adminMaker = AdminMaker(adminMaker: managedAdminMaker)
        }
        if let managedAdminChecker = application.adminChecker {
            self.adminChecker = AdminChecker(adminChecker: managedAdminChecker)
        }
        if let managedAccountUsers = application.accountUsers {
            accountUsers = []
            for managedAccountUser in managedAccountUsers {
                accountUsers?.append(AccountUser(accountUser: managedAccountUser as ManagedAccountUser))
            }
        }
        if let managedAccounts = application.accounts {
            accounts = []
            for managedAccount in managedAccounts {
                accounts?.append(Account(account: managedAccount as ManagedAccount))
            }
        }
        if let managedAuthorizationType = application.authorizationType {
            authorizationType = AuthorizationType(authorizationType: managedAuthorizationType)
        }
        customAuthorizationType = application.customAuthorizationType
        if let managedDeclarations = application.declarations {
            declarations = []
            for managedDeclaration in managedDeclarations {
                declarations?.append(Declaration(declaration: managedDeclaration as ManagedDeclaration))
            }
        }
        if let managedBankAdministrationDetail = application.bankAdministrationDetail {
            bankAdministrationDetail = BankAdminstrationDetail(bankAdminstrationDetail: managedBankAdministrationDetail)
        }
    }
    
    func dictionaryEncoded() -> [String : AnyObject] {
        var application = [String : AnyObject]()
        
        application["id"] =  applicationId
        if let _referenceNumber = referenceNumber {
            application["referenceNumber"] =  _referenceNumber
        }
        application["category"] = category.dictionaryEncoded()
        if let _company = company {
            application["company"] =  _company.dictionaryEncoded()
        }
        if let _company = company {
            application["company"] =  _company.dictionaryEncoded()
        }
        if let _primaryContactInformation = companyContactPeople?.primaryContactPerson {
            application["primaryContactInformation"] =  _primaryContactInformation.dictionaryEncoded()
        }
        if let _secondaryContactInformation = companyContactPeople?.secondaryContactPerson {
            application["secondaryContactInformation"] =  _secondaryContactInformation.dictionaryEncoded()
        }
        if let _billingAccount = billingAccount {
            application["billingAccount"] =  _billingAccount.dictionaryEncoded()
        }
        if let _portfolioSummaryServices = portfolioSummaryServices {
            var portfolioSummaryServicesDicts = [[String : AnyObject]]()
            for portfolioSummaryService in _portfolioSummaryServices {
                portfolioSummaryServicesDicts.append(portfolioSummaryService.dictionaryEncoded())
            }
            application["portfolioSummaryServices"] = portfolioSummaryServicesDicts
        }
        if let _paymentManagementServices = paymentManagementServices {
            var paymentManagementServicesDicts = [[String : AnyObject]]()
            for paymentManagementService in _paymentManagementServices {
                paymentManagementServicesDicts.append(paymentManagementService.dictionaryEncoded())
            }
            application["paymentManagementServices"] = paymentManagementServicesDicts
        }
        if let _transferPurpose = transferPurpose {
            application["transferPurpose"] = _transferPurpose.dictionaryEncoded()
        }
        if let _adminMaker = adminMaker {
            application["adminMaker"] = _adminMaker.dictionaryEncoded()
        }
        if let _adminChecker = adminChecker {
            application["adminChecker"] =  _adminChecker.dictionaryEncoded()
        }
        if let _accountUsers = accountUsers {
            var accountUsersDicts = [[String : AnyObject]]()
            for accountUser in _accountUsers {
                accountUsersDicts.append(accountUser.dictionaryEncoded())
            }
            application["accountUsers"] = accountUsersDicts
        }
        if let _accounts = accounts {
            var accountsDicts = [[String : AnyObject]]()
            for account in _accounts {
                accountsDicts.append(account.dictionaryEncoded())
            }
            application["accounts"] = accountsDicts
        }
        if let _authorizationType = authorizationType {
            application["authorizationType"] = _authorizationType.dictionaryEncoded()
        }
        if let _customAuthorizationType = customAuthorizationType {
            application["customAuthorizationType"] = _customAuthorizationType
        }
        if let _declarations = declarations {
            var declarationsDicts = [[String : AnyObject]]()
            for declaration in _declarations {
                declarationsDicts.append(declaration.dictionaryEncoded())
            }
            application["declarations"] = declarationsDicts
        }
        if let _bankAdministrationDetail = bankAdministrationDetail {
            application["bankAdministrationDetail"] = _bankAdministrationDetail.dictionaryEncoded()
        }
        return application
    }
}
class ManagedApplication : NSManagedObject {
    @NSManaged var applicationId: String
    @NSManaged var category: ManagedApplicationCategory
    @NSManaged var company: ManagedCompany?
    @NSManaged var companyContactPeople: ManagedCompanyContactPeople?
    @NSManaged var billingAccount: ManagedBillingAccount?
    @NSManaged var portfolioSummaryServices: NSSet?
    @NSManaged var paymentManagementServices: NSSet?
    @NSManaged var transferPurpose: ManagedTransferPurpose?
    @NSManaged var adminMaker: ManagedAdminMaker?
    @NSManaged var adminChecker: ManagedAdminChecker?
    @NSManaged var accountUsers: NSSet?
    @NSManaged var accounts: NSSet?
    @NSManaged var authorizationType: ManagedAuthorizationType?
    @NSManaged var customAuthorizationType: String?
    @NSManaged var declarations: NSSet?
    @NSManaged var bankAdministrationDetail: ManagedBankAdminstrationDetail?
    
    let parentRelation = "application"
    
    func manageApplication(application: Application) {
        if let _managedObjectContext = self.managedObjectContext {
            applicationId = application.applicationId
            
            category = _managedObjectContext.editableObjectOfEntity("ApplicationCategory", withRelation: parentRelation, onObject: self) as ManagedApplicationCategory
            category.manageApplicationCategory(application.category)
            if let applicationCompany = application.company {
                company = _managedObjectContext.editableObjectOfEntity("Company", withRelation: parentRelation, onObject: self) as? ManagedCompany
                company?.manageCompany(applicationCompany)
            }
            if let applicationCompanyContactPeople = application.companyContactPeople {
                companyContactPeople = _managedObjectContext.editableObjectOfEntity("CompanyContactPeople", withRelation: parentRelation, onObject: self) as? ManagedCompanyContactPeople
                companyContactPeople?.manageCompanyContactPeople(applicationCompanyContactPeople)
            }
            if let applicationBillingAccount = application.billingAccount {
                billingAccount = _managedObjectContext.editableObjectOfEntity("BillingAccount", withRelation: parentRelation, onObject: self) as? ManagedBillingAccount
                billingAccount?.manageBillingAccount(applicationBillingAccount)
            }
            var _portfolioSummaryServices: [ManagedAccountPortfolioSummaryService] = []
            if let applicationAccountPortfolioSummaryServices = application.portfolioSummaryServices {
                for applicationAccountPortfolioSummaryService in applicationAccountPortfolioSummaryServices {
                    let managedAccountPortfolioSummaryService = _managedObjectContext.editableObjectOfEntity("AccountPortfolioSummaryService", withRelation: parentRelation, onObject: self, predicate: NSPredicate(format: "id = %@", applicationAccountPortfolioSummaryService.id)!) as ManagedAccountPortfolioSummaryService
                    managedAccountPortfolioSummaryService.manageAccountPortfolioSummaryService(applicationAccountPortfolioSummaryService)
                    _portfolioSummaryServices.append(managedAccountPortfolioSummaryService)
                }
            }
            portfolioSummaryServices = NSSet(array: _portfolioSummaryServices)
            var _paymentManagementServices: [ManagedAccountPaymentManagementService] = []
            if let applicationAccountPaymentManagementServices = application.paymentManagementServices {
                for applicationAccountPaymentManagementService in applicationAccountPaymentManagementServices {
                    let managedAccountPaymentManagementService = _managedObjectContext.editableObjectOfEntity("AccountPaymentManagementService", withRelation: parentRelation, onObject: self, predicate: NSPredicate(format: "id = %@", applicationAccountPaymentManagementService.id)!) as ManagedAccountPaymentManagementService
                    managedAccountPaymentManagementService.manageAccountPaymentManagementService(applicationAccountPaymentManagementService)
                    _paymentManagementServices.append(managedAccountPaymentManagementService)
                }
            }
            paymentManagementServices = NSSet(array: _paymentManagementServices)
            if let applicationTransferPurpose = application.transferPurpose {
                let managedTransferPurpose = _managedObjectContext.editableObjectOfEntity("TransferPurpose", withRelation: parentRelation, onObject: self) as ManagedTransferPurpose
                managedTransferPurpose.manageTransferPurpose(applicationTransferPurpose)
                transferPurpose = managedTransferPurpose
            }
            if let applicationAdminMaker = application.adminMaker {
                adminMaker = _managedObjectContext.editableObjectOfEntity("AdminMaker", withRelation: parentRelation, onObject: self) as? ManagedAdminMaker
                adminMaker!.manageAdminMaker(applicationAdminMaker)
            }
            if let applicationAdminChecker = application.adminChecker {
                adminChecker = _managedObjectContext.editableObjectOfEntity("AdminChecker", withRelation: parentRelation, onObject: self) as? ManagedAdminChecker
                adminChecker!.manageAdminChecker(applicationAdminChecker)
            }
            var _accountUsers = [ManagedAccountUser]()
            if let __accountUsers = application.accountUsers {
                for accountUser in __accountUsers {
                    let managedAccountUser = _managedObjectContext.editableObjectOfEntity("AccountUser", withRelation: parentRelation, onObject: self, predicate: NSPredicate(format: "id = %@", accountUser.id)!) as ManagedAccountUser
                    managedAccountUser.manageAccountUser(accountUser)
                    _accountUsers.append(managedAccountUser)
                }
            }
            accountUsers = NSSet(array: _accountUsers)
            var _accounts = [ManagedAccount]()
            if let __accounts = application.accounts {
                for account in __accounts {
                    let managedAccount = _managedObjectContext.editableObjectOfEntity("Account", withRelation: parentRelation, onObject: self, predicate: NSPredicate(format: "id = %@", account.id)!) as ManagedAccount
                    managedAccount.manageAccount(account)
                    _accounts.append(managedAccount)
                }
            }
            accounts = NSSet(array: _accounts)
            if let applicationAuthorizationType = application.authorizationType {
                authorizationType = _managedObjectContext.editableObjectOfEntity("AuthorizationType", withRelation: parentRelation, onObject: self) as? ManagedAuthorizationType
                authorizationType!.manageAuthorizationType(applicationAuthorizationType)
            }
            customAuthorizationType = application.customAuthorizationType
            var _declarations = [ManagedDeclaration]()
            if let __declarations = application.declarations {
                for declaration in __declarations {
                    let managedDeclaration = _managedObjectContext.editableObjectOfEntity("Declaration", withRelation: parentRelation, onObject: self, predicate: NSPredicate(format: "id = %@", declaration.id)!) as ManagedDeclaration
                    managedDeclaration.manageDeclaration(declaration)
                    _declarations.append(managedDeclaration)
                }
            }
            declarations = NSSet(array: _declarations)
            if let applicationBankAdministrationDetail = application.bankAdministrationDetail {
                bankAdministrationDetail = _managedObjectContext.editableObjectOfEntity("BankAdminstrationDetail", withRelation: parentRelation, onObject: self) as? ManagedBankAdminstrationDetail
                bankAdministrationDetail!.manageBankAdminstrationDetail(applicationBankAdministrationDetail)
            }
        }
    }
}